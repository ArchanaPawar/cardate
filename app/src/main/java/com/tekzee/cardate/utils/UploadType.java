package com.tekzee.cardate.utils;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public interface UploadType {
    String PROFILE = "profile";
    String VEHICLE = "vehicle";
    String ID_PROOF = "idproof";
    String LICENCE = "licence";
    String INSURANCE = "insurance";
    String VEHICLE_REGISTRATION = "reg";
    String TEXT = "text";
    String IMAGE = "image";
    String AUDIO = "audio";
    String VIDEO = "video";
}
