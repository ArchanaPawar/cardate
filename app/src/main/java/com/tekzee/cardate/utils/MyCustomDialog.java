package com.tekzee.cardate.utils;

import android.content.Context;
import android.widget.Toast;

import com.tekzee.cardate.R;

import cn.refactor.lib.colordialog.ColorDialog;
import cn.refactor.lib.colordialog.PromptDialog;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public class MyCustomDialog {
    public static ColorDialog dialog;


    public static void show(Context context, String message, int type) {

//        final PromptDialog dialog = new PromptDialog(context);
//        dialog.setColor("#b51800");
//        dialog.setCancelable(false);
//        dialog.setDialogType(type)
//                .setAnimationEnable(true)
//                .setTitleText(context.getResources().getString(R.string.app_name))
//                .setContentText(message)
//                .setPositiveListener(context.getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
//                    @Override
//                    public void onClick(PromptDialog dialog1) {
//                        dialog.dismiss();
//                    }
//                }).show();
        ColorDialog dialog = new ColorDialog(context);
        dialog.setColor("#b51800");
        dialog.setCancelable(false);
        dialog.setAnimationEnable(true);
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setContentText(message);
        dialog.setPositiveListener(context.getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
            }
        }).show();


    }

    public static void show(Context context, String message) {
        ColorDialog dialog = new ColorDialog(context);
        dialog.setColor("#b51800");
        dialog.setCancelable(false);
        dialog.setAnimationEnable(true);
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setContentText(message);
        dialog.setPositiveListener(context.getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
            }
        }).show();
    }

    public static ColorDialog showOption(final Context context, String message) {
        dialog = new ColorDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.app_name));
        dialog.setContentText(message);
        dialog.setPositiveListener(context.getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                Toast.makeText(context, dialog.getPositiveText().toString(), Toast.LENGTH_SHORT).show();
            }
        })
                .setNegativeListener(context.getResources().getString(R.string.cancel), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        Toast.makeText(context, dialog.getNegativeText().toString(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }).show();

        return dialog;
    }


}
