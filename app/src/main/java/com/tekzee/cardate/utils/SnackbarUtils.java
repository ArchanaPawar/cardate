package com.tekzee.cardate.utils;

import android.graphics.Color;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.tekzee.cardate.R;

import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public final class SnackbarUtils {

    public static void snackBarBottom(View view, String message){

        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

    public static void snackBarTop(View view, String message){
        Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_LONG);
        View view1 = snackbar.getView();

        try {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view1.getLayoutParams();
            params.gravity = Gravity.TOP;
            view1.setLayoutParams(params);
        }catch (Exception e){

            try {
                DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) view1.getLayoutParams();
                params.gravity = Gravity.TOP;
                view1.setLayoutParams(params);
            }catch (Exception e1){
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view1.getLayoutParams();
                params.gravity = Gravity.TOP;
                view1.setLayoutParams(params);

            }

        }


//        View sbView = snackbar.getView();
        TextView textView = (TextView) view1.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

}
