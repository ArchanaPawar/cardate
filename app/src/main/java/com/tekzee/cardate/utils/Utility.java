package com.tekzee.cardate.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.OpenableColumns;
import android.util.DisplayMetrics;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.language.LanguageSettingsActivity;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public class Utility {
    private static String CAR_DATE = "CAR_DATE";

    public static void setSharedPreference(Context context, String name, String value) {
        SharedPreferences settings = context.getSharedPreferences(CAR_DATE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static String getSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(CAR_DATE, 0);
        return settings.getString(name, "");
    }

    public static void setSharedPreferenceBoolean(Context context, String name, boolean value) {
        SharedPreferences settings = context.getSharedPreferences(CAR_DATE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.commit();
    }

    public static boolean getSharedPreferencesBoolean(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(CAR_DATE, 0);
        return settings.getBoolean(name, false);
    }


    public static void clearSharedPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(CAR_DATE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    public static boolean isConnectingToInternet() {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(2000, TimeUnit.MILLISECONDS);
            future.cancel(true);

        } catch (Exception e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean dateValidator(final String date) {
        if (date.length() == 10) {
            int day = Integer.parseInt(date.split("-")[0]);
            int month = Integer.parseInt(date.split("-")[1]);
            int year = Integer.parseInt(date.split("-")[2]);
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);;

            if((day < 32) && (month < 13) && (year <= currentYear)){

                return true;
            }

        }

        return false;

    }

    public static byte[] getBytes(InputStream inputStream) {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        try {
            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteBuffer.toByteArray();
    }


    public static String getDoubleDigit(int number) {
        if (number < 10) {

            return "0" + number;

        } else {

            return "" + number + "";
        }
    }

    public static String getFormattedPostTime(String createdTimeStamp) {
        return createdTimeStamp;
    }


    public static String getFileName(Context mContext, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public static String covertTimeToText(Context mContext, String dataDate) {
        String convTime = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " " + mContext.getResources().getString(R.string.seconds) + " " + mContext.getResources().getString(R.string.ago);
            } else if (minute < 60) {
                convTime = minute + " " + mContext.getResources().getString(R.string.minutes) + " " + mContext.getResources().getString(R.string.ago);
            } else if (hour < 24) {
                convTime = hour + " " + mContext.getResources().getString(R.string.hours) + " " + mContext.getResources().getString(R.string.ago);
            } else if (day >= 7) {
                if (day > 30) {
                    convTime = (day / 30) + " " + mContext.getResources().getString(R.string.months) + " " + mContext.getResources().getString(R.string.ago);
                } else if (day > 360) {
                    convTime = (day / 360) + " " + mContext.getResources().getString(R.string.years) + " " + mContext.getResources().getString(R.string.ago);
                } else {
                    long week = (day / 7);
                    if(week > 1){
                        convTime = ((day / 7) + " " + mContext.getResources().getString(R.string.weeks) + " " + mContext.getResources().getString(R.string.ago)) + ", "+changeDateFormat(dataDate);
                    }else{
                        convTime = (day / 7) + " " + mContext.getResources().getString(R.string.week) + " " + mContext.getResources().getString(R.string.ago);
                    }


                }
            } else if (day < 7) {
                convTime = day + " " + mContext.getResources().getString(R.string.days) + " " + mContext.getResources().getString(R.string.ago);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.view("ConvTimeE", e.getMessage());
        }

        return convTime;

    }

    public static String changeDateFormat(String dataDate) {
        try {

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate=inputFormat.parse(dataDate);
            inputFormat= new SimpleDateFormat("dd/MM/YYYY");
            return inputFormat.format(newDate);

        } catch (ParseException e) {
            e.printStackTrace();
            Log.view("ConvTimeE", e.getMessage());
        }

        return dataDate;

    }


    public static void changeAppLanguage(Activity mActivity) {
        try {

            String language_code = Utility.getSharedPreferences(mActivity, Constant.LANGUAGE_CODE);
            if (language_code.equalsIgnoreCase("")) {
                Intent intent = new Intent(mActivity, LanguageSettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intent);
            }
            Resources res = mActivity.getResources();
            /**
             * Change locale settings in the app.
             */
            DisplayMetrics dm = res.getDisplayMetrics();
            Locale locale = new Locale(language_code);
            Locale.setDefault(locale); // this is needed to change even the map's language
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                conf.setLayoutDirection(conf.locale);
            }
            res.updateConfiguration(conf, dm);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

