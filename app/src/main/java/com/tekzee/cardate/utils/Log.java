package com.tekzee.cardate.utils;

import com.tekzee.cardate.BuildConfig;

/**
 * Created by Hasnain on 24-Apr-19.
 */
public final class Log {

    public static void view(String TAG, String message){
        if(BuildConfig.DEBUG){
            android.util.Log.e(TAG,message);
        }

    }
}
