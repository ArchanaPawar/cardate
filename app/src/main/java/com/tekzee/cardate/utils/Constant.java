package com.tekzee.cardate.utils;

/**
 * Created by Hasnain on 24-Apr-19.
 */
public interface Constant {
    int INSTA_IMAGE_REQUEST = 100 ;
    int APPLY_FILTER_REQUEST_CODE = 7007;
    int IMAGE_REQUEST = 9981;
    int STATUS_CODE = 404;
    int LOGOUT_DELAY = 4000;//5 seconds




    /**
     * Shared preference constant data
     */
//    String PROVIDE_AUTHORITY = "com.tekzee.cardate.fileprovider";
    /**
     * don't change firebase password otherwise
     * firebase chat not works
     */
    String FIREBASE_PASSWORD = "CarDate@123";
    String IS_LOGIN = "IS_LOGIN";
    String USER_ID = "USER_ID";
    String NAME = "NAME";
    String EMAIL = "EMAIL";
    String DOB = "DOB";
    String AGE = "AGE";
    String CAR_NUMBER = "CAR_NUMBER";
    String GENDER = "GENDER";
    String PROFILE_IMAGE = "PROFILE_IMAGE";
//    String COUNTRY_ID = "COUNTRY_ID";
//    String COUNTRY_NAME = "COUNTRY_NAME";
//    String CITY_ID = "CITY_ID";
//    String CITY_NAME = "CITY_NAME";
    String AUTH_TOKEN = "AUTH_TOKEN";
    String OCCUPATION = "OCCUPATION";
    String ABOUT_ME = "ABOUT_ME";
    String IS_SOCIAL = "IS_SOCIAL";
    String FCM_TOKEN = "FCM_TOKEN";
    String IS_EMAIL_NOTIFICATION = "IS_EMAIL_NOTIFICATION";

    /**
     * Temp
     */
    String UPDATE_CITY_ID = "UPDATE_CITY_ID";
    String UPDATE_COUNTRY_ID = "UPDATE_COUNTRY_ID";
    String UPDATE_DOB = "UPDATE_DOB";

    /**
     * filter data
     */
    String FILTER_GENDER = "FILTER_GENDER";
    String FILTER_GENDER_ID = "FILTER_GENDER_ID";
    String FILTER_AGE_MIN = "FILTER_AGE_MIN";
    String FILTER_AGE_MAX = "FILTER_AGE_MAX";
    String FILTER_LOCATION = "FILTER_LOCATION";
    String FILTER_LOCATION_ID = "FILTER_LOCATION_ID";
    String FILTER_COUNTRY = "FILTER_COUNTRY";
    String FILTER_COUNTRY_ID = "FILTER_COUNTRY_ID";
    String FILTER_CITY = "FILTER_CITY";
    String FILTER_CITY_ID = "FILTER_CITY_ID";
    String IS_PREMIUM_USER = "IS_PREMIUM_USER";
    String LANGUAGE_CODE = "LANGUAGE_CODE" ;
    String FROM_IMAGESELECOR = "FROM_IMAGESELECOR";
    String INSTA_ACCESS_TOKEN = "INSTA_ACCESS_TOKEN";

    /**
     * Fire-base
     */
    String FIREBASE_USERS = "Users";
    String FIREBASE_CHATS = "Chats";
    String FIREBASE_CHAT_LIST = "ChatList";
    String FIREBASE_SEARCH = "search";
    String FIREBASE_USER_ID = "FIREBASE_USER_ID";

    String MATCH_COUNT = "MATCH_COUNT";
    String FRIEND_COUNT = "FRIEND_COUNT";
    String CHAT_COUNT = "CHAT_COUNT";
}
