package com.tekzee.cardate.ui.post.everyone;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;


public interface EveryonePostView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<EveryOneModel> arrayList);

    void onPostFail(String message);
}
