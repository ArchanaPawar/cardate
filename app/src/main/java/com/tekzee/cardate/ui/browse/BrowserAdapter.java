package com.tekzee.cardate.ui.browse;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;

import java.util.ArrayList;

import static android.content.Context.WINDOW_SERVICE;

public class BrowserAdapter extends RecyclerView.Adapter<BrowserAdapter.ViewHolder> {
    private ArrayList<BrowseModel> arrayList;
    private RowSelect listener;
    private Context mContext;

    public BrowserAdapter(ArrayList<BrowseModel> arrayList, RowSelect listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    public interface RowSelect {
        void onSelect(BrowseModel model);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_browse, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(arrayList.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private TextViewRegular tvUserName;
        private TextViewRegular tvAge;
        private TextViewRegular tvCountry;
        private ImageView ivProfileImage;
        private ImageView ivOnline;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cvRoot = itemView.findViewById(R.id.cvRoot);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvAge = itemView.findViewById(R.id.tvAge);
            tvCountry = itemView.findViewById(R.id.tvCountry);
            ivOnline = itemView.findViewById(R.id.ivOnline);

            /**
             * set image dynamic height
             */
            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            DisplayMetrics dm = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) mContext.getSystemService(WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;
            width = ((width / 2) - 70);

            ivProfileImage.getLayoutParams().height = width;
            ivProfileImage.getLayoutParams().width = width;
        }


        private void bind(final BrowseModel model, final RowSelect listener) {
            tvUserName.setText(model.getName());
            tvAge.setText(model.getAge() + ", ");
            tvCountry.setText(model.getCountry());

            String image = model.getImage();

            if (!image.equalsIgnoreCase("")) {
                Glide.with(mContext).load(image).into(ivProfileImage);
            }

            if (model.isOnline()) {
                ivOnline.setVisibility(View.VISIBLE);
            } else {
                ivOnline.setVisibility(View.GONE);
            }

            cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelect(model);
                }
            });
        }
    }
}
