package com.tekzee.cardate.ui.signup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;

import java.util.ArrayList;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> implements Filterable {
    private ArrayList<CountryModel> originalArrayList;
    private ArrayList<CountryModel> arrayList;
    private RowSelect listener;

    public CountryAdapter(ArrayList<CountryModel> arrayList, RowSelect listener) {
        this.originalArrayList = arrayList;
        this.arrayList = arrayList;
        this.listener = listener;
    }

    private Filter fRecords;

    @Override
    public Filter getFilter() {
        if (fRecords == null) {
            fRecords = new RecordFilter();
        }
        return fRecords;
    }


    //filter class
    private class RecordFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            //Implement filter logic
            // if edittext is null return the actual list
            if (constraint == null || constraint.length() == 0) {
                //No need for filter
                results.values = originalArrayList;
                results.count = originalArrayList.size();

            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                ArrayList<CountryModel> fRecords = new ArrayList<>();

                for (CountryModel model : originalArrayList) {
                    if (model.getName().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim())) {
                        fRecords.add(model);
                    }
                }
                results.values = fRecords;
                results.count = fRecords.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            //it set the data from filter to adapter list and refresh the recyclerview adapter
            arrayList = (ArrayList<CountryModel>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface RowSelect {
        void onSelect(CountryModel model);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(arrayList.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private TextViewRegular tv_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cvRoot = itemView.findViewById(R.id.cvRoot);
            tv_name = itemView.findViewById(R.id.tv_name);
        }


        public void bind(final CountryModel model, final RowSelect listener) {
            tv_name.setText(model.getName());
            cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelect(model);
                }
            });
        }
    }
}
