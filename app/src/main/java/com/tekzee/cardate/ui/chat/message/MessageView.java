package com.tekzee.cardate.ui.chat.message;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface MessageView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void sendMessage(String sender, String receiver, String message,String url,String type);

    String getUserMessage();

    void onStartRecording(String filePath);

    void onStopRecording();

    void onStartPlaying(String fileName);

    void onStopPlaying();


}