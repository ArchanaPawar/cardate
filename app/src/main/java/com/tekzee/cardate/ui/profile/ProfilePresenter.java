package com.tekzee.cardate.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.ui.signup.CountryAdapter;
import com.tekzee.cardate.ui.signup.CountryModel;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfilePresenter extends BasePresenter<ProfileView> {
    private static final String TAG = ProfilePresenter.class.getSimpleName();
    private int characterCount;

    public ProfilePresenter(ProfileView view, Context mContext) {

        attachView(view, mContext);
    }

    public void getProfileDetails()
    {
        if (Utility.isConnectingToInternet())
        {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
//                input.put("user_id", "30");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);
            Log.view(TAG, "Token : " + Utility.getSharedPreferences(mvpView.getAppContext(), Constant.AUTH_TOKEN));

            addSubscription(apiStores.getUserProfile(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status"))
                        {
                            mvpView.onProfileDataSuccess(jsonObject.getJSONObject("data"));
                        } else if (!jsonObject.getBoolean("status"))
                        {
                            mvpView.showInPopup(jsonObject.getString("message"));
                        } else {
                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void unSubscribePlan() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);
            Log.view(TAG, "Token : " + Utility.getSharedPreferences(mvpView.getAppContext(), Constant.AUTH_TOKEN));

            addSubscription(apiStores.unsubscribePlan(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onUnsubscribePlan(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void getInstargramImages(String accessToken) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);


            addSubscription(instargarmApiStores.getInstargramImages(accessToken), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);
                        String code = jsonObject.getJSONObject("meta").getString("code");

                        if (code.equalsIgnoreCase("200")) {

                            JSONArray dataJsonArray = jsonObject.getJSONArray("data");
                            ArrayList<ImageUrl> arrayList = new ArrayList<>();

                            if (dataJsonArray.length() > 0) {

                                for (int i = 0; i < dataJsonArray.length(); i++) {

                                    String imageUrl = dataJsonArray.getJSONObject(i).getJSONObject("images").getJSONObject("standard_resolution").getString("url");
                                    Log.view(TAG, "imageUrl : " + imageUrl);
                                    arrayList.add(new ImageUrl(imageUrl));

                                }

                                mvpView.onInstagramImageLoaded(arrayList);

                            } else {

                                mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.no_image_found));

                            }


                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void openNameAgeEditDialog() {
        final Dialog dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_edit_name_age);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            final EditTextRegular etName = dialog.findViewById(R.id.etName);
            etName.setText(mvpView.getProfileName());

            final EditText etDOB = dialog.findViewById(R.id.etDOB);
            final String dob = Utility.getSharedPreferences(mvpView.getAppContext(), Constant.UPDATE_DOB);
            etDOB.setText(dob);

            final TextViewRegular tvCountry = dialog.findViewById(R.id.tvCountry);
            tvCountry.setText(mvpView.getCountryName());

            final TextViewRegular tvCity = dialog.findViewById(R.id.tvCity);
            tvCity.setText(mvpView.getCityName());

            etDOB.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                }

                @Override
                public void afterTextChanged(Editable view) {
                    if (characterCount <= etDOB.getText().toString().length()
                            &&(etDOB.getText().toString().length()==2
                            ||etDOB.getText().toString().length()==5)){
                        etDOB.setText(etDOB.getText().toString()+"-");
                        int pos = etDOB.getText().length();
                        etDOB.setSelection(pos);
                    }else if (characterCount >= etDOB.getText().toString().length()
                            &&(etDOB.getText().toString().length()==2
                            ||etDOB.getText().toString().length()==5)){
                        etDOB.setText(etDOB.getText().toString().substring(0,etDOB.getText().toString().length()-1));
                        int pos = etDOB.getText().length();
                        etDOB.setSelection(pos);
                    }
                    characterCount = etDOB.getText().toString().length();
                }

            });


//            tvDOB.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    int mYear = Integer.parseInt(dob.split("-")[2]);
//                    int mMonth = (Integer.parseInt(dob.split("-")[1]) - 1);
//                    int mDay = Integer.parseInt(dob.split("-")[0]);
//
//                    DatePickerDialog datePickerDialog = new DatePickerDialog(dialog.getContext(),
//                            new DatePickerDialog.OnDateSetListener() {
//
//                                @Override
//                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
//                                    tvDOB.setText(Utility.getDoubleDigit(dayOfMonth) + "-" + Utility.getDoubleDigit((monthOfYear + 1)) + "-" + year);
//                                    Utility.setSharedPreference(mvpView.getAppContext(), Constant.UPDATE_DOB, tvDOB.getText().toString());
//
//                                }
//                            }, mYear, mMonth, mDay);
//
//                    /**
//                     * set max day (Today)in calendar
//                     */
//                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//
//                    datePickerDialog.show();
//                }
//            });

            tvCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getAllCountry(tvCountry, tvCity);
                }
            });


            tvCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String countryId = Utility.getSharedPreferences(mvpView.getAppContext(), Constant.UPDATE_COUNTRY_ID);

                    if (countryId.equalsIgnoreCase("")) {

                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.alert_country_select));

                    } else {

                        getAllCity(countryId, tvCountry, tvCity);

                    }

                }
            });

            dialog.findViewById(R.id.btnUpdate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = etName.getText().toString().trim();
                    String city = tvCity.getText().toString().trim();
                    String dob = etDOB.getText().toString().trim();

                    if (name.equalsIgnoreCase("")) {
                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.alert_name));

                    } else if (dob.equalsIgnoreCase("")) {
                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.select_dob));

                    } else if (!Utility.dateValidator(dob)) {
                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.invalid_dob));

                    } else if (city.equalsIgnoreCase("")) {
                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.alert_select_city));

                    } else {
                        mvpView.setProfileName(name);
                        Utility.setSharedPreference(mvpView.getAppContext(), Constant.UPDATE_DOB, dob);
                        dialog.dismiss();
                        updateProfile();

                    }
                }
            });


            dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void getAllCountry(final TextViewRegular tvCountry, final TextViewRegular tvCity) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            addSubscription(apiStores.getAllCountry(), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<CountryModel> arrayList = new ArrayList<>();
                            JSONArray countryJsonArray = jsonObject.getJSONArray("data");
                            for (int index = 0; index < countryJsonArray.length(); index++) {
                                JSONObject object = countryJsonArray.getJSONObject(index);

                                arrayList.add(new CountryModel(
                                        object.getString("id"),
                                        object.getString("country")
                                ));
                            }

                            /**
                             * open dialog for select country
                             */
                            openSelector(arrayList, mvpView.getAppContext().getResources().getString(R.string.country), tvCountry, tvCity);

                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void getAllCity(String country_id, final TextViewRegular tvCountry, final TextViewRegular tvCity) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("country_id", country_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getAllCity(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<CountryModel> arrayList = new ArrayList<>();
                            JSONArray countryJsonArray = jsonObject.getJSONArray("data");
                            for (int index = 0; index < countryJsonArray.length(); index++) {
                                JSONObject object = countryJsonArray.getJSONObject(index);

                                arrayList.add(new CountryModel(
                                        object.getString("id"),
                                        object.getString("city")
                                ));
                            }

                            /**
                             * open dialog for select country
                             */
                            openSelector(arrayList, mvpView.getAppContext().getResources().getString(R.string.city), tvCountry, tvCity);


                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    private void openSelector(ArrayList<CountryModel> arrayList, final String type, final TextViewRegular tvCountry, final TextViewRegular tvCity) {
        if (arrayList.size() > 0) {

            final Dialog dialog = new Dialog(mvpView.getAppContext());
            try {
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setContentView(R.layout.popup_select_country);
                dialog.getWindow().setLayout(-1, -2);
                dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(false);

                ((TextViewBold) dialog.findViewById(R.id.title)).setText(mvpView.getAppContext().getResources().getString(R.string.select) + type);

                RecyclerView rv_country = dialog.findViewById(R.id.rv_country);
                rv_country.setLayoutManager(new LinearLayoutManager(dialog.getContext()));

                final CountryAdapter adapter = new CountryAdapter(arrayList, new CountryAdapter.RowSelect() {
                    @Override
                    public void onSelect(CountryModel model) {
                        dialog.dismiss();
                        if (type.equalsIgnoreCase("Country")) {

                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.UPDATE_COUNTRY_ID, model.getId());
                            tvCountry.setText(model.getName());
                            tvCity.setText("");

                        } else if (type.equalsIgnoreCase("City")) {
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.UPDATE_CITY_ID, model.getId());
                            tvCity.setText(model.getName());


                        }

                    }
                });
                rv_country.setAdapter(adapter);
                EditTextRegular et_country_name = dialog.findViewById(R.id.et_country_name);

                et_country_name.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.no_country_found));
        }

    }

    public void openAboutMeEditorDialog(String title, final TextViewRegular mTextViewRegular, String text) {

        final Dialog dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_about_me);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            ((TextViewBold) dialog.findViewById(R.id.title)).setText(title);

            final EditTextRegular etAboutMe = dialog.findViewById(R.id.etAboutMe);
            etAboutMe.setText(text);


            dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.findViewById(R.id.btnUpdate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String about = etAboutMe.getText().toString().trim();

                    if (about.equalsIgnoreCase("")) {

                        mvpView.onToast(mvpView.getAppContext().getResources().getString(R.string.alert_about_me));

                    } else {
                        dialog.dismiss();
                        mTextViewRegular.setText(about);
                        updateProfile();

                    }
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllInterestIn(final ArrayList<String> id) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);

            addSubscription(apiStores.getAllInterest(), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<InterestModel> arrayList = new ArrayList<>();
                            JSONArray interestJsonArray = jsonObject.getJSONArray("data");
                            for (int index = 0; index < interestJsonArray.length(); index++) {
                                JSONObject object = interestJsonArray.getJSONObject(index);

                                arrayList.add(new InterestModel(
                                        object.getString("id"),
                                        object.getString("interest"),
                                        false
                                ));
                            }

                            /**
                             * open dialog
                             */
                            openInterestSelector(arrayList, id);

                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }

    private void openInterestSelector(final ArrayList<InterestModel> arrayList, final ArrayList<String> id) {
        if (arrayList.size() > 0) {

            final Dialog dialog = new Dialog(mvpView.getAppContext());
            try {
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setContentView(R.layout.popup_interest);
                dialog.getWindow().setLayout(-1, -2);
                dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(false);

                RecyclerView rvInterest = dialog.findViewById(R.id.rvInterest);
                rvInterest.setLayoutManager(new LinearLayoutManager(dialog.getContext()));
                final InterestAdapter adapter = new InterestAdapter(arrayList, id, new InterestAdapter.RowSelect() {
                    @Override
                    public void onSelect(InterestModel model) {
                        dialog.dismiss();
                    }
                });
                rvInterest.setAdapter(adapter);

                dialog.findViewById(R.id.iv_submit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mvpView.updateInterested(arrayList);
                        updateProfile();
                    }
                });

                dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.no_country_found));
        }

    }


    public void updateProfile() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("name", mvpView.getProfileName());
                input.put("country", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.UPDATE_COUNTRY_ID));
                input.put("car_number", mvpView.getCarNumber());
                input.put("about_me", mvpView.getAboutMe());

                String interest = "";
                ArrayList<String> getInterest = mvpView.getInterestIs();
                for (int i = 0; i < getInterest.size(); i++) {

                    interest = interest + getInterest.get(i) + ",";
                }
                if (!interest.equalsIgnoreCase("")) {
                    interest = interest.substring(0, (interest.length() - 1));
                }


                input.put("interest", interest);
                input.put("occupation", mvpView.getOccupation());
                input.put("city", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.UPDATE_CITY_ID));
                input.put("dob", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.UPDATE_DOB));
                input.put("company_name", mvpView.getCompanyName());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.updateProfile(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            getProfileDetails();


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });

        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }
    }

    public void updateCarNumber(final TextViewRegular tvCarNumber) {

        final Dialog dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_update_car_number);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            final EditTextRegular et_car_no = dialog.findViewById(R.id.et_car_no);
            et_car_no.setHint(mvpView.getCarNumber());

            dialog.findViewById(R.id.iv_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String carNumber = et_car_no.getText().toString().trim();
                    tvCarNumber.setText(carNumber);
                    dialog.dismiss();
                    updateProfile();
                }
            });

            dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void uploadImage(String upload_type, byte[] imageBytes) {
        RequestBody type = RequestBody.create(MultipartBody.FORM, upload_type);
        RequestBody user_id = RequestBody.create(MultipartBody.FORM, Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes);
        MultipartBody.Part formData = MultipartBody.Part.createFormData("image", "image_" + System.currentTimeMillis() + ".jpg", requestFile);

        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            Log.view("type",upload_type);
            addSubscription(apiStores.uploadImage(formData, user_id, type), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            getProfileDetails();

                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
