package com.tekzee.cardate.ui.language;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

public class LanguageSettingsActivity extends AppCompatActivity {
    private static final String TAG = LanguageSettingsActivity.class.getSimpleName();
    private Activity mActivity;
    private RadioGroup rgLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_settings);
        mActivity = this;
        init();

        findViewById(R.id.btnContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedRadioButtonId = rgLanguage.getCheckedRadioButtonId();
                if (checkedRadioButtonId == -1) {
                    Toast.makeText(mActivity, getResources().getString(R.string.alert_language_selection), Toast.LENGTH_SHORT).show();

                } else {
                    if (checkedRadioButtonId == R.id.rbGerman) {
                        Utility.setSharedPreference(mActivity, Constant.LANGUAGE_CODE, "de");

                    } else {

                        Utility.setSharedPreference(mActivity, Constant.LANGUAGE_CODE, "en");

                    }

                    Utility.changeAppLanguage(mActivity);
                    onContinue();
                }

            }
        });
    }


    private void init() {
        rgLanguage = findViewById(R.id.rgLanguage);
    }


    private void onContinue() {
        if (Utility.getSharedPreferencesBoolean(mActivity, Constant.IS_LOGIN)) {
            startActivity(new Intent(mActivity, NavigationActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        } else {
            startActivity(new Intent(mActivity, IntroductionActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        }


    }


}
