package com.tekzee.cardate.ui.membershipplan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.membershipplan.heidelpay.HeidelPayActivity;

import cn.refactor.lib.colordialog.ColorDialog;

public class PageFragment extends Fragment {
    private static final String ARG_TEXT = "param1";
    private TextViewBold tvName;
    private TextViewRegular tvDescription;
    private RecyclerView rvContents;
    private AlertDialog.Builder mDialog;
    private PlanModel model;

    public PageFragment() {

    }

    public static PageFragment newInstance(PlanModel model) {
        PageFragment fragment = new PageFragment();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(ARG_TEXT, model);
        fragment.setArguments(mBundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            model = (com.tekzee.cardate.ui.membershipplan.PlanModel) getArguments().getSerializable(ARG_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        init(view);

        tvName.setText(model.getName());
        tvDescription.setText(model.getCurrency() +" "+ model.getAmount() +" "+ getActivity().getResources().getString(R.string.per_month));

        view.findViewById(R.id.btnGetItNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final double total = (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount()));
                String message =  getActivity().getResources().getString(R.string.thankyou_for_selecting) +" - "+ model.getName() + " - "+getActivity().getResources().getString(R.string.payble_amount) + " "+model.getCurrency() + total + " "+getActivity().getResources().getString(R.string.should_we_continue);
                mDialog = new AlertDialog.Builder(getActivity());
                mDialog.setTitle(getActivity().getResources().getString(R.string.app_name));
                mDialog.setMessage(message);
//                mColorDialog = new ColorDialog(getActivity());
//                mColorDialog.setTitle(getActivity().getResources().getString(R.string.app_name));
//                mColorDialog.setContentText(message);
//                mColorDialog.setPositiveListener(getActivity().getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
//                    @Override
//                    public void onClick(ColorDialog dialog) {
//                        dialog.dismiss();
//
//                        onStartPaymentActivity(total, model.getCurrency());
//                    }
//                })
//                        .setNegativeListener(getActivity().getResources().getString(R.string.cancel), new ColorDialog.OnNegativeListener() {
//                            @Override
//                            public void onClick(ColorDialog dialog) {
//
//                                dialog.dismiss();
//                            }
//                        }).show();

                mDialog.setPositiveButton(
                        getActivity().getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                onStartPaymentActivity(total, model.getCurrency());
                            }
                        });

                mDialog.setNegativeButton(
                        getActivity().getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog mAlertDialog = mDialog.create();
                mAlertDialog.show();

            }
        });


        return view;
    }

    private void init(View mView) {
        tvName = mView.findViewById(R.id.tvName);
        tvDescription = mView.findViewById(R.id.tvDescription);
        rvContents = mView.findViewById(R.id.rvContents);
        rvContents.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvContents.setAdapter(new ContentAdapter(model.getContentModelArrayList()));

    }


    private void onStartPaymentActivity(double total, String currency) {
        Intent intent = new Intent(getActivity(), HeidelPayActivity.class);
        intent.putExtra("model", model);
        startActivity(intent);

    }
}