package com.tekzee.cardate.ui.geotaggingimage;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface GeoTaggingImageView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

}
