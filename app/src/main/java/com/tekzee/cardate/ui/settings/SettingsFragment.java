package com.tekzee.cardate.ui.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.blockuser.BlockUserActivity;
import com.tekzee.cardate.ui.changepassword.ChangePasswordActivity;
import com.tekzee.cardate.ui.contactus.ContactUsActivity;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.language.LanguageSettingsActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.ui.profile.ProfileFragment;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;
import java.util.Locale;

import cn.refactor.lib.colordialog.ColorDialog;

public class SettingsFragment extends MvpFragment<SettingsPresenter> implements SettingsView {
    private static final String TAG = SettingsFragment.class.getSimpleName();
    private Switch toggleSwitch;
    private TextViewRegular tvImprint;
    private boolean isOtherPasswordCheck = true;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_settings, container, false);
        init(mView);

        /**
         * Change Password
         */
        mView.findViewById(R.id.tvChangePassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onStartChangePasswordActivity();

            }
        });

        /**
         * Cancel Account
         */
        mView.findViewById(R.id.tvCancelAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDialog dialog = new ColorDialog(getAppContext());
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setContentText(getResources().getString(R.string.message_cancel_account));
                dialog.setPositiveListener(getResources().getString(R.string.yes), new ColorDialog.OnPositiveListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();

                        /**
                         * conform  account password before delete
                         */
                        AlertDialog.Builder builder = new AlertDialog.Builder(getAppContext());
                        builder.setTitle(getResources().getString(R.string.txt_con_password));

                        final EditText input = new EditText(getAppContext());
                        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        input.setFocusable(true);
                        input.requestFocus();
                        input.setHint("******");
                        builder.setView(input);

                        builder.setPositiveButton(getResources().getString(R.string.submit), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String password = input.getText().toString();
                                String email = Utility.getSharedPreferences(getAppContext(), Constant.EMAIL);

                                deleteFirebaseUser(email, password);

                            }
                        });
                        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();

                    }
                }).setNegativeListener(getResources().getString(R.string.no), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {

                        dialog.dismiss();
                    }
                }).show();

            }
        });

        /**
         * Manage Account
         */
        mView.findViewById(R.id.tvManageAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationActivity) getActivity()).resetAllFilter();
                ((NavigationActivity) getActivity()).replaceFragment(ProfileFragment.newInstance(), getActivity().getResources().getString(R.string.menu_profile));

            }
        });

        /**
         * Blocked User
         */
        mView.findViewById(R.id.tvBlockedUser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartBlockUserActivity();

            }
        });


        /**
         * Change Language
         */
        mView.findViewById(R.id.tvChangeLanguage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangeLanguageSuccess();

            }
        });

        /**
         * Contact Us
         */
        mView.findViewById(R.id.tvContactUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartContactUsActivity();
            }
        });

        /**
         * Imprint
         */
        tvImprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * 1 => Payment Terms & Condition
                 * 2 => Imprint
                 * 3 => Privacy & Policy
                 * 4 => Terms of Uses
                 */
                mvpPresenter.getWebPage("2");
            }
        });

        /**
         * Privacy Policy
         */
        mView.findViewById(R.id.tvPrivacyPolicy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * 1 => Payment Terms & Condition
                 * 2 => Imprint
                 * 3 => Privacy & Policy
                 * 4 => Terms of Uses
                 */
                mvpPresenter.getWebPage("3");
            }
        });

        /**
         * Terms Of Use
         */

        mView.findViewById(R.id.tvTermsOfUse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * 1 => Payment Terms & Condition
                 * 2 => Imprint
                 * 3 => Privacy & Policy
                 * 4 => Terms of Uses
                 */
                mvpPresenter.getWebPage("4");
            }
        });


        /**
         * logout
         */
        mView.findViewById(R.id.tvLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDialog dialog = new ColorDialog(getAppContext());
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setContentText(getResources().getString(R.string.message_logout));
                dialog.setPositiveListener(getResources().getString(R.string.yes), new ColorDialog.OnPositiveListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();
                        deleteFirebaseToken();
                        FirebaseAuth.getInstance().signOut();

                        mvpPresenter.logoutFromApp();

                    }
                }).setNegativeListener(getResources().getString(R.string.no), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {

                        dialog.dismiss();
                    }
                }).show();

            }
        });


        toggleSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utility.getSharedPreferencesBoolean(getAppContext(), Constant.IS_EMAIL_NOTIFICATION)) {
                    mvpPresenter.updateEmailStatus("0");
                } else {
                    mvpPresenter.updateEmailStatus("1");
                }

            }
        });


        mvpPresenter.getEmailStatus();

        return mView;

    }

    private void init(View mView) {
        toggleSwitch = mView.findViewById(R.id.toggleSwitch);
        tvImprint = mView.findViewById(R.id.tvImprint);
        if (Utility.getSharedPreferences(getAppContext(), Constant.LANGUAGE_CODE).equalsIgnoreCase("en")) {
            tvImprint.setVisibility(View.GONE);
        } else {
            tvImprint.setVisibility(View.VISIBLE);
        }

    }


    private void deleteFirebaseUser(String email, String password) {
        try {
            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            Log.view(TAG, "Email: " + email + " Password: " + password);
            AuthCredential credential = EmailAuthProvider.getCredential(email, password);

            user.reauthenticate(credential)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.view(TAG, "onComplete: authentication complete");
                                user.delete()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.view(TAG, "User account deleted.");
                                                    mvpPresenter.cancelAccount();
                                                } else {
                                                    Log.view(TAG, "User account deletion unsucessful.");
                                                }
                                            }
                                        });
                            } else {
                                if(isOtherPasswordCheck){
                                    isOtherPasswordCheck = false;
                                    deleteFirebaseUser(email,Constant.FIREBASE_PASSWORD);

                                }else{
                                    Toast.makeText(getAppContext(), "Authentication failed", Toast.LENGTH_SHORT).show();

                                }

                            }
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected SettingsPresenter createPresenter() {
        return new SettingsPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
        onEmailStatusChange();
    }

    @Override
    public void showInSnackBar(String message) {
        onEmailStatusChange();
        snackBarTop(R.id.drawerLayout, message);

    }

    @Override
    public void onLogoutSuccess(String message) {
        String languageCode = Utility.getSharedPreferences(getAppContext(), Constant.LANGUAGE_CODE);
        Utility.clearSharedPreference(getAppContext());
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN, false);
        Utility.setSharedPreference(getAppContext(), Constant.LANGUAGE_CODE, languageCode);
        Intent intent = new Intent(mActivity, IntroductionActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getAppContext().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        getAppContext().finishAffinity();

    }

    @Override
    public void onCancelAccountSuccess(String message) {
        showProgressDialog(false);
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("status", "offline");
            hashMap.put("token", "logout");
            reference.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    dismissProgressDialog();

                    if (task.isSuccessful()) {
                        onLogoutSuccess(message);
                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            onLogoutSuccess(message);
        }

    }

    public void onChangeLanguageSuccess() {
        Intent intent = new Intent(mActivity, LanguageSettingsActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getAppContext().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        getAppContext().finish();

    }

    @Override
    public void onStartContactUsActivity() {
        Intent intent = new Intent(mActivity, ContactUsActivity.class);
        startActivity(intent);
        getAppContext().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);

    }

    @Override
    public void onStartBlockUserActivity() {
        Intent intent = new Intent(mActivity, BlockUserActivity.class);
        startActivity(intent);
        getAppContext().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    @Override
    public void onStartChangePasswordActivity() {
        Intent intent = new Intent(mActivity, ChangePasswordActivity.class);
        startActivity(intent);
        getAppContext().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    @Override
    public void onEmailStatusChange() {
        if (Utility.getSharedPreferencesBoolean(getAppContext(), Constant.IS_EMAIL_NOTIFICATION)) {

            toggleSwitch.setChecked(true);

        } else {

            toggleSwitch.setChecked(false);

        }

    }

    @Override
    public void onWebPageSuccess(String webUrl, String pageType) {
        /**
         * 1 => Payment Terms & Condition
         * 2 => Imprint
         * 3 => Privacy & Policy
         * 4 => Terms of Uses
         */
        AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
        if (pageType.equalsIgnoreCase("2")) {
            alert.setTitle(getResources().getString(R.string.txtImprint));

        } else if (pageType.equalsIgnoreCase("3")) {
            alert.setTitle(getResources().getString(R.string.txtPrivacyPolicy));

        } else if (pageType.equalsIgnoreCase("4")) {
            alert.setTitle(getResources().getString(R.string.txtTermsOfUse));

        }

        WebView wv = new WebView(mActivity);
        wv.loadUrl(webUrl);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.setView(wv);
        alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

//        alert.setNegativeButton(getResources().getString(R.string.txt_decline), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
        alert.show();
    }

    private void deleteFirebaseToken() {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("status", "offline");
            hashMap.put("token", "logout");

            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
