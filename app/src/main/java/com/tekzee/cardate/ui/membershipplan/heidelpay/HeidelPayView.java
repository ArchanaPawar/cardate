package com.tekzee.cardate.ui.membershipplan.heidelpay;

import android.app.Activity;

import com.heidelpay.android.ui.model.BICInput;
import com.heidelpay.android.ui.model.CardExpiryInput;
import com.heidelpay.android.ui.model.CreditCardInput;
import com.heidelpay.android.ui.model.CvvInput;
import com.heidelpay.android.ui.model.IBANInput;
import com.tekzee.cardate.ui.base.BaseView;

import org.json.JSONObject;

public interface HeidelPayView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onInsertPaymentSuccess(JSONObject data,String paymentId,String orderId);

    void makeCardPayment(CreditCardInput cardInput, CardExpiryInput expiryInput, CvvInput cvcInput);

    void heidelPayChargeFragmentDidFinish(boolean paymentStatus,String paymentId,String orderId);

    void onOurLocalServerSuccess(String message);

    void makeSepaDirectDebitPayment(IBANInput ibanInput, BICInput bicInput, String holder);

    void onTnCPageLoad(String generalUrl,String impressumUrl);


    void makePaypalPayment();
}