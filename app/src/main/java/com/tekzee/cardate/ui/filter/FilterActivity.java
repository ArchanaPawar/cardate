package com.tekzee.cardate.ui.filter;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.RadioButtonRegular;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.signup.CountryModel;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;
import com.xw.repo.BubbleSeekBar;

public class FilterActivity extends MvpActivity<FilterPresenter> implements FilterView {
    private static final String TAG = FilterActivity.class.getSimpleName();
    private BubbleSeekBar ageSlider;
    private TextViewRegular tvCountry;
    private TextViewRegular tvCity;
    private RadioGroup rgGender;
    private RadioGroup rgLocation;
    private String countryId = "";
    private String cityId = "";
    private RadioButtonRegular rb_male;
    private RadioButtonRegular rb_female;
    private RadioButtonRegular rb_near_me;
    private RadioButtonRegular rb_other_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        init();

        findViewById(R.id.toolbar_icon_navigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        findViewById(R.id.toolbar_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllFilter();
            }
        });

        findViewById(R.id.btnApplyFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * init gender
                 */
                initGender();
                Log.view(TAG, "Gender Id : " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_GENDER_ID));
                Log.view(TAG, "Gender Name: " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_GENDER));

                /**
                 * init age
                 */
                initAge();
                Log.view(TAG, "Age Min : " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_AGE_MIN));
                Log.view(TAG, "Age Max: " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_AGE_MAX));

                /**
                 * init location
                 */
                initLocation();
                Log.view(TAG, "Location Id : " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_LOCATION_ID));
                Log.view(TAG, "Location: " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_LOCATION));

                /**
                 * init country
                 */
                initCountry();
                Log.view(TAG, "Country Id : " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_COUNTRY_ID));
                Log.view(TAG, "Country: " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_COUNTRY));

                /**
                 * init city
                 */
                initCity();
                Log.view(TAG, "City Id : " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_CITY_ID));
                Log.view(TAG, "City: " + Utility.getSharedPreferences(getAppContext(), Constant.FILTER_CITY));

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "done");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

            }
        });


        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mvpPresenter.getAllCountry();
            }
        });


        tvCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countryId.equalsIgnoreCase("")) {

                    showInSnackBar(getAppContext().getResources().getString(R.string.alert_country_select));

                } else {
                    mvpPresenter.getAllCity(countryId);
                }


            }
        });
    }

    private void init() {
        /**
         * age
         */
        ageSlider = findViewById(R.id.ageSlider);
        if (!Utility.getSharedPreferences(getAppContext(), Constant.FILTER_AGE_MIN).equalsIgnoreCase("")) {
            ageSlider.setProgress(Float.parseFloat(Utility.getSharedPreferences(getAppContext(), Constant.FILTER_AGE_MAX)));
        }

        /**
         * Country
         */
        tvCountry = findViewById(R.id.tvCountry);
        countryId = Utility.getSharedPreferences(getAppContext(), Constant.FILTER_COUNTRY_ID);
        tvCountry.setText(Utility.getSharedPreferences(getAppContext(), Constant.FILTER_COUNTRY));

        /**
         * city
         */
        tvCity = findViewById(R.id.tvCity);
        cityId = Utility.getSharedPreferences(getAppContext(), Constant.FILTER_CITY_ID);
        tvCity.setText(Utility.getSharedPreferences(getAppContext(), Constant.FILTER_CITY));

        /**
         * gender
         */
        rgGender = findViewById(R.id.rgGender);
        rb_male = findViewById(R.id.rb_male);
        rb_female = findViewById(R.id.rb_female);
        if (Utility.getSharedPreferences(getAppContext(), Constant.FILTER_GENDER_ID).equalsIgnoreCase("1")) {
            rb_male.setChecked(true);
            rb_female.setChecked(false);
        } else if (Utility.getSharedPreferences(getAppContext(), Constant.FILTER_GENDER_ID).equalsIgnoreCase("2")) {

            rb_male.setChecked(false);
            rb_female.setChecked(true);


        } else {
            rb_male.setChecked(false);
            rb_female.setChecked(false);
        }

        /**
         * Location
         */
        rgLocation = findViewById(R.id.rgLocation);
        rb_near_me = findViewById(R.id.rb_near_me);
        rb_other_location = findViewById(R.id.rb_other_location);
        if (Utility.getSharedPreferences(getAppContext(), Constant.FILTER_LOCATION_ID).equalsIgnoreCase("1")) {
            rb_near_me.setChecked(true);
            rb_other_location.setChecked(false);
        } else if (Utility.getSharedPreferences(getAppContext(), Constant.FILTER_LOCATION_ID).equalsIgnoreCase("2")) {

            rb_near_me.setChecked(false);
            rb_other_location.setChecked(true);


        } else {
            rb_near_me.setChecked(false);
            rb_other_location.setChecked(false);
        }

    }

    @Override
    public void onCountrySelected(CountryModel model, String type) {
        if (type.equalsIgnoreCase("Country")) {
            tvCountry.setText(model.getName());
            countryId = model.getId();
            tvCity.setText("");
            cityId = "";
        } else {
            tvCity.setText(model.getName());
            cityId = model.getId();
        }


    }

    @Override
    protected FilterPresenter createPresenter() {
        return new FilterPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.filter_activity, message);
    }

    @Override
    public void initCountry() {
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY_ID, countryId);
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY, tvCountry.getText().toString().trim());
    }

    @Override
    public void initCity() {
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY_ID, cityId);
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY, tvCity.getText().toString().trim());
    }

    @Override
    public void initGender() {
        int selectedId = rgGender.getCheckedRadioButtonId();
        if (selectedId > 0) {
            RadioButton radioButton = findViewById(selectedId);
            String gender = radioButton.getText().toString();
            if (gender.equalsIgnoreCase(getResources().getString(R.string.txt_male))) {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "1");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, getResources().getString(R.string.txt_male));

            } else if (gender.equalsIgnoreCase(getResources().getString(R.string.txt_female))) {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "2");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, getResources().getString(R.string.txt_female));
            } else {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, "");
            }

        }
    }

    @Override
    public void initAge() {
        int ageMin = (int) ageSlider.getMin();
        int ageMax = (int) ageSlider.getProgress();

        if (ageMax == ageMin) {

            Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MIN, "");
            Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MAX, "");

        } else {

            Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MIN, "" + ageSlider.getMin());
            Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MAX, "" + ageSlider.getProgress());
        }

    }

    @Override
    public void initLocation() {
        int selectedId = rgLocation.getCheckedRadioButtonId();
        if (selectedId > 0) {
            RadioButton radioButton = findViewById(selectedId);
            String gender = radioButton.getText().toString();
            if (gender.equalsIgnoreCase(getResources().getString(R.string.txt_near_me))) {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "1");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, getResources().getString(R.string.txt_near_me));

            } else if (gender.equalsIgnoreCase(getResources().getString(R.string.txt_other_location))) {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "2");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, getResources().getString(R.string.txt_other_location));
            } else {

                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "");
                Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, "");
            }

        }
    }

    @Override
    public void resetAllFilter() {
        /**
         * gender
         */
        rgGender.clearCheck();
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, "");

        /**
         * age
         */
        ageSlider.setProgress(ageSlider.getMin());
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MIN, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MAX, "");

        /**
         * location
         */
        rgLocation.clearCheck();
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, "");

        /**
         * Country
         */
        tvCountry.setText("");
        countryId = "";
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY, "");

        /**
         * City
         */
        tvCity.setText("");
        cityId = "";
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY, "");

    }
}
