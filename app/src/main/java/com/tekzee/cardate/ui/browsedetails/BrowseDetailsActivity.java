package com.tekzee.cardate.ui.browsedetails;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.chat.message.MessageActivity;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;
import com.tekzee.cardate.ui.navigation.MyFirebaseUser;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.MyCustomDialog;

import org.json.JSONObject;

import cn.refactor.lib.colordialog.ColorDialog;
import cn.refactor.lib.colordialog.PromptDialog;

public class BrowseDetailsActivity extends MvpActivity<BrowseDetailsPresenter> implements BrowseDetailsView {
    private static final String TAG = BrowseDetailsActivity.class.getSimpleName();
    private ImageView ivAddUser;
    private ImageView ivProfile;
    private TextViewRegular tvAddUser;
    private TextViewRegular tvUserName;
    private String friendStatus = "";
    private String mFirebaseUserId = "";
    private String userEmailAddress = "";
    private String profileImageUrl = "";
    private boolean isFromBlockCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.toolbar_icon_navigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });


        ivAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "";
                String userName = tvUserName.getText().toString();

                if (friendStatus.equalsIgnoreCase("0")) {
                    message = getResources().getString(R.string.add_friend1) + " " + userName + " " + getResources().getString(R.string.add_friend2);

                } else if (friendStatus.equalsIgnoreCase("1")) {
                    message = getResources().getString(R.string.block_friend1) + " " + userName + " " + getResources().getString(R.string.block_friend2);

                } else if (friendStatus.equalsIgnoreCase("2")) {
                    message = getResources().getString(R.string.confirming1) + " " + userName + " " + getResources().getString(R.string.confirming2);

                } else {


                }

                ColorDialog dialog = new ColorDialog(getAppContext());
                dialog.setColor(Color.parseColor("#666666"));
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setContentText(message);
                dialog.setPositiveListener(getResources().getString(R.string.yes), new ColorDialog.OnPositiveListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();

                        if (friendStatus.equalsIgnoreCase("0")) {
                            mvpPresenter.sendFriendRequest(getIntent().getExtras());

                        } else if (friendStatus.equalsIgnoreCase("1")) {
                            mvpPresenter.blockUser(getIntent().getExtras());

                        } else if (friendStatus.equalsIgnoreCase("2")) {
                            mvpPresenter.acceptFriendRequest(getIntent().getExtras());

                        }

                    }
                })
                        .setNegativeListener(getResources().getString(R.string.no), new ColorDialog.OnNegativeListener() {
                            @Override
                            public void onClick(ColorDialog dialog) {

                                dialog.dismiss();
                            }
                        });

                if (!message.equalsIgnoreCase("")) {

                    dialog.show();
                }


            }
        });

        findViewById(R.id.rlFooter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * check is user blocked
                 */
                mvpPresenter.isUserBlocked(getIntent().getExtras().getString("id"));

            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getAppContext(), DocumentViewerActivity.class);
                intent.putExtra("title", tvUserName.getText().toString());
                intent.putExtra("imageUrl", profileImageUrl);
                startActivity(intent);
            }
        });

        mvpPresenter.getBrowseDetails(getIntent().getExtras());
    }

    private void init() {
        ivProfile = findViewById(R.id.ivProfile);
        ivAddUser = findViewById(R.id.ivAddUser);
        tvAddUser = findViewById(R.id.tvAddUser);
        tvUserName = findViewById(R.id.tvUserName);

        initFirebaseUserId();
    }

    @Override
    protected BrowseDetailsPresenter createPresenter() {
        return new BrowseDetailsPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void showInSnackBar(String message) {

        snackBarTop(R.id.browse_details_activity, message);

    }

    @Override
    public void onProfileDataSuccess(JSONObject jsonObject) {
        try {
            userEmailAddress = jsonObject.getString("email");
            tvUserName.setText(jsonObject.getString("name"));
            ((TextViewRegular) findViewById(R.id.tvAge)).setText(jsonObject.getString("age") + " " + jsonObject.getString("age_unit"));
            ((TextViewRegular) findViewById(R.id.tvCountry)).setText(jsonObject.getString("country"));
            ((TextViewRegular) findViewById(R.id.tvDistance)).setText(jsonObject.getString("distance") + " " + jsonObject.getString("distance_unit"));
            ((TextViewRegular) findViewById(R.id.tvOccupation)).setText(jsonObject.getString("occupation"));
            ((TextViewRegular) findViewById(R.id.tvAboutMe)).setText(jsonObject.getString("about_me"));
            friendStatus = jsonObject.getString("friendStatus");

            profileImageUrl = jsonObject.getString("image");
            if (!profileImageUrl.equalsIgnoreCase("")) {
                Glide.with(getAppContext()).load(profileImageUrl).into(ivProfile);
            }

            if (friendStatus.equalsIgnoreCase("0")) {
                tvAddUser.setText(getResources().getString(R.string.send_request));
                ivAddUser.setImageResource(R.drawable.ic_person_add_red);

            } else if (friendStatus.equalsIgnoreCase("1")) {
                tvAddUser.setText(getResources().getString(R.string.block_user));
                ivAddUser.setImageResource(R.drawable.ic_block_user);

            } else if (friendStatus.equalsIgnoreCase("2")) {
                tvAddUser.setText(getResources().getString(R.string.respond));
                ivAddUser.setImageResource(R.drawable.arrowgreen);

            } else if (friendStatus.equalsIgnoreCase("3")) {
                tvAddUser.setText(getResources().getString(R.string.request_send));
                ivAddUser.setImageResource(R.drawable.ic_person_add_green);


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showDialog(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                    }
                }).show();

    }

    @Override
    public void onUserBlocked(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();


                        mvpPresenter.getBrowseDetails(getIntent().getExtras());
                    }
                }).show();

    }

    @Override
    public void userNotBlocked() {
        if (friendStatus.equalsIgnoreCase("1") || friendStatus.equalsIgnoreCase("0")) {
            if (mFirebaseUserId != null && !mFirebaseUserId.equalsIgnoreCase("")) {
                Intent intent = new Intent(getAppContext(), MessageActivity.class);
                intent.putExtra("userid", mFirebaseUserId);
                startActivity(intent);
            } else {
                isFromBlockCheck = true;
                initFirebaseUserId();
            }

        } else {

            showInSnackBar(getResources().getString(R.string.need_to_be_a_friend));

        }
    }

    private void initFirebaseUserId() {
        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    try {
                        MyFirebaseUser user = snapshot.getValue(MyFirebaseUser.class);

                        if (user.getEmail() != null) {

                            if (user.getEmail().equalsIgnoreCase(userEmailAddress)) {
                                mFirebaseUserId = user.getId();
                                break;

                            }

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                if(isFromBlockCheck){
                    userNotBlocked();
                    isFromBlockCheck = false;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
