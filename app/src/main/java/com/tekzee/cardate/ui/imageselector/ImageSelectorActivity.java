package com.tekzee.cardate.ui.imageselector;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.profile.ImageUrl;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;

public class ImageSelectorActivity extends AppCompatActivity {
    private Activity mActivity;
    private RecyclerView rvContent;
    private ArrayList<ImageUrl> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_selector);
        mActivity = this;
        init();
    }

    private void init() {
        rvContent = findViewById(R.id.rvContent);
        rvContent.setLayoutManager(new GridLayoutManager(mActivity,2));
        arrayList = (ArrayList<ImageUrl>) getIntent().getSerializableExtra("ImageUrl");
        rvContent.setAdapter(new ImageAdapter(arrayList, new ImageAdapter.RowSelect() {
            @Override
            public void onSelect(ImageUrl model) {
                Utility.setSharedPreferenceBoolean(mActivity, Constant.FROM_IMAGESELECOR,true);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("image_url",model.getImageUrl());
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        }));
    }
}
