package com.tekzee.cardate.ui.browse;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.browsedetails.BrowseDetailsActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;


public class BrowseFragment extends MvpFragment<BrowsePresenter> implements BrowseView, SwipeRefreshLayout.OnRefreshListener, BrowserAdapter.RowSelect {
    private static final String TAG = BrowseFragment.class.getSimpleName();
    final Handler handler = new Handler();
    private int LOCATION_RETRY_COUNT;
    private RecyclerView rvBrowseData;
    private SwipeRefreshLayout swipeContainer;
    private TextViewBold tvNotFound;
    private ArrayList<BrowseModel> arrayList = new ArrayList<>();
    private BrowserAdapter adapter;


    public static BrowseFragment newInstance() {
        return new BrowseFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_browse, container, false);
        init(mView);
        onRefresh();
        return mView;

    }

    private void init(View mView) {
        tvNotFound = mView.findViewById(R.id.tvNotFound);
        swipeContainer = mView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        rvBrowseData = mView.findViewById(R.id.rvBrowseData);
        rvBrowseData.setLayoutManager(new GridLayoutManager(getAppContext(), 2));
        adapter = new BrowserAdapter(arrayList, this);
        rvBrowseData.setAdapter(adapter);
    }


    @Override
    protected BrowsePresenter createPresenter() {
        return new BrowsePresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        swipeContainer.setRefreshing(false);
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        swipeContainer.setRefreshing(false);
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onBrowseDataFail(String message) {
        swipeContainer.setRefreshing(false);
        ((NavigationActivity) getActivity()).populateSideMenu();
        showInPopup(message);


        arrayList.clear();
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onBrowseDataSuccess(ArrayList<BrowseModel> modelArrayList) {
        swipeContainer.setRefreshing(false);
        tvNotFound.setVisibility(View.GONE);
        try {
            arrayList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        arrayList.addAll(modelArrayList);
        adapter.notifyDataSetChanged();

        ((NavigationActivity) getActivity()).populateSideMenu();

    }

    @Override
    public void onStartBrowseDetailsActivity(BrowseModel model) {
        Location mLocation = ((NavigationActivity) getAppContext()).getLocation();
        if (mLocation != null) {
            Intent intent = new Intent(getAppContext(), BrowseDetailsActivity.class);
            intent.putExtra("lat", mLocation.getLatitude());
            intent.putExtra("long", mLocation.getLongitude());
            intent.putExtra("id", model.getId());
            startActivity(intent);

        } else {
            onToast(getResources().getString(R.string.location_not_found));
        }

    }

    private void getBrowseData() {
        try {
            Location mLocation = ((NavigationActivity) getAppContext()).getLocation();
            if (mLocation != null) {
                swipeContainer.setRefreshing(true);
                mvpPresenter.getBrowseData(mLocation);
                LOCATION_RETRY_COUNT = 0;

            } else {

                if (LOCATION_RETRY_COUNT < 10) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            getBrowseData();
                            LOCATION_RETRY_COUNT++;

                        }
                    }, 2000);

                    Log.view(TAG, "LOCATION_RETRY_COUNT : " + LOCATION_RETRY_COUNT);

                } else {

                    onBrowseDataFail(getString(R.string.not_found));

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void applyFilter() {

        getBrowseData();
    }

    @Override
    public void onRefresh() {
        swipeContainer.setRefreshing(true);
        getBrowseData();
    }

    @Override
    public void onSelect(BrowseModel model) {
        onStartBrowseDetailsActivity(model);
    }
}
