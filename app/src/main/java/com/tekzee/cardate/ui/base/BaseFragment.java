package com.tekzee.cardate.ui.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.KeyboardUtils;
import com.tekzee.cardate.utils.SnackbarUtils;
import com.tekzee.cardate.utils.Utility;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public class BaseFragment extends Fragment implements BaseView {
    public ProgressDialog progressDialog;
    public Activity mActivity;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public ProgressDialog showProgressDialog(boolean cancelable) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.txt_pleae_wait));
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
        return progressDialog;

    }

    @Override
    public ProgressDialog showProgressDialog(CharSequence message, boolean cancelable) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
        return progressDialog;
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return Utility.isConnectingToInternet();
    }

    @Override
    public void snackBarBottom(int view_id, String message) {
        SnackbarUtils.snackBarBottom(getActivity().findViewById(view_id), message);

    }

    @Override
    public void snackBarTop(int view_id, String message) {
        SnackbarUtils.snackBarTop(getActivity().findViewById(view_id), message);
    }


    @Override
    public void hideSoftKeyboard() {
        KeyboardUtils.hideSoftInput(getActivity());
    }

    @Override
    public void showSoftKeyboard(EditText editText) {
        KeyboardUtils.showSoftInput(editText, getActivity());
    }

    @Override
    public void onToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void restartApp() {
        String languageCode = Utility.getSharedPreferences(mActivity,Constant.LANGUAGE_CODE);
        Utility.clearSharedPreference(getActivity());
        Utility.setSharedPreferenceBoolean(getActivity(), Constant.IS_LOGIN, false);
        Utility.setSharedPreference(mActivity, Constant.LANGUAGE_CODE, languageCode);
        Intent intent = new Intent(mActivity, IntroductionActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        getActivity().finishAffinity();

    }

//    @Override
//    public String getFileStorageLocation(int fileType, boolean isSent) {
//        /**
//         * 0 = Audio
//         */
//        String location = "";
//        switch (fileType) {
//            case 0:
//                if (isSent) {
//                    File mFile = new File(Environment.getExternalStorageDirectory() + "/CarDate/Audio/Sent");
//                    if (!mFile.exists()) {
//                        mFile.mkdirs();
//                    }
//
//                    location = mFile.getAbsolutePath();
//                }
//
//
//        }
//
//        return location;
//    }


}
