package com.tekzee.cardate.ui.searchbycar;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface SearchByCarView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onSearchResult(String key, ArrayList<SearchByCarModel> arrayList);

    void onSearchFail(String message);

}
