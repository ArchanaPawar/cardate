package com.tekzee.cardate.ui.searchbycar;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;

import java.util.ArrayList;

public class SearchByCarAdapter extends RecyclerView.Adapter<SearchByCarAdapter.ViewHolder> {
    private ArrayList<SearchByCarModel> arrayList;
    private Click listener;
    private Context mContext;

    public SearchByCarAdapter(ArrayList<SearchByCarModel> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onCLick(SearchByCarModel model);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_search_by_car, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private CircleImageView ivProfileImage;
        private TextViewRegular tvProfileName;
        private TextViewRegular tvAgeCity;
        private TextViewBold tvCarNumber;
        private ImageView ivOnline;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cvRoot = itemView.findViewById(R.id.cvRoot);
            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvAgeCity = itemView.findViewById(R.id.tvAgeCity);
            ivOnline = itemView.findViewById(R.id.ivOnline);
            tvCarNumber = itemView.findViewById(R.id.tvCarNumber);

        }

        public void bind(final SearchByCarModel model, final Click listener) {
            tvProfileName.setText(model.getName());
            tvCarNumber.setText(model.getCarNumber());
            tvAgeCity.setText(model.getAge());

            /**
             * check online
             */
            if (model.getIsOnline().equalsIgnoreCase("1")) {
                ivOnline.setVisibility(View.VISIBLE);
            } else {
                ivOnline.setVisibility(View.GONE);
            }

            /**
             * profile image
             */
            String imageUrl = model.getImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivProfileImage);
            }

            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(model.getName(), model.getImage());
                }
            });


            cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(model);
                }
            });

        }

        public void bigImageView(String title, String imageUrl) {
            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("imageUrl", imageUrl);
            mContext.startActivity(intent);

        }
    }
}
