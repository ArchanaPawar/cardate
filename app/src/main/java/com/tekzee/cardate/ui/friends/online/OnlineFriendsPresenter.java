package com.tekzee.cardate.ui.friends.online;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OnlineFriendsPresenter extends BasePresenter<OnlineFriendsView> {
    private static final String TAG = OnlineFriendsPresenter.class.getSimpleName();

    public OnlineFriendsPresenter(OnlineFriendsView view, Context mContext) {

        attachView(view,mContext);
    }

    public void getOnline(String filter) {
        if (Utility.isConnectingToInternet()) {
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("filter", filter);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getAllFriends(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<OnlineFriendModel> arrayList = new ArrayList<>();
                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject object = mJsonArray.getJSONObject(i);
                                arrayList.add(new OnlineFriendModel(
                                        object.getString("id"),
                                        object.getString("date_of_birth"),
                                        object.getString("image"),
                                        object.getString("name"),
                                        object.getString("age"),
                                        object.getString("city"),
                                        object.getString("city_id"),
                                        object.getString("is_online")

                                ));
                            }

                            mvpView.onPostSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.onPostFail(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
