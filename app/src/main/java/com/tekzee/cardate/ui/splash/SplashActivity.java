package com.tekzee.cardate.ui.splash;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.language.LanguageSettingsActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

import cn.refactor.lib.colordialog.PromptDialog;

public class SplashActivity extends MvpActivity<SplashPresenter> implements SplashView {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private LinearLayout ll_no_internet;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setUpFullScreen();

        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.FROM_IMAGESELECOR, false);
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_SOCIAL, false);

        ll_no_internet = findViewById(R.id.ll_no_internet);
        ll_no_internet.setVisibility(View.GONE);

        findViewById(R.id.btn_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (Utility.isConnectingToInternet()) {
                            getFCMToken();
                            ll_no_internet.setVisibility(View.GONE);
                        } else {
                            ll_no_internet.setVisibility(View.VISIBLE);
                        }

                        dismissProgressDialog();


                    }
                }, 3000);


            }
        });


        setApplicationLanguage();


    }

    private void setUpFullScreen() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    protected SplashPresenter createPresenter() {
        return new SplashPresenter(this, this);
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);


    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.splash_activity, message);

    }

    @Override
    public void onNoInternetConnection() {
        showInSnackBar(getString(R.string.internet_error));
        ll_no_internet.setVisibility(View.VISIBLE);

    }

    @Override
    public void onStartIntroductionActivity() {
        startActivity(new Intent(getAppContext(), IntroductionActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onStartNavigationActivity() {
        /**
         * if user logout form friebase than will logout from app too
         */
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mFirebaseUser != null) {

            startActivity(new Intent(getAppContext(), NavigationActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();

        } else {

            mvpPresenter.logoutFromApp();


        }


    }

    @Override
    public void onStartLanguageSettingsActivity() {
        startActivity(new Intent(getAppContext(), LanguageSettingsActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void showUpdatePopup(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();

                        /**
                         * Re-Direct user to play store
                         */
                        final String appPackageName = getPackageName(); // package name of the app
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            finish();
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                }).show();

    }

    @Override
    public void onLogoutSuccess(String message) {

        String languageCode = Utility.getSharedPreferences(getAppContext(), Constant.LANGUAGE_CODE);
        Utility.clearSharedPreference(getAppContext());
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN, false);
        Utility.setSharedPreference(getAppContext(), Constant.LANGUAGE_CODE, languageCode);
        onStartIntroductionActivity();

    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            onToast(getResources().getString(R.string.get_instance_id_failed) + task.getException());
                            Log.view(TAG, "getInstanceId failed : " + task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        final String token = task.getResult().getToken();
                        Log.view(TAG, "Auth Token: " + Utility.getSharedPreferences(getAppContext(), Constant.AUTH_TOKEN));


                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                updateToken(token);
                                mvpPresenter.validateAppVersion(token);
                                resetAllFilter();
                            }
                        }, 2000);

                    }
                });

    }


    private void updateToken(String refreshToken) {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("token", refreshToken);
            hashMap.put("userId", Utility.getSharedPreferences(getAppContext(), Constant.USER_ID));
            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void resetAllFilter() {
        /**
         * gender
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, "");

        /**
         * age
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MIN, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MAX, "");

        /**
         * location
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, "");

        /**
         * Country
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY, "");

        /**
         * City
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY, "");

    }

    private void setApplicationLanguage() {
        try {
            Utility.changeAppLanguage(mActivity);
            if (Utility.isConnectingToInternet()) {
                getFCMToken();
                ll_no_internet.setVisibility(View.GONE);
            } else {
                ll_no_internet.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
