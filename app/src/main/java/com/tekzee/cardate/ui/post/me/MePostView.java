package com.tekzee.cardate.ui.post.me;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;


public interface MePostView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<MePostModel> arrayList);

    void onPostFail(String message);
}
