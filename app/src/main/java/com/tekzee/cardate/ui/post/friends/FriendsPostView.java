package com.tekzee.cardate.ui.post.friends;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;


public interface FriendsPostView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<FriendPostModel> arrayList);

    void onPostFail(String message);
}
