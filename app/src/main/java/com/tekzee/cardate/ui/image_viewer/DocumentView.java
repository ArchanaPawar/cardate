package com.tekzee.cardate.ui.image_viewer;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;


public interface DocumentView extends BaseView {
    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);
}
