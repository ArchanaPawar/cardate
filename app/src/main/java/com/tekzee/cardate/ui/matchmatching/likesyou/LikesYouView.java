package com.tekzee.cardate.ui.matchmatching.likesyou;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface LikesYouView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onLikesYouSuccess(ArrayList<LikesYouModel> arrayList);
}
