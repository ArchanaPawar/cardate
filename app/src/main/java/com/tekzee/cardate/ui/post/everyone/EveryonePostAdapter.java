package com.tekzee.cardate.ui.post.everyone;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;

public class EveryonePostAdapter extends RecyclerView.Adapter<EveryonePostAdapter.ViewHolder> {
    private ArrayList<EveryOneModel> arrayList;
    private Click listener;
    private Context mContext;

    public EveryonePostAdapter(ArrayList<EveryOneModel> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onProfileCLick(EveryOneModel model);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_everyone_post, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfileImage;
        private TextViewRegular tvProfileName;
        private TextViewRegular tvPostTime;
        private ImageView ivPost;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvPostTime = itemView.findViewById(R.id.tvPostTime);
            ivPost = itemView.findViewById(R.id.ivPost);
        }

        public void bind(final EveryOneModel model, final Click listener) {
            tvProfileName.setText(model.getUserName());
            tvProfileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProfileCLick(model);

                }
            });
//            tvPostTime.setText(Utility.covertTimeToText(mContext,model.getCreatedAt()));
            tvPostTime.setText(Utility.changeDateFormat(model.getCreatedAt()));

            tvPostTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProfileCLick(model);
                }
            });

            /**
             * profile image
             */
            String imageUrl = model.getProfileImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivProfileImage);
            }
            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(model.getUserName(), model.getProfileImage());
                }
            });

            /**
             * post image
             */

            String postImageUrl = model.getPostImage();
            if (postImageUrl != null && !postImageUrl.equalsIgnoreCase("")) {

                Glide.with(mContext).load(postImageUrl).into(ivPost);

            } else {

//                llTextView.setVisibility(View.VISIBLE);
            }


            ivPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(model.getUserName(), model.getPostImage());
                }
            });

        }

        public void bigImageView(String title, String imageUrl) {
            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("imageUrl", imageUrl);
            mContext.startActivity(intent);

        }
    }
}
