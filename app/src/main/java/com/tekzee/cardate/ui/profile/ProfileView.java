package com.tekzee.cardate.ui.profile;
import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import org.json.JSONObject;

import java.util.ArrayList;
public interface ProfileView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void onUnsubscribePlan(String message);

    void showInSnackBar(String message);

    void onProfileDataSuccess(JSONObject data);

    String getAboutMe();

    String getProfileName();

    void setProfileName(String name);

    String getOccupation();

    ArrayList<String> getInterestIs();

    String getCompanyName();

    String getCarNumber();

    String getCountryName();

    String getCityName();

    void updateInterested(ArrayList<InterestModel> arrayList);

    void onInstagramImageLoaded(ArrayList<ImageUrl> arrayList);
}
