package com.tekzee.cardate.ui.matchmatching.request;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface RequestView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onUserFound(ArrayList<RequestModel> arrayList);
}
