package com.tekzee.cardate.ui.matchmatching.likesyou;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;

import java.util.ArrayList;

public class LikesYouAdapter extends RecyclerView.Adapter<LikesYouAdapter.ViewHolder> {
    private ArrayList<LikesYouModel> arrayList;
    private Click listener;
    private Context mContext;

    public LikesYouAdapter(ArrayList<LikesYouModel> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onCLick();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_likes_you, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfileImage;
        private TextViewRegular tvProfileName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
        }

        public void bind(final LikesYouModel model, Click listener) {
            tvProfileName.setText(model.getName());

            String imageUrl = model.getImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivProfileImage);
            }

            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(model.getName(),model.getImage());
                }
            });

        }

        public void bigImageView(String title, String imageUrl) {
            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
            intent.putExtra("title",title);
            intent.putExtra("imageUrl",imageUrl);
            mContext.startActivity(intent);

        }
    }
}
