package com.tekzee.cardate.ui.forgotpassword;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.login.LoginActivity;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import cn.refactor.lib.colordialog.PromptDialog;

public class ForgotPasswordActivity extends MvpActivity<ForgotPasswordPresenter> implements ForgotPasswordView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        findViewById(R.id.toolbar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartLoginActivity();
            }
        });

        findViewById(R.id.txt_remember_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartLoginActivity();
            }
        });

        findViewById(R.id.btn_get_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getEmail().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_email));

                } else if (!Utility.emailValidator(getEmail())) {
                    showInSnackBar(getResources().getString(R.string.alert_invalid_email));

                } else {

                    mvpPresenter.getPassword();

                }

            }
        });


    }

    @Override
    protected ForgotPasswordPresenter createPresenter() {
        return new ForgotPasswordPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showAlert(String message) {

        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                        onStartLoginActivity();
                    }
                }).show();

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.activity_forgot_password, message);
    }

    @Override
    public void onStartLoginActivity() {
        startActivity(new Intent(getAppContext(), LoginActivity.class));
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        finish();

    }

    @Override
    public String getEmail() {
        return ((EditTextRegular) findViewById(R.id.et_email)).getText().toString().trim();
    }

    @Override
    public void onBackPressed() {

        onStartLoginActivity();

    }
}
