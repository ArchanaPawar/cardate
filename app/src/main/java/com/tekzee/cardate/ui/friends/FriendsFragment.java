package com.tekzee.cardate.ui.friends;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.friends.all.AllFriendsFragment;
import com.tekzee.cardate.ui.friends.online.OnlineFriendsFragment;
import com.tekzee.cardate.ui.friends.request.RequestFriendFragment;

public class FriendsFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    int[] unreadCount = {0, 0, 0};
    private FriendsPagerAdapter adapter;
    private String[] tabTitle;
    public static FriendsFragment newInstance() {
        return new FriendsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_friends, container, false);
        init(mView);

        return mView;

    }

    private void init(View mView) {
        tabTitle = getResources().getStringArray(R.array.title_friend_tab);

        //Initializing viewPager
        viewPager = mView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        //Initializing the tablayout
        tabLayout = mView.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        try {
            setupTabIcons();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new FriendsPagerAdapter(getChildFragmentManager());
        adapter.addFragment(AllFriendsFragment.newInstance(), tabTitle[0]);
        adapter.addFragment(OnlineFriendsFragment.newInstance(), tabTitle[1]);
        adapter.addFragment(RequestFriendFragment.newInstance(), tabTitle[2]);
        viewPager.setAdapter(adapter);
    }


    private void setupTabIcons() {

        for (int i = 0; i < tabTitle.length; i++) {

            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }


    }

    private View prepareTabView(int pos) {
        View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
        TextViewRegular tv_title = view.findViewById(R.id.tv_title);
        TextViewRegular tv_count = view.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if (unreadCount[pos] > 0) {
            tv_count.setVisibility(View.VISIBLE);
            tv_count.setText("" + unreadCount[pos]);
        } else
            tv_count.setVisibility(View.GONE);


        return view;
    }
}
