package com.tekzee.cardate.ui.chat.chat;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface ChatView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onUserBlocked(String message);

    void userNotBlocked(String blockUserId);

}
