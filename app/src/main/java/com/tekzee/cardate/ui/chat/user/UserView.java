package com.tekzee.cardate.ui.chat.user;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface UserView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

}
