package com.tekzee.cardate.ui.browse;

import android.content.Context;
import android.location.Location;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BrowsePresenter extends BasePresenter<BrowseView> {
    private static final String TAG = BrowsePresenter.class.getSimpleName();

    public BrowsePresenter(BrowseView view, Context mContext) {

        attachView(view,mContext);
    }

    public void getBrowseData(Location mLocation) {
        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_lat", mLocation.getLatitude());
                input.put("user_long", mLocation.getLongitude());
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("country_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_COUNTRY_ID));
                input.put("city_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_CITY_ID));
                input.put("gender", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_GENDER_ID));
                input.put("min", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_AGE_MIN));
                input.put("max", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_AGE_MAX));
                input.put("nearby_me", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FILTER_LOCATION_ID));
                input.put("is_filter", "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);


            Log.view(TAG, "Bearer Token : " + Utility.getSharedPreferences(mvpView.getAppContext(),Constant.AUTH_TOKEN));
            addSubscription(apiStores.nearbyMe(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<BrowseModel> arrayList = new ArrayList<>();

                            JSONObject countObject = jsonObject.getJSONObject("data").getJSONObject("counts");

                            Utility.setSharedPreference(mvpView.getAppContext(),Constant.FRIEND_COUNT, countObject.getString("friend_count"));
                            Utility.setSharedPreference(mvpView.getAppContext(),Constant.MATCH_COUNT, countObject.getString("match_count"));

                            JSONArray userJsonArray = jsonObject.getJSONObject("data").getJSONArray("users");
                            for (int i = 0; i < userJsonArray.length(); i++) {
                                JSONObject dataObject = userJsonArray.getJSONObject(i);
                                arrayList.add(new BrowseModel(
                                        dataObject.getString("id"),
                                        dataObject.getString("name"),
                                        dataObject.getString("image"),
                                        dataObject.getString("country").toString(),
                                        dataObject.getString("date_of_birth").toString(),
                                        dataObject.getString("country_id"),
                                        dataObject.getString("distance"),
                                        dataObject.getString("age"),
                                        (dataObject.getString("is_online").equalsIgnoreCase("1"))
                                ));

                            }

                            mvpView.onBrowseDataSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.onBrowseDataFail(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }
}
