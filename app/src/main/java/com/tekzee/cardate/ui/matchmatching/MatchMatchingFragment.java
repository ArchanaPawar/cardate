package com.tekzee.cardate.ui.matchmatching;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.NonSwipeableViewPager;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.matchmatching.likesyou.LikesYouFragment;
import com.tekzee.cardate.ui.matchmatching.matches.MatchesFragment;
import com.tekzee.cardate.ui.matchmatching.request.RequestFragment;

public class MatchMatchingFragment extends Fragment {
    private TabLayout tabLayout;
    public static NonSwipeableViewPager viewPager;
    String[] tabTitle;
    int[] unreadCount = {0, 0, 0};
    private ViewPagerAdapter adapter;


    public static MatchMatchingFragment newInstance() {
        return new MatchMatchingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_match_matching, container, false);
        init(mView);


        return mView;

    }

    private void init(View mView) {
        tabTitle = getResources().getStringArray(R.array.title_matches_tab);
        /**
         * Initializing viewPager
         */
        viewPager = mView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        /**
         * Initializing the tab layout
         */
        tabLayout = mView.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        try {
            setupTabIcons();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    public void goToFirst(){
////        TabLayout.Tab tab = tabLayout.getTabAt(0);
////        tab.select();
////        setupViewPager(viewPager);
//
////        MatchMatchingFragment.newInstance();
//
////        adapter.addFragment(RequestFragment.newInstance(), tabTitle[0]);
//
//        viewPager.setCurrentItem(0);
//    }

    private void setupViewPager(NonSwipeableViewPager viewPager) {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(RequestFragment.newInstance(), tabTitle[0]);
        adapter.addFragment(LikesYouFragment.newInstance(), tabTitle[1]);
        adapter.addFragment(MatchesFragment.newInstance(), tabTitle[2]);
        viewPager.setAdapter(adapter);
    }


    private void setupTabIcons() {
        for (int i = 0; i < tabTitle.length; i++) {
            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }


    }


    private View prepareTabView(int pos) {
        View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
        TextViewRegular tv_title = view.findViewById(R.id.tv_title);
        TextViewRegular tv_count = view.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if (unreadCount[pos] > 0) {
            tv_count.setVisibility(View.VISIBLE);
            tv_count.setText("" + unreadCount[pos]);
        } else
            tv_count.setVisibility(View.GONE);


        return view;
    }

}
