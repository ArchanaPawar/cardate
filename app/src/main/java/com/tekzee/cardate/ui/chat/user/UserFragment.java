package com.tekzee.cardate.ui.chat.user;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.chat.model.User;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;
import java.util.List;

public class UserFragment extends MvpFragment<UserPresenter> implements UserView {
    private RecyclerView recyclerViewData;
//    private UserAdapter userAdapter;
    private List<User> mUsers;
    private EditTextRegular search_users;

    public static UserFragment newInstance() {
        return new UserFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chat_user, container, false);
        init(mView);
        mUsers = new ArrayList<>();
        readUsers();

        search_users = mView.findViewById(R.id.search_users);
        search_users.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchUsers(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return mView;

    }

    private void init(View mView) {
        recyclerViewData = mView.findViewById(R.id.recyclerViewData);
        recyclerViewData.setHasFixedSize(true);
        recyclerViewData.setLayoutManager(new LinearLayoutManager(getAppContext()));

    }

    @Override
    protected UserPresenter createPresenter() {
        return new UserPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    private void searchUsers(String s) {
        final FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).orderByChild(Constant.FIREBASE_SEARCH)
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);

                    assert user != null;
                    assert fuser != null;
                    if (!user.getId().equals(fuser.getUid())){
                        mUsers.add(user);
                    }
                }

//                userAdapter = new UserAdapter(getContext(), mUsers, false,this);
//                recyclerViewData.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void readUsers() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (search_users.getText().toString().equals("")) {
                    mUsers.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = snapshot.getValue(User.class);

                        assert user != null;
                        if(user.getId() != null){

                            assert firebaseUser != null;
                            if (!user.getId().equals(firebaseUser.getUid())) {
                                mUsers.add(user);
                            }
                        }

                    }

//                    userAdapter = new UserAdapter(getContext(), mUsers, false);
//                    recyclerViewData.setAdapter(userAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
