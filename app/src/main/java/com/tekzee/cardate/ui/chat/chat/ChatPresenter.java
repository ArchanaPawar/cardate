package com.tekzee.cardate.ui.chat.chat;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class ChatPresenter extends BasePresenter<ChatView> {
    private static final String TAG = ChatPresenter.class.getSimpleName();

    public ChatPresenter(ChatView view, Context mContext) {

        attachView(view,mContext);
    }

    public void isUserBlocked(final String UserId,final String blockUserId ) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("blockuser_id", blockUserId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.checkBlockUser(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onUserBlocked(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.userNotBlocked(UserId);

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
