package com.tekzee.cardate.ui.post.everyone;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.browsedetails.BrowseDetailsActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;

public class EveryonePostFragment extends MvpFragment<EveryonePostPresenter> implements EveryonePostView, SwipeRefreshLayout.OnRefreshListener, EveryonePostAdapter.Click {
    private static final String TAG = EveryonePostFragment.class.getSimpleName();
    private RecyclerView rvEveryOnePost;
    private SwipeRefreshLayout swipeContainer;
    private TextViewBold tvNotFound;
    private ArrayList<EveryOneModel> arrayList = new ArrayList<>();
    private EveryonePostAdapter adapter;
    private boolean mUserSeen = false;
    private boolean mViewCreated = false;

    public static EveryonePostFragment newInstance() {
        return new EveryonePostFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_every_one_post, container, false);
        init(mView);
        return mView;

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!mUserSeen && isVisibleToUser) {
            mUserSeen = true;
            tryViewCreatedFirstSight();
        }
        onUserVisibleChanged(isVisibleToUser);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewCreated = false;
        mUserSeen = false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewCreated = true;
        tryViewCreatedFirstSight();
    }


    private void tryViewCreatedFirstSight() {
        if (mUserSeen && mViewCreated) {
            onViewCreatedFirstSight(getView());
        }
    }

    /**
     * Called when the new created view is visible to user for the first time.
     */
    protected void onViewCreatedFirstSight(View view) {
        onRefresh();
    }

    /**
     * Called when the visible state to user has been changed.
     */
    protected void onUserVisibleChanged(boolean visible) {
        if(visible && mViewCreated){
            onRefresh();
        }
    }

    private void init(View mView) {
        tvNotFound = mView.findViewById(R.id.tvNotFound);
        swipeContainer = mView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        rvEveryOnePost = mView.findViewById(R.id.rvEveryOnePost);
        rvEveryOnePost.setLayoutManager(new LinearLayoutManager(getAppContext()));
        adapter = new EveryonePostAdapter(arrayList,this);
        rvEveryOnePost.setAdapter(adapter);

    }

    @Override
    protected EveryonePostPresenter createPresenter() {
        return new EveryonePostPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        swipeContainer.setRefreshing(false);
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        swipeContainer.setRefreshing(false);
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onPostSuccess(ArrayList<EveryOneModel> modelArrayList) {
        swipeContainer.setRefreshing(false);
        tvNotFound.setVisibility(View.GONE);
        try {
            arrayList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        arrayList.addAll(modelArrayList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPostFail(String message) {
        swipeContainer.setRefreshing(false);
        tvNotFound.setVisibility(View.VISIBLE);
        tvNotFound.setText(message);
        try {
            arrayList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        swipeContainer.setRefreshing(true);
        mvpPresenter.getEveryOnePost("all");

    }

    @Override
    public void onProfileCLick(EveryOneModel model) {
        Location mLocation = ((NavigationActivity) getAppContext()).getLocation();
        if (mLocation != null) {
            Intent intent = new Intent(getAppContext(), BrowseDetailsActivity.class);
            intent.putExtra("lat",mLocation.getLatitude());
            intent.putExtra("long",mLocation.getLongitude());
            intent.putExtra("id",model.getUserId());
            startActivity(intent);

        }else{
            onToast(getResources().getString(R.string.location_not_found));
        }

    }
}
