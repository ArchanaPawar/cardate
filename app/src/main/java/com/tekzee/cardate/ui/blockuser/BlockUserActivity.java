package com.tekzee.cardate.ui.blockuser;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpActivity;

import java.util.ArrayList;

import cn.refactor.lib.colordialog.PromptDialog;

public class BlockUserActivity extends MvpActivity<BlockUserPresenter> implements BlockUserView {
    private RecyclerView rvUser;
    private EditTextRegular etUserName;
    private BlockUserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_user);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mvpPresenter.getBlockedUserList();
    }

    private void init() {
        rvUser = findViewById(R.id.rvUser);
        rvUser.setLayoutManager(new LinearLayoutManager(getAppContext()));
        etUserName = findViewById(R.id.etUserName);

    }

    @Override
    protected BlockUserPresenter createPresenter() {
        return new BlockUserPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                }).show();

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.blocked_user_activity, message);

    }

    @Override
    public void onUserListSuccess(ArrayList<BlockedUserModel> arrayList) {
        adapter = new BlockUserAdapter(arrayList, new BlockUserAdapter.RowSelect() {
            @Override
            public void onSelect(BlockedUserModel model) {


            }

            @Override
            public void onUnBlock(BlockedUserModel model) {
                mvpPresenter.unBlockUser(model);
            }
        });
        rvUser.setAdapter(adapter);

    }

    @Override
    public void onUnBlockSuccess() {

        mvpPresenter.getBlockedUserList();

    }
}
