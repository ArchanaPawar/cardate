package com.tekzee.cardate.ui.signup;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SignUpPresenter extends BasePresenter<SignUpView> {
    private static final String TAG = SignUpPresenter.class.getSimpleName();


    public SignUpPresenter(SignUpView view, Context mContext) {

        attachView(view, mContext);
    }

    public void signUpNewUser(String fcmToken, String signUpType) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(true);
            Log.view("SignUpcall","call start");
            JSONObject input = new JSONObject();
            try {
                input.put("name", mvpView.getFullName());
                input.put("email", mvpView.getEmail());
                input.put("gender", mvpView.getGender());
                input.put("dob", mvpView.getDOB());
//                input.put("country_id", mvpView.getCountry());
//                input.put("city_id", mvpView.getCity());
                input.put("country_id", "");
                input.put("city_id", "");
                input.put("password", mvpView.getPassword());
                input.put("image_url", mvpView.getImageUrl());
                input.put("device_type", "ANDROID");
                input.put("signup_type", signUpType);
                input.put("device_token", fcmToken.equalsIgnoreCase("") ? Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FCM_TOKEN) : fcmToken);
            } catch (Throwable e) {

                Log.view(TAG, "input : " + e);
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);
            addSubscription(apiStores.signUp(input), new ApiCallback()
            {
                @Override
                public void onSuccess(Object model) {
                    try {

                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {
                            mvpView.onToast(jsonObject.getString("message"));
                            mvpView.onStartLoginActivity();

                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void getPassword(String fcmToken) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("email", mvpView.getEmail());
                input.put("device_type", "ANDROID");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

//            addSubscription(apiStores.getPassword(input), new ApiCallback() {
//                @Override
//                public void onSuccess(Object model) {
//                    try {
//                        JSONObject jsonObject = (JSONObject) model;
//                        Log.view(TAG, "Response : " + jsonObject);
//
//                        if (jsonObject.getBoolean("status")) {
//
                            String password = "123456";
                            mvpView.onGetPasswordSuccess(password,fcmToken);
//
//                        } else if (!jsonObject.getBoolean("status")) {
//
//                            mvpView.showInPopup(jsonObject.getString("message"));
//
//                        } else {
//
//                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
//
//                        }
//
//                    } catch (Exception e) {
//                        mvpView.dismissProgressDialog();
//                        mvpView.showInSnackBar(e.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(String msg) {
//                    mvpView.dismissProgressDialog();
//                    mvpView.showInSnackBar(msg);
//                }
//
//                @Override
//                public void onTokenExpired(String message) {
//                    mvpView.showProgressDialog(message, false);
//                    new Handler().postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            mvpView.dismissProgressDialog();
//                            mvpView.restartApp();
//                        }
//
//                    }, Constant.LOGOUT_DELAY);
//
//                }
//
//                @Override
//                public void onFinish() {
//                    mvpView.dismissProgressDialog();
//                }
//            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

//    public void getAllCountry() {
//        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
//            addSubscription(apiStores.getAllCountry(), new ApiCallback() {
//                @Override
//                public void onSuccess(Object model) {
//                    try {
//                        JSONObject jsonObject = (JSONObject) model;
//                        Log.view(TAG, "Response : " + jsonObject);
//
//                        if (jsonObject.getBoolean("status")) {
//
//                            ArrayList<CountryModel> arrayList = new ArrayList<>();
//                            JSONArray countryJsonArray = jsonObject.getJSONArray("data");
//                            for (int index = 0; index < countryJsonArray.length(); index++) {
//                                JSONObject object = countryJsonArray.getJSONObject(index);
//
//                                arrayList.add(new CountryModel(
//                                        object.getString("id"),
//                                        object.getString("country")
//                                ));
//                            }
//
//                            /**
//                             * open dialog for select country
//                             */
//                            openSelector(arrayList, mvpView.getAppContext().getResources().getString(R.string.country));
//
//                        } else if (!jsonObject.getBoolean("status")) {
//
//                            mvpView.showInPopup(jsonObject.getString("message"));
//
//                        } else {
//
//                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
//
//                        }
//
//                    } catch (Exception e) {
//                        mvpView.dismissProgressDialog();
//                        mvpView.showInSnackBar(e.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(String msg) {
//                    mvpView.dismissProgressDialog();
//                    mvpView.showInSnackBar(msg);
//                }
//
//                @Override
//                public void onTokenExpired(String message) {
//                    mvpView.showProgressDialog(message, false);
//                    new Handler().postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            mvpView.dismissProgressDialog();
//                            mvpView.restartApp();
//                        }
//
//                    }, Constant.LOGOUT_DELAY);
//
//                }
//
//                @Override
//                public void onFinish() {
//                    mvpView.dismissProgressDialog();
//                }
//            });
//
//
//        } else {
//            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
//        }
//
//
//    }

//    public void getAllCity(String country_id) {
//        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
//            JSONObject input = new JSONObject();
//            try {
//                input.put("country_id", country_id);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Log.view(TAG, "input : " + input);
//
//            addSubscription(apiStores.getAllCity(input), new ApiCallback() {
//                @Override
//                public void onSuccess(Object model) {
//                    try {
//                        JSONObject jsonObject = (JSONObject) model;
//                        Log.view(TAG, "Response : " + jsonObject);
//
//                        if (jsonObject.getBoolean("status")) {
//
//                            ArrayList<CountryModel> arrayList = new ArrayList<>();
//                            JSONArray countryJsonArray = jsonObject.getJSONArray("data");
//                            for (int index = 0; index < countryJsonArray.length(); index++) {
//                                JSONObject object = countryJsonArray.getJSONObject(index);
//
//                                arrayList.add(new CountryModel(
//                                        object.getString("id"),
//                                        object.getString("city")
//                                ));
//                            }
//
//                            /**
//                             * open dialog for select country
//                             */
//                            openSelector(arrayList, mvpView.getAppContext().getResources().getString(R.string.city));
//
//
//                        } else if (!jsonObject.getBoolean("status")) {
//
//                            mvpView.showInPopup(jsonObject.getString("message"));
//
//                        } else {
//
//                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
//
//                        }
//
//                    } catch (Exception e) {
//                        mvpView.dismissProgressDialog();
//                        mvpView.showInSnackBar(e.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(String msg) {
//                    mvpView.dismissProgressDialog();
//                    mvpView.showInSnackBar(msg);
//                }
//
//                @Override
//                public void onTokenExpired(String message) {
//                    mvpView.showProgressDialog(message, false);
//                    new Handler().postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            mvpView.dismissProgressDialog();
//                            mvpView.restartApp();
//                        }
//
//                    }, Constant.LOGOUT_DELAY);
//
//                }
//
//                @Override
//                public void onFinish() {
//                    mvpView.dismissProgressDialog();
//                }
//            });
//
//
//        } else {
//            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
//        }
//
//
//    }


//    private void openSelector(ArrayList<CountryModel> arrayList, final String type) {
//        if (arrayList.size() > 0) {
//
//            final Dialog dialog = new Dialog(mvpView.getAppContext());
//            try {
//                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//                dialog.setContentView(R.layout.popup_select_country);
//                dialog.getWindow().setLayout(-1, -2);
//                dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                dialog.setCancelable(false);
//
//                ((TextViewBold) dialog.findViewById(R.id.title)).setText(mvpView.getAppContext().getResources().getString(R.string.available) + type);
//
//                RecyclerView rv_country = dialog.findViewById(R.id.rv_country);
//                rv_country.setLayoutManager(new LinearLayoutManager(dialog.getContext()));
//                final CountryAdapter adapter = new CountryAdapter(arrayList, new CountryAdapter.RowSelect() {
//                    @Override
//                    public void onSelect(CountryModel model) {
//                        dialog.dismiss();
//
//                        mvpView.onCountrySelected(model, type);
//
//
//                    }
//                });
//                rv_country.setAdapter(adapter);
//                EditTextRegular et_country_name = dialog.findViewById(R.id.et_country_name);
//                et_country_name.setHint(mvpView.getAppContext().getResources().getString(R.string.search) + type);
//
//                et_country_name.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence s, int start, int before, int count) {
//                        String searchKey = s.toString();
//                        Log.view(TAG, "searchKey : " + searchKey);
//                        adapter.getFilter().filter(searchKey);
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable s) {
//
//                    }
//                });
//
//
//                dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//                dialog.show();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        } else {
//            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.no_country_found));
//        }
//
//    }
}
