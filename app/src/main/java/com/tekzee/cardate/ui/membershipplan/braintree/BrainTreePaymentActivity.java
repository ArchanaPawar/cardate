//package com.tekzee.cardate.ui.membershipplan.braintree;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.view.WindowManager;
//
//import com.braintreepayments.api.dropin.DropInActivity;
//import com.braintreepayments.api.dropin.DropInRequest;
//import com.braintreepayments.api.dropin.DropInResult;
//import com.braintreepayments.api.models.CardNonce;
//import com.braintreepayments.api.models.GooglePaymentRequest;
//import com.braintreepayments.api.models.PaymentMethodNonce;
//import com.tekzee.cardate.R;
//import com.tekzee.cardate.custom.TextViewRegular;
//import com.tekzee.cardate.ui.base.MvpActivity;
//import com.tekzee.cardate.ui.membershipplan.PlanModel;
//import com.tekzee.cardate.utils.Log;
//import com.tekzee.cardate.utils.MyCustomDialog;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//public class BrainTreePaymentActivity extends MvpActivity<BrainTreePresenter> implements BrainTreeView {
//    private static final String TAG = BrainTreePaymentActivity.class.getSimpleName();
//    private PlanModel model;
//    private double totalPayable;
//
//
//    static final String EXTRA_PAYMENT_RESULT = "payment_result";
//    static final String EXTRA_DEVICE_DATA = "device_data";
//    static final String EXTRA_COLLECT_DEVICE_DATA = "collect_device_data";
//
//    private static final int DROP_IN_REQUEST = 1;
//    private static final int GOOGLE_PAYMENT_REQUEST = 2;
//    private static final int CARDS_REQUEST = 3;
//    private static final int PAYPAL_REQUEST = 4;
//    private static final int VENMO_REQUEST = 5;
//    private static final int VISA_CHECKOUT_REQUEST = 6;
//    private static final int LOCAL_PAYMENTS_REQUEST = 7;
//
//
//    private static final String KEY_NONCE = "nonce";
//
//    private PaymentMethodNonce mNonce;
//
//    private CardNonce cardNonce;
//    private String paymentId = "";
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_brain_tree_payment);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        init();
//
//        findViewById(R.id.toolbar_navigation_back).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onBackPressed();
//
//            }
//        });
//
//        if (savedInstanceState != null) {
//            if (savedInstanceState.containsKey(KEY_NONCE)) {
//                mNonce = savedInstanceState.getParcelable(KEY_NONCE);
//            }
//        }
//
//        findViewById(R.id.btnPay).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                mvpPresenter.insertPayment(model);
//
//            }
//        });
//
//
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        if (mNonce != null) {
//            outState.putParcelable(KEY_NONCE, mNonce);
//        }
//    }
//
//
//    private void init() {
//        model = (PlanModel) getIntent().getSerializableExtra("model");
//
//        ((TextViewRegular) findViewById(R.id.tvMonth)).setText(model.getDuration() + getResources().getString(R.string.month));
//        ((TextViewRegular) findViewById(R.id.tvPerMonthCharge)).setText(model.getCurrency() + model.getAmount());
//
//        totalPayable = (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount()));
//        ((TextViewRegular) findViewById(R.id.tvTotal)).setText(model.getCurrency() + totalPayable);
//
//    }
//
//    @Override
//    protected BrainTreePresenter createPresenter() {
//        return new BrainTreePresenter(this, this);
//    }
//
//    @Override
//    public Activity getAppContext() {
//        return this;
//    }
//
//    @Override
//    public void showInPopup(String message) {
//        MyCustomDialog.show(getAppContext(), message);
//    }
//
//    @Override
//    public void showInSnackBar(String message) {
//        snackBarTop(R.id.brain_tree_activity, message);
//    }
//
//    @Override
//    public void onPaymentSuccess(JSONObject mJsonObject) {
//
//    }
//
//    @Override
//    public void onOurLocalServerSuccess(String message) {
//        onToast(message);
//        finish();
//
//    }
//
//    @Override
//    public void onPaymentFail(String reason) {
//        showInPopup(reason);
//    }
//
//    @Override
//    public void onTokenReceived(JSONObject data) {
//        DropInRequest dropInRequest = null;
//        try {
//            paymentId = data.getString("payment_id");
//
//            dropInRequest = new DropInRequest().amount(totalPayable + "")
//                    .clientToken(data.getString("clientToken"))
//                    .amount(data.getString("totalamount"));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        startActivityForResult(dropInRequest.getIntent(this), DROP_IN_REQUEST);
//    }
//
//
//    @Override
//    public void launchDropIn() {
////        startActivityForResult(getDropInRequest().getIntent(this), DROP_IN_REQUEST);
//    }
//
//    @Override
//    public void launchGooglePayment() {
//        Intent intent = new Intent(this, GooglePaymentActivity.class);
//        startActivityForResult(intent, GOOGLE_PAYMENT_REQUEST);
//    }
//
//    @Override
//    public void launchCards() {
////        Intent intent = new Intent(this, CardActivity.class)
////                .putExtra(EXTRA_COLLECT_DEVICE_DATA, Settings.shouldCollectDeviceData(this));
////        startActivityForResult(intent, CARDS_REQUEST);
//
//    }
//
//    @Override
//    public void launchPayPal() {
////        Intent intent = new Intent(this, PayPalActivity.class)
////                .putExtra(EXTRA_COLLECT_DEVICE_DATA, Settings.shouldCollectDeviceData(this));
////        startActivityForResult(intent, PAYPAL_REQUEST);
//
//    }
//
//    @Override
//    public void launchVenmo() {
////        Intent intent = new Intent(this, VenmoActivity.class);
////        startActivityForResult(intent, VENMO_REQUEST);
//
//    }
//
//    @Override
//    public void launchVisaCheckout() {
////        Intent intent = new Intent(this, VisaCheckoutActivity.class);
////        startActivityForResult(intent, VISA_CHECKOUT_REQUEST);
//
//    }
//
//    @Override
//    public void launchLocalPayments() {
////        Intent intent = new Intent(this, LocalPaymentsActivity.class);
////        startActivityForResult(intent, LOCAL_PAYMENTS_REQUEST);
//
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == DROP_IN_REQUEST) {
//            if (resultCode == RESULT_OK) {
//                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
//                Log.view(TAG,"\t"+result.getPaymentMethodNonce()+", \t"+result.getDeviceData());
//                mvpPresenter.submitPaymentSuccess(paymentId,model,result.getPaymentMethodNonce());
//
////                displayNonce(result.getPaymentMethodNonce(), result.getDeviceData());
//                // use the result to update your UI and send the payment method nonce to your server
//            } else if (resultCode == RESULT_CANCELED) {
//                // the user canceled
//            } else {
//                // handle errors here, an exception may be available in
//                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
//            }
//        }
//    }
//
////    private void displayNonce(PaymentMethodNonce paymentMethodNonce, String deviceData) {
////        mNonce = paymentMethodNonce;
////
////        Intent intent = new Intent(this, CreateTransactionActivity.class).putExtra(CreateTransactionActivity.EXTRA_PAYMENT_METHOD_NONCE, mNonce);
////        startActivity(intent);
////
////
//////        mNonceIcon.setImageResource(PaymentMethodType.forType(mNonce).getDrawable());
//////        mNonceIcon.setVisibility(VISIBLE);
//////
//////        mNonceString.setText(getString(R.string.nonce_placeholder, mNonce.getNonce()));
//////        mNonceString.setVisibility(VISIBLE);
//////
//////        String details = "";
//////        if (mNonce instanceof CardNonce) {
//////            details = CardActivity.getDisplayString((CardNonce) mNonce);
//////        }
//////        else if (mNonce instanceof PayPalAccountNonce) {
//////            details = PayPalActivity.getDisplayString((PayPalAccountNonce) mNonce);
//////        } else if (mNonce instanceof GooglePaymentCardNonce) {
//////            details = GooglePaymentActivity.getDisplayString((GooglePaymentCardNonce) mNonce);
//////        } else if (mNonce instanceof VisaCheckoutNonce) {
//////            details = VisaCheckoutActivity.getDisplayString((VisaCheckoutNonce) mNonce);
//////        } else if (mNonce instanceof VenmoAccountNonce) {
//////            details = VenmoActivity.getDisplayString((VenmoAccountNonce) mNonce);
//////        } else if (mNonce instanceof LocalPaymentResult) {
//////            details = LocalPaymentsActivity.getDisplayString((LocalPaymentResult) mNonce);
//////        }
//////
//////        mNonceDetails.setText(details);
//////        mNonceDetails.setVisibility(VISIBLE);
//////
//////        mDeviceData.setText(getString(R.string.device_data_placeholder, deviceData));
//////        mDeviceData.setVisibility(VISIBLE);
//////
//////        mCreateTransactionButton.setEnabled(true);
////
////    }
//
//
//}
