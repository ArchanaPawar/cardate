package com.tekzee.cardate.ui.friends.online;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface OnlineFriendsView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<OnlineFriendModel> arrayList);

    void onPostFail(String message);
}
