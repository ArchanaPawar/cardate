package com.tekzee.cardate.ui.browse;

public class BrowseModel {
    private String id;
    private String name;
    private String image;
    private String country;
    private String dateOfBirth;
    private String countryId;
    private String distance;
    private String age;
    private boolean isOnline;

    public BrowseModel(String id, String name, String image, String country, String dateOfBirth, String countryId, String distance, String age, boolean isOnline) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.country = country;
        this.dateOfBirth = dateOfBirth;
        this.countryId = countryId;
        this.distance = distance;
        this.age = age;
        this.isOnline = isOnline;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getCountry() {
        return country;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getDistance() {
        return distance;
    }

    public String getAge() {
        return age;
    }

    public boolean isOnline() {
        return isOnline;
    }
}
