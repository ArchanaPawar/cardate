package com.tekzee.cardate.ui.signup;

public class CountryModel {
    private String id;
    private String name;

    public CountryModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
