package com.tekzee.cardate.ui.searchbycar;

public class SearchByCarModel {
    private String id;
    private String name;
    private String email;
    private String dateOfBirth;
    private String occupation;
    private String aboutMe;
    private String gender;
    private String image;
    private String isOnline;
    private String interest;
    private String carNumber;
    private String age;

    public SearchByCarModel(String id, String name, String email, String dateOfBirth, String occupation, String aboutMe, String gender, String image, String isOnline, String interest, String carNumber, String age) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.occupation = occupation;
        this.aboutMe = aboutMe;
        this.gender = gender;
        this.image = image;
        this.isOnline = isOnline;
        this.interest = interest;
        this.carNumber = carNumber;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getGender() {
        return gender;
    }

    public String getImage() {
        return image;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public String getInterest() {
        return interest;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getAge() {
        return age;
    }
}