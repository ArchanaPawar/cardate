package com.tekzee.cardate.ui.matchmatching.matches;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import org.json.JSONObject;

import java.util.ArrayList;

public interface MatchesView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onMatchSuccess(ArrayList<MatchModel> arrayList);

    void bigImageView(String title,String imageUrl);

    void onUserBlocked(String message);

    void userNotBlocked();

    void onFriendStatusSuccess(MatchModel model,JSONObject data);

}
