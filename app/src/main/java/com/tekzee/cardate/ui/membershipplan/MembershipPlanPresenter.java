package com.tekzee.cardate.ui.membershipplan;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MembershipPlanPresenter extends BasePresenter<MembershipPlanView> {
    private static final String TAG = MembershipPlanPresenter.class.getSimpleName();

    public MembershipPlanPresenter(MembershipPlanView view, Context mContext) {

        attachView(view, mContext);
    }

    public void getMembershipPlan() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try{
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.view(TAG,"input : "+input);
            addSubscription(apiStores.getPlan(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");

                            /**
                             * get user current plan
                             */
                            String planId = "";
                            String planName = "";
                            String startDate = "";
                            String endDate = "";
                            String duration = "";
                            JSONObject userPlanObject = dataObject.getJSONObject("userplan");
                            if(userPlanObject.length() > 0){
                                planId = userPlanObject.getString("plan_id");
                                planName = userPlanObject.getString("plan_name");
                                startDate = userPlanObject.getString("start_date");
                                endDate = userPlanObject.getString("end_date");
                                duration = userPlanObject.getString("duration");
                            }

                            JSONArray dataJsonArray = dataObject.getJSONArray("plan");
                            ArrayList<PlanModel> arrayList = new ArrayList<>();
                            for (int i = 0; i < dataJsonArray.length(); i++) {
                                JSONObject object = dataJsonArray.getJSONObject(i);

                                ArrayList<ContentModel> contentModels = new ArrayList<>();
                                JSONArray contentJsonArray = object.getJSONArray("content");
                                for (int j = 0; j < contentJsonArray.length(); j++) {
                                    JSONObject contentObject = contentJsonArray.getJSONObject(j);
                                    contentModels.add(new ContentModel(
                                            contentObject.getString("title"),
                                            contentObject.getString("description")
                                    ));
                                }


                                arrayList.add(new PlanModel(
                                        object.getString("id"),
                                        object.getString("plan_name"),
                                        object.getString("duration"),
                                        object.getString("amount"),
                                        object.getString("currency"),
                                        object.getString("feature"),
                                        contentModels
                                ));
                            }

                            mvpView.updateCurrentPlan(planId,planName,startDate,endDate,duration);
                            mvpView.onPlanFoundSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }
    }
}
