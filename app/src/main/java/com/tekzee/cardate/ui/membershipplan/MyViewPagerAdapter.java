package com.tekzee.cardate.ui.membershipplan;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<PlanModel> arrayList;

    public MyViewPagerAdapter(FragmentManager fragmentManager, ArrayList<PlanModel> arrayList) {
        super(fragmentManager);
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(arrayList.get(position));
    }
}