package com.tekzee.cardate.ui.matchmatching.matches;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.chat.message.MessageActivity;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;
import com.tekzee.cardate.ui.matchmatching.MatchMatchingFragment;
import com.tekzee.cardate.ui.navigation.MyFirebaseUser;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

public class MatchesFragment extends MvpFragment<MatchesPresenter> implements MatchesView {
    private static final String TAG = MatchesFragment.class.getSimpleName();
    private RecyclerView rvMatches;
    private CircleImageView ivProfileImageLeft;
    private CircleImageView ivProfileImageRight;
    private TextViewRegular tvMatchName;
    private MatchModel model;
    private boolean mViewCreated = false;
    private boolean mUserSeen = false;
    private String imageUrl;
    private String mFirebaseUserId = "";

    public static MatchesFragment newInstance() {
        return new MatchesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_matches, container, false);
        init(mView);


        ivProfileImageLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bigImageView(Utility.getSharedPreferences(getAppContext(), Constant.NAME), imageUrl);

            }
        });

        ivProfileImageRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model != null)
                    bigImageView(model.getName(), model.getImage());
            }
        });


        mView.findViewById(R.id.btnKeepSwiping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MatchMatchingFragment.viewPager.setCurrentItem(0);


            }
        });

        mView.findViewById(R.id.btnSendMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(model != null){
                    mvpPresenter.getFriendStatus(model);
                }else{
                   onToast(getString(R.string.matches_not_found));
                }

            }
        });


        return mView;

    }

    private void init(View mView) {
        tvMatchName = mView.findViewById(R.id.tvMatchName);
        ivProfileImageLeft = mView.findViewById(R.id.ivProfileImageLeft);
        imageUrl = Utility.getSharedPreferences(getAppContext(), Constant.PROFILE_IMAGE);
        if (imageUrl != null && !imageUrl.equalsIgnoreCase("")) {
            Glide.with(getAppContext()).load(imageUrl).into(ivProfileImageLeft);
        }

        ivProfileImageRight = mView.findViewById(R.id.ivProfileImageRight);

        rvMatches = mView.findViewById(R.id.rvMatches);
        rvMatches.setLayoutManager(new LinearLayoutManager(getAppContext()));


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!mUserSeen && isVisibleToUser) {
            mUserSeen = true;
            tryViewCreatedFirstSight();
        }
        onUserVisibleChanged(isVisibleToUser);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewCreated = false;
        mUserSeen = false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewCreated = true;
        tryViewCreatedFirstSight();
    }


    private void tryViewCreatedFirstSight() {
        if (mUserSeen && mViewCreated) {
            onViewCreatedFirstSight(getView());
        }
    }

    /**
     * Called when the new created view is visible to user for the first time.
     */
    protected void onViewCreatedFirstSight(View view) {
//        mvpPresenter.getMyLikes(true,"match");
    }

    /**
     * Called when the visible state to user has been changed.
     */
    protected void onUserVisibleChanged(boolean visible) {
        if (visible && mViewCreated) {
            mvpPresenter.getMyLikes("match");
        }
    }

    @Override
    protected MatchesPresenter createPresenter() {
        return new MatchesPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
        Utility.setSharedPreference(getAppContext(), Constant.MATCH_COUNT, "0");
//        ((NavigationActivity) getActivity()).populateSideMenu();

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);

    }

    @Override
    public void onMatchSuccess(ArrayList<MatchModel> arrayList) {
        Utility.setSharedPreference(getAppContext(), Constant.MATCH_COUNT, "0");
        if (arrayList.size() > 0) {
            model = arrayList.get(0);
            setMatch();
            rvMatches.setAdapter(new MatchAdapter(arrayList, new MatchAdapter.Click() {
                @Override
                public void onCLick(MatchModel model) {
                    if(model != null){
                        Log.view(TAG,"Name : "+model.getName());
                        mvpPresenter.getFriendStatus(model);
                    }else{
                        onToast(getString(R.string.matches_not_found));
                    }

                }
            }));

        }
//        ((NavigationActivity) getActivity()).populateSideMenu();

    }

    @Override
    public void bigImageView(String title, String imageUrl) {
        Intent intent = new Intent(getAppContext(), DocumentViewerActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("imageUrl", imageUrl);
        startActivity(intent);

    }

    @Override
    public void onUserBlocked(String message) {
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void userNotBlocked() {
        Intent intent = new Intent(getAppContext(), MessageActivity.class);
        intent.putExtra("userid", mFirebaseUserId);
        startActivity(intent);
    }

    @Override
    public void onFriendStatusSuccess(MatchModel matchModel, JSONObject data) {
        try {
            if (data.getString("friendStatus").equalsIgnoreCase("0") || data.getString("friendStatus").equalsIgnoreCase("1")) {
                DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);
                mDatabaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            MyFirebaseUser user = snapshot.getValue(MyFirebaseUser.class);

                            if (user.getEmail() != null && matchModel != null) {

                                if (user.getEmail().equalsIgnoreCase(matchModel.getEmail())) {
                                    mFirebaseUserId = user.getId();
                                    break;

                                }

                            }

                        }

                        if (mFirebaseUserId != null) {
                            if (matchModel != null && !mFirebaseUserId.equalsIgnoreCase("")) {
                                /**
                                 * check is user blocked
                                 */
                                mvpPresenter.isUserBlocked(matchModel.getId());

                            } else {

                                showInSnackBar(getAppContext().getResources().getString(R.string.matches_not_found));

                            }
                        } else {

                            showInSnackBar(getAppContext().getResources().getString(R.string.matches_not_found));

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            } else {

                showInSnackBar(getResources().getString(R.string.need_to_be_a_friend));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setMatch() {

        /**
         * set self image
         */
        if (!Utility.getSharedPreferences(getAppContext(), Constant.PROFILE_IMAGE).equalsIgnoreCase("")) {
            Glide.with(getAppContext()).load(Utility.getSharedPreferences(getAppContext(), Constant.PROFILE_IMAGE)).into(ivProfileImageLeft);
        }

        /**
         * set friend image
         */
        String imageUrl = model.getImage();
        if (!imageUrl.equalsIgnoreCase("")) {
            Glide.with(getAppContext()).load(imageUrl).into(ivProfileImageRight);
        }

        /**
         * set name
         */
        tvMatchName.setText(model.getName() + " " + getAppContext().getResources().getString(R.string.likes_you_too));

    }


}
