package com.tekzee.cardate.ui.navigation;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface NavigationView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onLogoutSuccess(String message);

    void resetAllFilter();

}
