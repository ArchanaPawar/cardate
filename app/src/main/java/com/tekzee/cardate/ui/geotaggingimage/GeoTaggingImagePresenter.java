package com.tekzee.cardate.ui.geotaggingimage;

import android.content.Context;

import com.tekzee.cardate.ui.base.BasePresenter;

public class GeoTaggingImagePresenter extends BasePresenter<GeoTaggingImageView> {
    private static final String TAG = GeoTaggingImagePresenter.class.getSimpleName();

    public GeoTaggingImagePresenter(GeoTaggingImageView view, Context mContext) {

        attachView(view, mContext);
    }
}
