package com.tekzee.cardate.ui.introduction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import android.view.View;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.tekzee.cardate.R;
import com.tekzee.cardate.social_auth.facebookSignIn.FacebookHelper;
import com.tekzee.cardate.social_auth.facebookSignIn.FacebookResponse;
import com.tekzee.cardate.social_auth.facebookSignIn.FacebookUser;
import com.tekzee.cardate.social_auth.googleAuthSignin.GoogleAuthResponse;
import com.tekzee.cardate.social_auth.googleAuthSignin.GoogleAuthUser;
import com.tekzee.cardate.social_auth.googleAuthSignin.GoogleSignInHelper;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.login.LoginActivity;
import com.tekzee.cardate.ui.signup.SignUpActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;

import cn.refactor.lib.colordialog.PromptDialog;

public class IntroductionActivity extends MvpActivity<IntroductionPresenter> implements IntroductionView, FacebookResponse, GoogleAuthResponse {
    private static final String TAG = IntroductionActivity.class.getSimpleName();
    private SliderLayout mSliderLayout;
//    private FacebookHelper mFbHelper;
    private GoogleSignInHelper mGAuthHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        setUpFullScreen();
        init();

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartLoginActivity();
            }
        });

//        findViewById(R.id.btn_login_with_facebook).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mFbHelper.performSignIn(getAppContext());
//            }
//        });


        findViewById(R.id.btn_login_with_gmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGAuthHelper.performSignIn(getAppContext());
            }
        });

        mvpPresenter.getSliderImage();
    }

    private void init() {
        mSliderLayout = findViewById(R.id.slider);

        //google auth initialization
        mGAuthHelper = new GoogleSignInHelper(this, "83127726587-6idcl7ei6op8v7g6d400k6k01vjblphh.apps.googleusercontent.com", this);

        //fb api initialization
//        mFbHelper = new FacebookHelper(this,"id,name,email,gender,birthday,picture,cover",this);

    }

    private void setUpFullScreen() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    protected IntroductionPresenter createPresenter() {
        return new IntroductionPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.activity_introduction, message);
    }

    @Override
    public void onStartLoginActivity() {
        startActivity(new Intent(getAppContext(), LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onStartSignUpActivity(String name, String email, String imageUrl, String gender) {
        Intent intent = new Intent(getAppContext(), SignUpActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("email", email);
        intent.putExtra("imageUrl", imageUrl);
        intent.putExtra("gender", gender);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }


    @Override
    public void onSliderFound(ArrayList<String> arrayList) {
        for (String name : arrayList) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(name)
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            mSliderLayout.addSlider(textSliderView);
        }

        mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSliderLayout.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
        mSliderLayout.setCurrentPosition(0, true);

    }

    @Override
    protected void onStop() {
        mSliderLayout.stopAutoCycle();
        super.onStop();

        /**
         * signout from social
         */
//        try {
//            mFbHelper.performSignOut();
//        } catch (Exception e) {
//
//        }

        try {
            mGAuthHelper.performSignOut();
        } catch (Exception e) {

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //handle results
//        mFbHelper.onActivityResult(requestCode, resultCode, data);
        mGAuthHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFbSignInFail() {

        onToast(getResources().getString(R.string.facebook_signin_failed));

    }

    @Override
    public void onFbSignInSuccess() {
    }

    @Override
    public void onFbProfileReceived(FacebookUser facebookUser) {
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_SOCIAL, true);
        String imageUrl = ("https://graph.facebook.com/" + facebookUser.facebookID + "/picture?type=large");
        onStartSignUpActivity(facebookUser.name, facebookUser.email, imageUrl, facebookUser.gender);
//        mFbHelper.performSignOut();

    }

    @Override
    public void onFBSignOut() {
//        onToast("onFBSignOut");
    }

    @Override
    public void onGoogleAuthSignIn(GoogleAuthUser user) {
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_SOCIAL, true);
        String imageUrl = user.photoUrl.toString();
        onStartSignUpActivity(user.name, user.email, imageUrl, "");
        mGAuthHelper.performSignOut();

    }

    @Override
    public void onGoogleAuthSignInFailed() {
        onToast(getResources().getString(R.string.google_signin_failed));

    }

    @Override
    public void onGoogleAuthSignOut(boolean isSuccess) {

//        if (isSuccess) {
//            onToast("onGoogleAuthSignOut");
//        } else {
//            onToast("onGoogleAuthSignOut Failed");
//        }
    }


}
