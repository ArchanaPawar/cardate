package com.tekzee.cardate.ui.friends.request;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface RequestFriendView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<RequestFriendModel> arrayList);

    void onPostFail(String message);
}
