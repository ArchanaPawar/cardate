package com.tekzee.cardate.ui.searchbycar;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.browsedetails.BrowseDetailsActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SearchByCarFragment extends MvpFragment<SearchByCarPresenter> implements SearchByCarView, SearchByCarAdapter.Click {
    private static final String TAG = SearchByCarFragment.class.getSimpleName();
    private EditTextRegular etCarNumber;
    private Timer mTimer = new Timer();
    private final long DELAY = 600; // in ms

    private RecyclerView rvCarView;
    private TextViewBold tvNotFound;
    private ArrayList<SearchByCarModel> arrayList = new ArrayList<>();
    private SearchByCarAdapter adapter;

    public static SearchByCarFragment newInstance() {
        return new SearchByCarFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_search_by_car, container, false);
        init(mView);

        etCarNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                final String search_key = s.toString().trim();
                try {
                    if (mTimer != null) {
                        mTimer.cancel();
                    }

                    if (search_key.length() > 1) {
                        mTimer = new Timer();
                        mTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {

                                getAppContext().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mvpPresenter.searchCar(search_key);
                                    }
                                });
                            }
                        }, DELAY);
                    }

                } catch (Exception e) {
                    mvpPresenter.searchCar(search_key);
                    e.printStackTrace();

                }

            }
        });

        return mView;

    }

    private void init(View mView) {
        etCarNumber = mView.findViewById(R.id.etCarNumber);
        tvNotFound = mView.findViewById(R.id.tvNotFound);
        rvCarView = mView.findViewById(R.id.rvCarView);
        rvCarView.setLayoutManager(new LinearLayoutManager(getAppContext()));
        adapter = new SearchByCarAdapter(arrayList, this);
        rvCarView.setAdapter(adapter);


    }

    @Override
    protected SearchByCarPresenter createPresenter() {
        return new SearchByCarPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onSearchResult(String key, ArrayList<SearchByCarModel> modelArrayList) {
        tvNotFound.setVisibility(View.GONE);
        try {
            arrayList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        arrayList.addAll(modelArrayList);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onSearchFail(String message) {
        tvNotFound.setVisibility(View.VISIBLE);
        tvNotFound.setText(message);
        try {
            arrayList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onCLick(SearchByCarModel model) {
        Location mLocation = ((NavigationActivity) getAppContext()).getLocation();
        if (mLocation != null) {
            Intent intent = new Intent(getAppContext(), BrowseDetailsActivity.class);
            intent.putExtra("lat", mLocation.getLatitude());
            intent.putExtra("long", mLocation.getLongitude());
            intent.putExtra("id", model.getId());
            startActivity(intent);

        } else {
            onToast(getAppContext().getResources().getString(R.string.location_null));
        }

    }
}
