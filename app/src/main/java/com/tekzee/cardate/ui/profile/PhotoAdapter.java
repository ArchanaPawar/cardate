package com.tekzee.cardate.ui.profile;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;

import java.util.ArrayList;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
    private ArrayList<PhotoModel> arrayList;
    private Context mContext;

    public PhotoAdapter(ArrayList<PhotoModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_photo, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
        }

        public void bind(final PhotoModel model) {


            /**
             * image
             */
            String imageUrl = model.getImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivImage);
            }
//
//            ivImage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    bigImageView(model.getName(), model.getImage());
//                }
//            });



        }

//        public void bigImageView(String title, String imageUrl) {
//            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
//            intent.putExtra("title", title);
//            intent.putExtra("imageUrl", imageUrl);
//            mContext.startActivity(intent);
//
//        }
    }
}
