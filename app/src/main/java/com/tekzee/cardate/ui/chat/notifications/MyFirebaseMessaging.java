package com.tekzee.cardate.ui.chat.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.chat.message.MessageActivity;
import com.tekzee.cardate.ui.splash.SplashActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;

import java.util.HashMap;
import java.util.Map;

import static com.tekzee.cardate.ApplicationController.CHANNEL_FRIEND_REQUEST;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessaging.class.getSimpleName();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        if (firebaseUser != null) {
            updateToken(refreshToken);
        }
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null)
            return;

        Log.view(TAG, "remoteMessage : " + remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            try {
                Map<String, String> messageDataPayload = remoteMessage.getData();
                RemoteMessage.Notification mNotification = remoteMessage.getNotification();
                int message_type = Integer.parseInt(messageDataPayload.get("type"));
                Log.view(TAG, "message_type=========================> " + message_type);

                switch (message_type) {
                    /**
                     * Chat notification
                     */
                    case 7007: {
                        sendChatNotification(messageDataPayload.get("title"), messageDataPayload.get("body"), messageDataPayload.get("sented"));
                    }
                    break;

                    /**
                     * Other notification
                     */
                    default: {
                       sendCommonNotification(mNotification.getTitle(), mNotification.getBody());
                    }
                    break;

                }

            } catch (Exception e) {
                Log.view(TAG, "Exception: " + e.getMessage());
            }

        }
    }

    private void sendChatNotification(String title, String message, String sented) {
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("userid", sented);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_FRIEND_REQUEST)
                .setSmallIcon(R.drawable.logo_red)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        int id = (int) System.currentTimeMillis();

        managerCompat.notify(id, notification);


    }

    private void sendCommonNotification(String title, String message) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_FRIEND_REQUEST)
                .setSmallIcon(R.drawable.logo_red)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        int id = (int) System.currentTimeMillis();

        managerCompat.notify(id, notification);


    }

    private void updateToken(String refreshToken) {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("token", refreshToken);
            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
