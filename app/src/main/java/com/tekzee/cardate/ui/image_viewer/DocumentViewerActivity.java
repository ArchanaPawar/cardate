package com.tekzee.cardate.ui.image_viewer;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import android.view.View;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.utils.MyCustomDialog;

import cn.refactor.lib.colordialog.PromptDialog;

public class DocumentViewerActivity extends MvpActivity<DocumentViewerPresenter> implements DocumentView {
    private String image_url;
    private ZoomageView myZoomageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_document);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();

        /**
         * Hide soft keyboard
         */
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.toolbar_icon_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        image_url = getIntent().getExtras().getString("imageUrl");
        ((TextViewRegular) findViewById(R.id.tv_title)).setText(getIntent().getExtras().getString("title"));


        if (image_url != null && !image_url.equalsIgnoreCase("")) {

            /**
             * load image in ZoomImageView
             */
            Glide.with(getAppContext())
                    .load(image_url)
                    .into(myZoomageView);
        } else {
            showInPopup(getResources().getString(R.string.image_details_not_found));
        }


    }

    private void init() {

        myZoomageView = findViewById(R.id.myZoomageView);

    }

    @Override
    protected DocumentViewerPresenter createPresenter() {
        return new DocumentViewerPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.document_viewer_activity, message);

    }

}
