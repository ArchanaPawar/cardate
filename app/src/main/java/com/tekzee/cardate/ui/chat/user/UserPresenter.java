package com.tekzee.cardate.ui.chat.user;

import android.content.Context;

import com.tekzee.cardate.ui.base.BasePresenter;

public class UserPresenter extends BasePresenter<UserView> {

    public UserPresenter(UserView view, Context mContext) {

        attachView(view, mContext);

    }
}
