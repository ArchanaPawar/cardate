package com.tekzee.cardate.ui.membershipplan;

import java.io.Serializable;

public class ContentModel implements Serializable {
    private String title;
    private String description;

    public ContentModel(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
