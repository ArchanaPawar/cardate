package com.tekzee.cardate.ui.chat.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.chat.adapter.UserAdapter;
import com.tekzee.cardate.ui.chat.message.MessageActivity;
import com.tekzee.cardate.ui.chat.model.ChatList;
import com.tekzee.cardate.ui.chat.model.User;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatFragment extends MvpFragment<ChatPresenter> implements ChatView, SwipeRefreshLayout.OnRefreshListener, UserAdapter.ChatUserClick {
    private static final String TAG = ChatFragment.class.getSimpleName();
    private RecyclerView recyclerViewData;
    private SwipeRefreshLayout swipeContainer;
    private TextViewBold tvNotFound;
    private UserAdapter adapter;
    private List<User> mUsers = new ArrayList<>();
    private List<ChatList> usersList = new ArrayList<>();
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;


    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chat, container, false);
        init(mView);

        onRefresh();

        return mView;

    }

    private void init(View mView) {
        tvNotFound = mView.findViewById(R.id.tvNotFound);
        swipeContainer = mView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recyclerViewData = mView.findViewById(R.id.recyclerViewData);
        recyclerViewData.setLayoutManager(new LinearLayoutManager(getAppContext()));
        adapter = new UserAdapter(getAppContext(), mUsers, true, this);
        recyclerViewData.setAdapter(adapter);

    }

    @Override
    protected ChatPresenter createPresenter() {
        return new ChatPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        swipeContainer.setRefreshing(false);
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        swipeContainer.setRefreshing(false);
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onUserBlocked(String message) {
        swipeContainer.setRefreshing(false);
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void userNotBlocked(String blockUserId) {
        swipeContainer.setRefreshing(false);
        Intent intent = new Intent(getAppContext(), MessageActivity.class);
        intent.putExtra("userid", blockUserId);
        getAppContext().startActivity(intent);

    }

    private void chatList() {
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    try {
                        User user = snapshot.getValue(User.class);
                        for (ChatList chatlist : usersList) {

                            if (user.getId() != null) {

                                if (user.getId().equals(chatlist.getId())) {
                                    mUsers.add(user);
                                    Log.view(TAG,user.getId()+"->"+user.getName()+"->"+user.getEmail());
                                }
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (mUsers.size() > 0) {
                    tvNotFound.setVisibility(View.GONE);

                } else {

                    tvNotFound.setVisibility(View.VISIBLE);
                }

                adapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onRefresh() {
        swipeContainer.setRefreshing(true);

        /**
         * init fire-base
         */
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_CHAT_LIST).child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ChatList chatlist = snapshot.getValue(ChatList.class);
                    usersList.add(chatlist);
                }

                chatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onChatUserSelected(User model) {
        /**
         * check is user blocked
         */
        mvpPresenter.isUserBlocked(model.getId(), model.getUserId());

    }
}
