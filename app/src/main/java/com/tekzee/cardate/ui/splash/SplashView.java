package com.tekzee.cardate.ui.splash;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface SplashView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onNoInternetConnection();

    void onStartIntroductionActivity();

    void onStartNavigationActivity();

    void onStartLanguageSettingsActivity();

    void resetAllFilter();

    void showUpdatePopup(String message);

    void onLogoutSuccess(String message);
}
