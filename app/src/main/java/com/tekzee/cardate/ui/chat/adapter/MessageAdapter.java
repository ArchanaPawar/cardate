package com.tekzee.cardate.ui.chat.adapter;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.tekzee.cardate.ChatVoicePlayer.VoicePlayerView;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.chat.model.Chat;
import com.tekzee.cardate.utils.UploadType;

import java.util.HashMap;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MessageAdapter.class.getSimpleName();
    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;
    public static final int MSG_TYPE_AUDIO_LEFT = 2;
    public static final int MSG_TYPE_AUDIO_RIGHT = 3;
    private Context mContext;
    private List<Chat> mChat;
    private String imageurl;
    private FirebaseUser fuser;
    private HashMap<Integer, VoicePlayerView> mediaPlayerHashMap = new HashMap<>();
    private Integer currentClickedPosition = -1;
    private Dialog mProgressDialog;
    private Handler mHandler = new Handler();

    public MessageAdapter(Context mContext, List<Chat> mChat, String imageurl) {
        this.mChat = mChat;
        this.mContext = mContext;
        this.imageurl = imageurl;
        mProgressDialog = new Dialog(mContext, android.R.style.Theme_Black);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mProgressDialog.setContentView(LayoutInflater.from(mContext).inflate(R.layout.remove_border, null));

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_right, parent, false);
            return new TextViewHolder(view);

        } else if (viewType == MSG_TYPE_LEFT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_left, parent, false);
            return new TextViewHolder(view);

        } else if (viewType == MSG_TYPE_AUDIO_LEFT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_audio_left, parent, false);
            return new AudioViewHolderLeft(view);

        } else if (viewType == MSG_TYPE_AUDIO_RIGHT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.row_chat_audio_right, parent, false);
            return new AudioViewHolderRight(view);

        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final Chat chat = mChat.get(position);
        if (holder instanceof TextViewHolder) {
            final TextViewHolder textViewHolder = (TextViewHolder) holder;
            textViewHolder.show_message.setText(chat.getMessage());
            textViewHolder.show_message.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ClipboardManager cManager = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData cData = ClipData.newPlainText("text", textViewHolder.show_message.getText().toString());
                    cManager.setPrimaryClip(cData);
                    Toast.makeText(mContext, mContext.getString(R.string.copy), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            if (imageurl.equals("default")) {
                textViewHolder.profile_image.setImageResource(R.mipmap.ic_launcher);
            } else {
                Glide.with(mContext).load(imageurl).into(textViewHolder.profile_image);
            }
            if (position == mChat.size() - 1) {
                if (chat.isIsseen()) {
                    textViewHolder.txt_seen.setBackgroundResource(R.drawable.ic_double_tick_seen);
                } else {
                    textViewHolder.txt_seen.setBackgroundResource(R.drawable.ic_double_tick_delivered);
                }
            } else {
                textViewHolder.txt_seen.setVisibility(View.GONE);
            }

            /**
             * Audio
             */
        } else if (holder instanceof AudioViewHolderLeft) {
            AudioViewHolderLeft audioViewHolder = (AudioViewHolderLeft) holder;
            audioViewHolder.bind(chat, position);

        } else if (holder instanceof AudioViewHolderRight) {
            AudioViewHolderRight audioViewHolder = (AudioViewHolderRight) holder;
            audioViewHolder.bind(chat, position);
        }


    }

    @Override
    public int getItemCount() {
        return mChat.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {
        public TextViewRegular show_message;
        public CircleImageView profile_image;
        public TextViewRegular txt_seen;

        public TextViewHolder(View itemView) {
            super(itemView);

            show_message = itemView.findViewById(R.id.show_message);
            profile_image = itemView.findViewById(R.id.profile_image);
            txt_seen = itemView.findViewById(R.id.txt_seen);
        }
    }


    public class AudioViewHolderRight extends RecyclerView.ViewHolder {
        private VoicePlayerView voicePlayerView;
        private Button btnStart;
        private ImageView imgPlay;
        private ImageView imgPause;

        public AudioViewHolderRight(View itemView) {
            super(itemView);
            btnStart = itemView.findViewById(R.id.btnStart);
            btnStart.setVisibility(View.GONE);
            voicePlayerView = itemView.findViewById(R.id.voicePlayerView);
            voicePlayerView.findViewById(R.id.imgShare).setVisibility(View.GONE);
            imgPlay = voicePlayerView.findViewById(R.id.imgPause);
            imgPause = voicePlayerView.findViewById(R.id.imgPlay);
        }

        public void bind(final Chat chat, final int position) {

            final String AUDIO_FILE_PATH = chat.getUrl();
            Log.v(TAG, "AUDIO_FILE_PATH : " + AUDIO_FILE_PATH);


            imgPause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProgressDialog.show();
                    voicePlayerView.setAudio(AUDIO_FILE_PATH);

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            new InitPlayer(position, voicePlayerView).execute();

                        }
                    }, 500);

                }
            });

            imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voicePlayerView.onImagePauseClick();

                }
            });

        }
    }

    public class AudioViewHolderLeft extends RecyclerView.ViewHolder {
        private VoicePlayerView voicePlayerView;
        private Button btnStart;
        private ImageView imgPlay;
        private ImageView imgPause;

        public AudioViewHolderLeft(View itemView) {
            super(itemView);
            btnStart = itemView.findViewById(R.id.btnStart);
            btnStart.setVisibility(View.GONE);
            voicePlayerView = itemView.findViewById(R.id.voicePlayerView);
            voicePlayerView.findViewById(R.id.imgShare).setVisibility(View.GONE);
            imgPlay = voicePlayerView.findViewById(R.id.imgPause);
            imgPause = voicePlayerView.findViewById(R.id.imgPlay);

        }

        public void bind(final Chat chat, final int position) {
            final String AUDIO_FILE_PATH = chat.getUrl();
            Log.v(TAG, "AUDIO_FILE_PATH : " + AUDIO_FILE_PATH);

            imgPause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProgressDialog.show();
                    voicePlayerView.setAudio(AUDIO_FILE_PATH);


                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            new InitPlayer(position, voicePlayerView).execute();

                        }
                    }, 100);

                }
            });

            imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voicePlayerView.onImagePauseClick();

                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        Chat chat = mChat.get(position);
        if (chat.getSender().equals(fuser.getUid())) {

            if (chat.getViewType().equalsIgnoreCase(UploadType.AUDIO)) {
                return MSG_TYPE_AUDIO_RIGHT;
            } else {
                return MSG_TYPE_RIGHT;
            }


        } else {

            if (chat.getViewType().equalsIgnoreCase(UploadType.AUDIO)) {
                return MSG_TYPE_AUDIO_LEFT;
            } else {
                return MSG_TYPE_LEFT;
            }


        }
    }

    private void stopPlayer(int position) {
        if (currentClickedPosition != -1) {
            try{
                VoicePlayerView voicePlayerView = mediaPlayerHashMap.get(currentClickedPosition);
                voicePlayerView.onStop();
            }catch (Exception e){
                e.printStackTrace();
            }



        }

        currentClickedPosition = position;

    }

    class InitPlayer extends AsyncTask<Void, Void, Void> {
        private int position;
        private VoicePlayerView voicePlayerView;

        public InitPlayer(int position, VoicePlayerView voicePlayerView) {
            this.position = position;
            this.voicePlayerView = voicePlayerView;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... voids) {
            /**
             * init player
             */
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (position != currentClickedPosition && voicePlayerView != null) {
                        stopPlayer(position);
                    }
                }
            });



            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
            mediaPlayerHashMap.put(position, voicePlayerView);
            voicePlayerView.onImagePlayClick();

        }
    }
}