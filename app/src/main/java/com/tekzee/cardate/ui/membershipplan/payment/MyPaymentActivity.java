//package com.tekzee.cardate.ui.membershipplan.payment;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.paypal.android.sdk.payments.PayPalAuthorization;
//import com.paypal.android.sdk.payments.PayPalConfiguration;
//import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
//import com.paypal.android.sdk.payments.PayPalItem;
//import com.paypal.android.sdk.payments.PayPalOAuthScopes;
//import com.paypal.android.sdk.payments.PayPalPayment;
//import com.paypal.android.sdk.payments.PayPalPaymentDetails;
//import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
//import com.paypal.android.sdk.payments.PayPalService;
//import com.paypal.android.sdk.payments.PaymentActivity;
//import com.paypal.android.sdk.payments.PaymentConfirmation;
//import com.paypal.android.sdk.payments.ShippingAddress;
//import com.tekzee.cardate.BuildConfig;
//import com.tekzee.cardate.R;
//import com.tekzee.cardate.custom.TextViewRegular;
//import com.tekzee.cardate.ui.base.MvpActivity;
//import com.tekzee.cardate.ui.membershipplan.PlanModel;
//import com.tekzee.cardate.utils.Log;
//import com.tekzee.cardate.utils.MyCustomDialog;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.math.BigDecimal;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.Set;
//
//import cn.refactor.lib.colordialog.PromptDialog;
//
//public class MyPaymentActivity extends MvpActivity<PaymentPresenter> implements PaymentView {
//    private PlanModel model;
//    private double totalPayable;
//    private String paymentId;
//
//    private static final String TAG = MyPaymentActivity.class.getSimpleName();
//    /**
//     * https://developer.paypal.com
//     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
//     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
//     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires without communicating to PayPal's servers.
//     */
//    private static final String ENVIRONMENT_SANDBOX = PayPalConfiguration.ENVIRONMENT_SANDBOX;
//    private static final String ENVIRONMENT_PRODUCTION = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
//
//    /**
//     * Sandbox account
//     * seloaba-facilitator@gmail.com
//     * API CREDENTIALS
//     */
//    private static final String CLIENT_ID_SANDBOX = "AUezKCuIQaAy2vR3rPkic1wgBoTDLy7L-l5uthPojN1-tguMLnuW9Un1ZBTfMp7eS17jlT11nXgNyNtX";
//    private static final String CLIENT_ID_PRODUCTION = "AccSdc2vLeeiC9QYmRn8O7MfDaLxYBwyGvWWZ4D5eyijComyzMHPaQ3vph5xBRTMTyJk2uh5QkHt7wrY";
//
//    private static final int REQUEST_CODE_PAYMENT = 1;
////    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
////    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
//
//    private static PayPalConfiguration config = new PayPalConfiguration()
//            .environment((BuildConfig.DEBUG) ? PayPalConfiguration.ENVIRONMENT_SANDBOX : PayPalConfiguration.ENVIRONMENT_PRODUCTION)
//            .clientId((BuildConfig.DEBUG) ? "AUezKCuIQaAy2vR3rPkic1wgBoTDLy7L-l5uthPojN1-tguMLnuW9Un1ZBTfMp7eS17jlT11nXgNyNtX" : "AccSdc2vLeeiC9QYmRn8O7MfDaLxYBwyGvWWZ4D5eyijComyzMHPaQ3vph5xBRTMTyJk2uh5QkHt7wrY")
//            // The following are only used in PayPalFuturePaymentActivity.
//            .merchantName("CarDate")
//            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
//            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        init();
//
//        findViewById(R.id.toolbar_navigation_back).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onBackPressed();
//
//            }
//        });
//        findViewById(R.id.btnPay).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                mvpPresenter.insertPayment(model);
//
//            }
//        });
//
//
//        Intent intent = new Intent(this, PayPalService.class);
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//        startService(intent);
//    }
//
//
//    public void onBuyPressed(String currency) {
//        /*
//         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
//         * Change PAYMENT_INTENT_SALE to
//         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
//         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
//         *     later via calls from your server.
//         *
//         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
//         */
//        PayPalPayment thingToBuy = getThingToBuy(currency, PayPalPayment.PAYMENT_INTENT_SALE);
//
//        /*
//         * See getStuffToBuy(..) for examples of some available payment options.
//         */
//
//        Intent intent = new Intent(getAppContext(), PaymentActivity.class);
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
//        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
//        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
//    }
//
//    private PayPalPayment getThingToBuy(String currency, String paymentIntent) {
//        return new PayPalPayment(new BigDecimal(totalPayable), currency, model.getDuration() + getResources().getString(R.string.month_membership_plan), paymentIntent);
//    }
//
//
//    private void init() {
//        model = (PlanModel) getIntent().getSerializableExtra("model");
//
//        ((TextViewRegular) findViewById(R.id.tvMonth)).setText(model.getDuration() + getResources().getString(R.string.month));
//        ((TextViewRegular) findViewById(R.id.tvPerMonthCharge)).setText(model.getCurrency() + model.getAmount());
//
//        totalPayable = (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount()));
//        ((TextViewRegular) findViewById(R.id.tvTotal)).setText(model.getCurrency() + totalPayable);
//
//    }
//
//    @Override
//    protected PaymentPresenter createPresenter() {
//        return new PaymentPresenter(this, getAppContext());
//    }
//
//    @Override
//    public Activity getAppContext() {
//        return this;
//    }
//
//    @Override
//    public void showInPopup(String message) {
//        MyCustomDialog.show(getAppContext(), message);
//    }
//
//    @Override
//    public void showInSnackBar(String message) {
//
//        snackBarTop(R.id.payment_activity, message);
//
//    }
//
//    @Override
//    public void onPaymentSuccess(JSONObject jsonObject) {
//        String transaction_id = "";
//        try {
//
//            JSONObject clientObject = jsonObject.getJSONObject("client");
//            Log.view(TAG, "environment : " + clientObject.getString("environment"));
//            Log.view(TAG, "paypal_sdk_version : " + clientObject.getString("paypal_sdk_version"));
//            Log.view(TAG, "platform : " + clientObject.getString("environment"));
//            Log.view(TAG, "product_name : " + clientObject.getString("product_name"));
//
//            JSONObject responseObject = jsonObject.getJSONObject("response");
//            Log.view(TAG, "create_time : " + responseObject.getString("create_time"));
//            transaction_id = responseObject.getString("id");
//            Log.view(TAG, "intent : " + responseObject.getString("intent"));
//            Log.view(TAG, "state : " + responseObject.getString("state"));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        mvpPresenter.submitPaymentSuccess(transaction_id,paymentId,model.getId());
//
//    }
//
//    @Override
//    public void onOurLocalServerSuccess(String message) {
//        final PromptDialog dialog = new PromptDialog(getAppContext());
//        dialog.setCancelable(false);
//        dialog.setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
//                .setAnimationEnable(true)
//                .setTitleText(getAppContext().getResources().getString(R.string.app_name))
//                .setContentText(message)
//                .setPositiveListener(getAppContext().getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
//                    @Override
//                    public void onClick(PromptDialog dialog1) {
//                        dialog.dismiss();
//
//                        finish();
//                    }
//                }).show();
//    }
//
//    @Override
//    public void onPaymentFail(String reason) {
//
//        showInPopup(reason);
//
//    }
//
//    @Override
//    public void onPaymentReceived(JSONObject data) {
//        try {
//
//            onBuyPressed(data.getString("currency"));
//            paymentId = data.getString("payment_id");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    /*
//     * This method shows use of optional payment details and item list.
//     */
////    private PayPalPayment getStuffToBuy(String paymentIntent) {
////        //--- include an item list, payment amount details
////        PayPalItem[] items =
////                {
////                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD", "sku-12345678"),
////                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"), "USD", "sku-zero-price"),
////                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"), "USD", "sku-33333")
////                };
////        BigDecimal subtotal = PayPalItem.getItemTotal(items);
////        BigDecimal shipping = new BigDecimal("7.21");
////        BigDecimal tax = new BigDecimal("4.67");
////        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
////        BigDecimal amount = subtotal.add(shipping).add(tax);
////        PayPalPayment payment = new PayPalPayment(amount, "USD", "sample item", paymentIntent);
////        payment.items(items).paymentDetails(paymentDetails);
////
////        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
////        payment.custom("This is text that will be associated with the payment that the app can use.");
////
////        return payment;
////    }
//
//    /*
//     * Add app-provided shipping address to payment
//     */
////    private void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
////        ShippingAddress shippingAddress =
////                new ShippingAddress().recipientName("Mom Parker").line1("52 North Main St.")
////                        .city("Austin").state("TX").postalCode("78729").countryCode("US");
////        paypalPayment.providedShippingAddress(shippingAddress);
////    }
//
//    /*
//     * Enable retrieval of shipping addresses from buyer's PayPal account
//     */
////    private void enableShippingAddressRetrieval(PayPalPayment paypalPayment, boolean enable) {
////        paypalPayment.enablePayPalShippingAddressesRetrieval(enable);
////    }
//
////    public void onFuturePaymentPressed(View pressed) {
////        Intent intent = new Intent(getAppContext(), PayPalFuturePaymentActivity.class);
////
////        // send the same configuration for restart resiliency
////        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
////
////        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
////    }
//
////    public void onProfileSharingPressed(View pressed) {
////        Intent intent = new Intent(getAppContext(), PayPalProfileSharingActivity.class);
////
////        // send the same configuration for restart resiliency
////        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
////
////        intent.putExtra(PayPalProfileSharingActivity.EXTRA_REQUESTED_SCOPES, getOauthScopes());
////
////        startActivityForResult(intent, REQUEST_CODE_PROFILE_SHARING);
////    }
//
////    private PayPalOAuthScopes getOauthScopes() {
////        /* create the set of required scopes
////         * Note: see https://developer.paypal.com/docs/integration/direct/identity/attributes/ for mapping between the
////         * attributes you select for this app in the PayPal developer portal and the scopes required here.
////         */
////        Set<String> scopes = new HashSet<String>(
////                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS));
////        return new PayPalOAuthScopes(scopes);
////    }
//
////    protected void displayResultText(String result) {
//////        ((TextView) findViewById(R.id.txtResult)).setText("Result : " + result);
////        Toast.makeText(
////                getApplicationContext(),
////                result, Toast.LENGTH_LONG)
////                .show();
////    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_CODE_PAYMENT) {
//            if (resultCode == Activity.RESULT_OK) {
//                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
//                if (confirm != null) {
//                    try {
//                        JSONObject responseObject = confirm.toJSONObject().getJSONObject("response");
//                        String state = responseObject.getString("state");
//                        Log.view(TAG, "state : " + state);
//                        if (state.equalsIgnoreCase("approved")) {
//                            onPaymentSuccess(confirm.toJSONObject());
//                        } else {
//                            onPaymentFail(getResources().getString(R.string.payment_fail));
//                        }
//
//
////                        Log.view(TAG, confirm.toString());
////                        Log.view(TAG, confirm.toJSONObject().toString(4));
////                        Log.view(TAG, confirm.getPayment().toJSONObject().toString(4));
//                        /**
//                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
//                         * or consent completion.
//                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
//                         * for more details.
//                         *
//                         * For sample mobile backend interactions, see
//                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
//                         */
//
//
//                    } catch (Exception e) {
//                        onPaymentFail(getResources().getString(R.string.payment_fail) + e);
//                    }
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                onPaymentFail(getResources().getString(R.string.payment_cancel));
//
//            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
//                onPaymentFail(getResources().getString(R.string.payment_invalid));
//            }
////        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
////            if (resultCode == Activity.RESULT_OK) {
////                PayPalAuthorization auth =
////                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
////                if (auth != null) {
////                    try {
////                        Log.view(TAG, "FuturePaymentExample" + auth.toJSONObject().toString(4));
////
////                        String authorization_code = auth.getAuthorizationCode();
////                        Log.view(TAG, "FuturePaymentExample" + authorization_code);
////
////                        sendAuthorizationToServer(auth);
////                        displayResultText("Future Payment code received from PayPal");
////
////                    } catch (JSONException e) {
////                        Log.view(TAG, "an extremely unlikely failure occurred: " + e);
////                    }
////                }
////            } else if (resultCode == Activity.RESULT_CANCELED) {
////                Log.view("FuturePaymentExample", "The user canceled.");
////            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
////                Log.view(
////                        "FuturePaymentExample",
////                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
////            }
////        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
////            if (resultCode == Activity.RESULT_OK) {
////                PayPalAuthorization auth =
////                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
////                if (auth != null) {
////                    try {
////                        Log.view(TAG, "ProfileSharingExample" + auth.toJSONObject().toString(4));
////
////                        String authorization_code = auth.getAuthorizationCode();
////                        Log.view(TAG, "ProfileSharingExample" + authorization_code);
////
////                        sendAuthorizationToServer(auth);
////                        displayResultText("Profile Sharing code received from PayPal");
////
////                    } catch (JSONException e) {
////                        Log.view(TAG, "ProfileSharingExample  an extremely unlikely failure occurred: " + e);
////                    }
////                }
////            } else if (resultCode == Activity.RESULT_CANCELED) {
////                Log.view(TAG, "ProfileSharingExample The user canceled.");
////            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
////                Log.view(TAG, "ProfileSharingExample Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
////            }
//        }
//    }
//
////    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
////
////        /**
////         * TODO: Send the authorization response to your server, where it can
////         * exchange the authorization code for OAuth access and refresh tokens.
////         *
////         * Your server must then store these tokens, so that your server code
////         * can execute payments for this user in the future.
////         *
////         * A more complete example that includes the required app-server to
////         * PayPal-server integration is available from
////         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
////         */
////
////    }
//
////    public void onFuturePaymentPurchasePressed(View pressed) {
////        // Get the Client Metadata ID from the SDK
////        String metadataId = PayPalConfiguration.getClientMetadataId(this);
////
////        Log.view(TAG, "FuturePaymentExample Client Metadata ID: " + metadataId);
////
////        // TODO: Send metadataId and transaction details to your server for processing with
////        // PayPal...
////        displayResultText("Client Metadata Id received from SDK");
////    }
//
//    @Override
//    public void onDestroy() {
//        // Stop service when done
//        stopService(new Intent(this, PayPalService.class));
//        super.onDestroy();
//    }
//}