package com.tekzee.cardate.ui.introduction;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface IntroductionView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onStartLoginActivity();

    void onStartSignUpActivity(String name, String email, String imageUrl, String gender);

    void onSliderFound(ArrayList<String> arrayList);

}
