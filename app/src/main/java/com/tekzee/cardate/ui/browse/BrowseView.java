package com.tekzee.cardate.ui.browse;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface BrowseView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onBrowseDataFail(String message);

    void onBrowseDataSuccess(ArrayList<BrowseModel> arrayList);

    void onStartBrowseDetailsActivity(BrowseModel model);
}
