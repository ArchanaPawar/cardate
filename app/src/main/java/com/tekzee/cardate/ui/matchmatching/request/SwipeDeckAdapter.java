package com.tekzee.cardate.ui.matchmatching.request;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewLight;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.utils.Log;

import java.util.ArrayList;

public class SwipeDeckAdapter extends BaseAdapter {
    private static final String TAG = SwipeDeckAdapter.class.getSimpleName();
    private ArrayList<RequestModel> data;
    private Context mContext;
    private ButtonClick listener;

    public SwipeDeckAdapter(ArrayList<RequestModel> data,ButtonClick listener) {
        this.data = data;
        this.listener = listener;
    }

    interface ButtonClick {
        void onCancel(RequestModel model);

        void onLike(RequestModel model);

        void onSuperLike(RequestModel model);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        mContext = parent.getContext();
        View mView = convertView;
        if (mView == null) {

            mView = LayoutInflater.from(mContext).inflate(R.layout.test_card2, parent, false);
        }

        final RequestModel model = data.get(position);
        ((TextViewRegular) mView.findViewById(R.id.tvUserName)).setText(model.getName());
        ((TextViewLight) mView.findViewById(R.id.tvAge)).setText(model.getAge() + ", " + model.getCountry());

        ImageView imageView = mView.findViewById(R.id.offer_image);
        String image = model.getImage();
        if (image != null && !image.equalsIgnoreCase("")) {

            Glide.with(mContext).load(image).into(imageView);
        } else {
            Glide.with(mContext).load(R.drawable.avatar).into(imageView);
        }

        ImageView ivLike = mView.findViewById(R.id.ivLike);
        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLike(model);
            }
        });

        ImageView ivSuperLike = mView.findViewById(R.id.ivSuperLike);
        ivSuperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSuperLike(model);
            }
        });

        ImageView ivCancel = mView.findViewById(R.id.ivCancel);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCancel(model);

            }
        });


//        TextView textView = mView.findViewById(R.id.sample_text);
//        String item = (String) getItem(position);
//        textView.setText(item);

//        mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.view(TAG, "Layer type: " + Integer.toString(v.getLayerType()));
//                Log.view(TAG, "Hardware Accel type:" + Integer.toString(View.LAYER_TYPE_HARDWARE));
//                    /*Intent i = new Intent(v.getContext(), BlankActivity.class);
//                    v.getContext().startActivity(i);*/
//            }
//        });


        return mView;
    }
}