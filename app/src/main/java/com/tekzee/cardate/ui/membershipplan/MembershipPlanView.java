package com.tekzee.cardate.ui.membershipplan;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface MembershipPlanView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPlanFoundSuccess(ArrayList<PlanModel> arrayList);

    void updateCurrentPlan(String planId, String planName, String startDate, String endDate, String duration);
}
