package com.tekzee.cardate.ui.matchmatching.likesyou;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;

public class LikesYouFragment extends MvpFragment<LikesYouPresenter> implements LikesYouView {
    private static final String TAG = LikesYouFragment.class.getSimpleName();
    private RecyclerView rvLikes;
    private boolean mViewCreated = false;
    private boolean mUserSeen = false;

    public static LikesYouFragment newInstance() {
        return new LikesYouFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_likes_you, container, false);
        init(mView);

        return mView;

    }

    private void init(View mView) {
        rvLikes = mView.findViewById(R.id.rvLikes);
        rvLikes.setLayoutManager(new LinearLayoutManager(getAppContext()));

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!mUserSeen && isVisibleToUser) {
            mUserSeen = true;
            tryViewCreatedFirstSight();
        }
        onUserVisibleChanged(isVisibleToUser);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewCreated = false;
        mUserSeen = false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewCreated = true;
        tryViewCreatedFirstSight();
    }


    private void tryViewCreatedFirstSight() {
        if (mUserSeen && mViewCreated) {
            onViewCreatedFirstSight(getView());
        }
    }

    /**
     * Called when the new created view is visible to user for the first time.
     */
    protected void onViewCreatedFirstSight(View view) {
//        mvpPresenter.getMyLikes("like");
    }

    /**
     * Called when the visible state to user has been changed.
     */
    protected void onUserVisibleChanged(boolean visible) {
        if(visible && mViewCreated){
            mvpPresenter.getMyLikes("like");
        }
    }

    @Override
    protected LikesYouPresenter createPresenter() {
        return new LikesYouPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onLikesYouSuccess(ArrayList<LikesYouModel> arrayList) {

        rvLikes.setAdapter(new LikesYouAdapter(arrayList, new LikesYouAdapter.Click() {
            @Override
            public void onCLick() {

            }
        }));

    }
}
