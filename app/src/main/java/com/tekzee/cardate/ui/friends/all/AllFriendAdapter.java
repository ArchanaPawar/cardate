package com.tekzee.cardate.ui.friends.all;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;

import java.util.ArrayList;

public class AllFriendAdapter extends RecyclerView.Adapter<AllFriendAdapter.ViewHolder> {
    private ArrayList<AllFriendModel> arrayList;
    private Click listener;
    private Context mContext;

    public AllFriendAdapter(ArrayList<AllFriendModel> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onCLick(AllFriendModel model);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_all_friend, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private CircleImageView ivProfileImage;
        private TextViewRegular tvProfileName;
        private TextViewRegular tvAgeCity;
        private ImageView ivOnline;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cvRoot = itemView.findViewById(R.id.cvRoot);
            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvAgeCity = itemView.findViewById(R.id.tvAgeCity);
            ivOnline = itemView.findViewById(R.id.ivOnline);
        }

        public void bind(final AllFriendModel model, final Click listener) {
            tvProfileName.setText(model.getName());
            tvProfileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(model);
                }
            });
            tvAgeCity.setText(model.getAge() + ", " + model.getCity());
            tvAgeCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(model);
                }
            });

            /**
             * check online
             */
            if (model.getIsOnline().equalsIgnoreCase("1")) {
                ivOnline.setVisibility(View.VISIBLE);
            } else {
                ivOnline.setVisibility(View.GONE);
            }

            /**
             * profile image
             */
            String imageUrl = model.getImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivProfileImage);
            }

            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(model.getName(), model.getImage());
                }
            });

            cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(model);
                }
            });

        }

        public void bigImageView(String title, String imageUrl) {
            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("imageUrl", imageUrl);
            mContext.startActivity(intent);

        }
    }
}
