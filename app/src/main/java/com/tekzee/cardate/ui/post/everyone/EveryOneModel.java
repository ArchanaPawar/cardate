package com.tekzee.cardate.ui.post.everyone;

public class EveryOneModel {
    private String postId;
    private String postImage;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String userId;
    private String userName;
    private String profileImage;

    public EveryOneModel(String postId, String postImage, String status, String createdAt, String updatedAt, String userId, String userName, String profileImage) {
        this.postId = postId;
        this.postImage = postImage;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.userId = userId;
        this.userName = userName;
        this.profileImage = profileImage;
    }

    public String getPostId() {
        return postId;
    }

    public String getPostImage() {
        return postImage;
    }

    public String getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getProfileImage() {
        return profileImage;
    }
}