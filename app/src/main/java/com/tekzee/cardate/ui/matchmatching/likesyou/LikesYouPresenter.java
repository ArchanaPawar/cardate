package com.tekzee.cardate.ui.matchmatching.likesyou;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LikesYouPresenter extends BasePresenter<LikesYouView> {
    private static final String TAG = LikesYouPresenter.class.getSimpleName();

    public LikesYouPresenter(LikesYouView view, Context mContext) {

        attachView(view,mContext);
    }


    public void getMyLikes(String filter) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("filter", filter);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getAllMatching(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            ArrayList<LikesYouModel> arrayList = new ArrayList<>();
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject object = mJsonArray.getJSONObject(i);
                                arrayList.add(new LikesYouModel(
                                        object.getString("id"),
                                        object.getString("name"),
                                        object.getString("date_of_birth"),
                                        object.getString("age"),
                                        object.getString("city"),
                                        object.getString("city_id"),
                                        object.getString("country"),
                                        object.getString("country_id"),
                                        object.getString("image")
                                ));

                            }

                            mvpView.onLikesYouSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
