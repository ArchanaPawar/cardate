package com.tekzee.cardate.ui.chat.message;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class MessagePresenter extends BasePresenter<MessageView> {
    private static final String TAG = MessagePresenter.class.getCanonicalName();

    public MessagePresenter(MessageView view, Context mContext) {

        attachView(view,mContext);
    }

    public void sendNotification(String fcmToken,String body,String userId) {
            JSONObject input = new JSONObject();
            try {
                input.put("fcm_token", fcmToken);
                input.put("title", "New Message");
                input.put("body", body);
                input.put("userid", userId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.sendNotification(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {

                }

                @Override
                public void onFailure(String msg) {

                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {

                }
            });

    }
}
