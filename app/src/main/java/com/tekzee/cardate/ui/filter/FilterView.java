package com.tekzee.cardate.ui.filter;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;
import com.tekzee.cardate.ui.signup.CountryModel;

public interface FilterView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void initCountry();

    void initCity();

    void initGender();

    void initAge();

    void initLocation();

    void resetAllFilter();

    void onCountrySelected(CountryModel model, String type);
}
