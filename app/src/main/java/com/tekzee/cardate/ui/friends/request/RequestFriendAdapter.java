package com.tekzee.cardate.ui.friends.request;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;

import java.util.ArrayList;

public class RequestFriendAdapter extends RecyclerView.Adapter<RequestFriendAdapter.ViewHolder> {
    private ArrayList<RequestFriendModel> arrayList;
    private Click listener;
    private Context mContext;

    public RequestFriendAdapter(ArrayList<RequestFriendModel> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onRequestAccept(RequestFriendModel model);
        void onRequestReject(RequestFriendModel model);
        void onDetailsClick(RequestFriendModel model);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_request_friend, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfileImage;
        private TextViewRegular tvProfileName;
        private TextViewRegular tvAgeCity;
        private ImageView btnAccept;
        private ImageView btnReject;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvAgeCity = itemView.findViewById(R.id.tvAgeCity);
            btnAccept = itemView.findViewById(R.id.btnAccept);
            btnReject = itemView.findViewById(R.id.btnReject);

        }

        public void bind(final RequestFriendModel model, final Click listener) {
            tvProfileName.setText(model.getName());
            tvProfileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDetailsClick(model);
                }
            });
            tvAgeCity.setText(model.getAge()+", "+model.getCity());
            tvAgeCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDetailsClick(model);
                }
            });

            /**
             * profile image
             */
            String imageUrl = model.getImage();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivProfileImage);
            }
            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bigImageView(model.getName(), model.getImage());
                }
            });

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRequestAccept(model);
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onRequestReject(model);
                }
            });

        }

        public void bigImageView(String title, String imageUrl) {
            Intent intent = new Intent(mContext, DocumentViewerActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("imageUrl", imageUrl);
            mContext.startActivity(intent);

        }
    }
}
