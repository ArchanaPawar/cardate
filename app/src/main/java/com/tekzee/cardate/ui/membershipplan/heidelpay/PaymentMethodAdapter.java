package com.tekzee.cardate.ui.membershipplan.heidelpay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tekzee.cardate.R;

import java.util.ArrayList;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {
    private ArrayList<String> arrayList;
    private Click listener;
    private Context mContext;
    private String selectedPaymentMethod = "";

    public PaymentMethodAdapter(ArrayList<String> arrayList, Click listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    interface Click {
        void onCLick(String paymentMethod);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_payment_method, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.bind(arrayList.get(position), listener);
        if (arrayList.get(position).equalsIgnoreCase(selectedPaymentMethod)) {
            viewHolder.rbPayment.setChecked(true);
            viewHolder.rbPayment.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

        } else {
            viewHolder.rbPayment.setChecked(false);
            viewHolder.rbPayment.setTextColor(mContext.getResources().getColor(R.color.textColorLight));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llRoot;
        private RadioButton rbPayment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            llRoot = itemView.findViewById(R.id.llRoot);
            rbPayment = itemView.findViewById(R.id.rbPayment);

        }

        public void bind(final String paymentMethod, final Click listener) {
            rbPayment.setText(paymentMethod);

            rbPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(paymentMethod);
                    selectedPaymentMethod = paymentMethod;
                    notifyDataSetChanged();
                }
            });

            llRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCLick(paymentMethod);
                    notifyDataSetChanged();
                }
            });

        }
    }
}
