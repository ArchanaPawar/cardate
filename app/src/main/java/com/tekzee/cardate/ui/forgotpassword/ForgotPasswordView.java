package com.tekzee.cardate.ui.forgotpassword;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface ForgotPasswordView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showAlert(String message);

    void showInSnackBar(String message);

    void onStartLoginActivity();

    String getEmail();

}
