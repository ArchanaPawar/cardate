package com.tekzee.cardate.ui.signup;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface SignUpView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onStartLoginActivity();

    String getFullName();

    String getEmail();

    String getGender();

    String getDOB();

//    String getCity();
//
//    String getCountry();

    String getPassword();

    String getConformPassword();

    String getImageUrl();

    void setImageUrl(String imageUrl);

//    void onCountrySelected(CountryModel model, String type);

    void onGetPasswordSuccess(String password, String fcmToken);
}
