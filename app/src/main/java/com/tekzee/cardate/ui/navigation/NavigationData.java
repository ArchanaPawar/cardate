package com.tekzee.cardate.ui.navigation;

/**
 * Created by Hasnain on 3-Apr-18.
 */

public class NavigationData {
    private String name;
    private String count;
    private int DrawableId;
    private boolean selected;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableId() {
        return DrawableId;
    }

    public void setDrawableId(int drawableId) {
        DrawableId = drawableId;
    }


}
