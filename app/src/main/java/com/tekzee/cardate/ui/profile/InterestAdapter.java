package com.tekzee.cardate.ui.profile;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CheckBoxRegular;

import java.util.ArrayList;

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.ViewHolder>  {
    private ArrayList<InterestModel> arrayList;
    private RowSelect listener;
    private ArrayList<String> interestId;

    public InterestAdapter(ArrayList<InterestModel> arrayList,ArrayList<String> interestId, RowSelect listener) {
        this.arrayList = arrayList;
        this.listener = listener;
        this.interestId = interestId;
    }

    public interface RowSelect {
        void onSelect(InterestModel model);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_interest, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(arrayList.get(i),interestId, listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBoxRegular cbIntereset;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cbIntereset = itemView.findViewById(R.id.cbIntereset);
        }


        public void bind(final InterestModel model, ArrayList<String> interestId, final RowSelect listener) {

            cbIntereset.setText(model.getName());

            if(interestId.contains(model.getId())){
                cbIntereset.setChecked(true);
                model.setSelected(true);
            }else{
                model.setSelected(false);
                cbIntereset.setChecked(false);
            }


            cbIntereset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(model.isSelected()){
                        cbIntereset.setChecked(false);
                        model.setSelected(false);
                    }else{
                        cbIntereset.setChecked(true);
                        model.setSelected(true);
                    }
                }
            });

        }
    }
}
