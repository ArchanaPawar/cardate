//package com.tekzee.cardate.ui.membershipplan.braintree;
//
//import android.app.Activity;
//
//import com.tekzee.cardate.ui.base.BaseView;
//
//import org.json.JSONObject;
//
//public interface BrainTreeView extends BaseView {
//
//    Activity getAppContext();
//
//    void showInPopup(String message);
//
//    void showInSnackBar(String message);
//
//    void onPaymentSuccess(JSONObject mJsonObject);
//
//    void onOurLocalServerSuccess(String message);
//
//    void onPaymentFail(String reason);
//
//    void onTokenReceived(JSONObject data);
//
//    void launchDropIn();
//
//    void launchGooglePayment();
//
//    void launchCards();
//
//    void launchPayPal();
//
//    void launchVenmo();
//
//    void launchVisaCheckout();
//
//    void launchLocalPayments();
//}