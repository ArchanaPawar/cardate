package com.tekzee.cardate.ui.friends.request;

public class RequestFriendModel {
    private String id;
    private String dateOfBirth;
    private String image;
    private String name;
    private String age;
    private String city;
    private String cityId;
    private String isOnline;

    public RequestFriendModel(String id, String dateOfBirth, String image, String name, String age, String city, String cityId, String isOnline) {
        this.id = id;
        this.dateOfBirth = dateOfBirth;
        this.image = image;
        this.name = name;
        this.age = age;
        this.city = city;
        this.cityId = cityId;
        this.isOnline = isOnline;
    }

    public String getId() {
        return id;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public String getCityId() {
        return cityId;
    }

    public String getIsOnline() {
        return isOnline;
    }
}