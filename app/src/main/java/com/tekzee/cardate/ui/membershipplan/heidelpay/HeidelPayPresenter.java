package com.tekzee.cardate.ui.membershipplan.heidelpay;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.heidelpay.android.ui.BicEditText;
import com.heidelpay.android.ui.CardExpiryEditText;
import com.heidelpay.android.ui.CreditCardNumberEditText;
import com.heidelpay.android.ui.CvvEditText;
import com.heidelpay.android.ui.FormattingEditText;
import com.heidelpay.android.ui.IBANEditText;
import com.heidelpay.android.ui.model.BICInput;
import com.heidelpay.android.ui.model.CardExpiryInput;
import com.heidelpay.android.ui.model.CreditCardInput;
import com.heidelpay.android.ui.model.CvvInput;
import com.heidelpay.android.ui.model.IBANInput;
import com.heidelpay.android.ui.model.Input;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.ui.membershipplan.PlanModel;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class HeidelPayPresenter extends BasePresenter<HeidelPayView> {
    private static final String TAG = HeidelPayPresenter.class.getSimpleName();
    private Dialog dialog;

    public HeidelPayPresenter(HeidelPayView view, Context mContext) {
        attachView(view, mContext);
    }

    public void insertPayment(PlanModel model, String paymentId) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("plan_id", model.getId());
                input.put("paymentId", paymentId);
                input.put("amount", (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount())));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);
            Log.view(TAG, "AUTH_TOKEN : " + "Bearer " + Utility.getSharedPreferences(mvpView.getAppContext(), Constant.AUTH_TOKEN));

            addSubscription(apiStores.insertPayment(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String orderId = dataObject.getString("payment_id");

                            mvpView.onInsertPaymentSuccess(dataObject.getJSONObject("payment_info"), paymentId,orderId);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }
    }

    public void openCardDetailsSelector() {
        final Dialog dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_card_details_select);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            CreditCardNumberEditText tvCreditCardNumberEditText = dialog.findViewById(R.id.tvCreditCardNumberEditText);
            CardExpiryEditText tvCardExpiryEditText = dialog.findViewById(R.id.tvCardExpiryEditText);
            CvvEditText tvCvvEditText = dialog.findViewById(R.id.tvCvvEditText);

            /**
             * 4711100000000000
             * 12/23
             * 123
             * secret3
             */

            dialog.findViewById(R.id.btnContinue).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    CreditCardInput cardInput = tvCreditCardNumberEditText.getUserInput();
                    CardExpiryInput expiryInput = tvCardExpiryEditText.getUserInput();
                    CvvInput cvcInput = tvCvvEditText.getUserInput();
                    if (cardInput.getValid() && cvcInput.getValid() && expiryInput.getValid()) {

                        mvpView.makeCardPayment(cardInput, expiryInput, cvcInput);

                    } else {

                        mvpView.onToast(mvpView.getAppContext().getString(R.string.invlid_card_details));

                    }


                }
            });

            dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openSepaDirectDebitSelector() {
        final Dialog dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_sepa_direct_debit_select);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            IBANEditText etIBANEditText = dialog.findViewById(R.id.etIBANEditText);
            BicEditText etBicEditText = dialog.findViewById(R.id.etBicEditText);
            EditTextRegular etHolder = dialog.findViewById(R.id.etHolder);

            /**
             * DE89370400440532013000
             * COBADEFFXXX
             * Holder
             */

            dialog.findViewById(R.id.btnContinue).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    IBANInput ibanInput = etIBANEditText.getUserInput();
                    BICInput bicInput = etBicEditText.getUserInput();
                    String holder = etHolder.getText().toString().trim();

                    if (ibanInput.getValid() && bicInput.getValid() && !holder.equalsIgnoreCase("")) {

                        mvpView.makeSepaDirectDebitPayment(ibanInput, bicInput, holder);

                    } else {

                        mvpView.onToast(mvpView.getAppContext().getString(R.string.invlid_card_details));

                    }


                }
            });

            dialog.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPaymentPopUp(String redirectUrl, final String returnUrl, String paymentId, String orderId) {
        dialog = new Dialog(mvpView.getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.dialog_payment);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);

            final WebView wv_payment = dialog.findViewById(R.id.wv_payment);
            WebSettings settings = wv_payment.getSettings();
            settings.setDomStorageEnabled(true);
            settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            wv_payment.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    String webViewUrl = request.getUrl().toString();
                    if (webViewUrl.startsWith(returnUrl)) {
                        mvpView.heidelPayChargeFragmentDidFinish(true, paymentId,orderId);
                        dialog.dismiss();
                    } else {
                        view.loadUrl(webViewUrl);
                    }

                    return super.shouldOverrideUrlLoading(view, request);

                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    Log.view(TAG, url);
                    Log.view(TAG, returnUrl);
                    if (url.startsWith(returnUrl)) {
                        mvpView.heidelPayChargeFragmentDidFinish(false, paymentId,orderId);
                        dialog.dismiss();
                    } else {
                        Log.view(TAG, "onPageFinished else");
                    }

                }
            });

            settings.setJavaScriptEnabled(true);
            wv_payment.loadUrl(redirectUrl);

            /**
             * Close button
             */
            dialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(dialog.getContext());
                    builder.setMessage(R.string.cancel_payment_message)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface mDialog, int id) {
                                    mDialog.dismiss();

                                    dialog.dismiss();


                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface mDialog, int id) {
                                    mDialog.dismiss();
                                }
                            });
                    builder.create().show();

                }
            });

            dialog.show();

        } catch (Exception e) {
            mvpView.onToast(e.getMessage());
            e.printStackTrace();
        }
    }

    public void submitPaymentSuccess(PlanModel model, String paymentId,String orderId) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {

                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("plan_id", model.getId());
                input.put("payment_id", paymentId);
                input.put("order_id", orderId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.validateTransactionV1(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onOurLocalServerSuccess(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }

    public void getTnCPage() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);

            JSONObject input = new JSONObject();
            try {
                /**
                 * 1 => Payment Terms & Condition
                 * 2 => Imprint
                 * 3 => Privacy & Policy
                 * 4 => Terms of Uses
                 */
                input.put("page_type", "1");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getTnC_unsubscribe(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onTnCPageLoad(jsonObject.getJSONObject("data").getString("general_url"),jsonObject.getJSONObject("data").getString("impressum_url"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
