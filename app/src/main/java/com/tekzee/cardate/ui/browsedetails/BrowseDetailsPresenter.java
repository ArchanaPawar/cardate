package com.tekzee.cardate.ui.browsedetails;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class BrowseDetailsPresenter extends BasePresenter<BrowseDetailsView> {
    private static final String TAG = BrowseDetailsPresenter.class.getSimpleName();

    public BrowseDetailsPresenter(BrowseDetailsView view, Context mContext) {

        attachView(view,mContext);
    }

    public void getBrowseDetails(Bundle bundle) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("loginuser_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("user_lat", bundle.getDouble("lat"));
                input.put("user_long", bundle.getDouble("long"));
                input.put("user_id", bundle.getString("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getUserDetail(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onProfileDataSuccess(jsonObject.getJSONObject("data"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }


                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void sendFriendRequest(Bundle bundle) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("friend_id", bundle.getString("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.sendFriendRequest(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.showDialog(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void blockUser(Bundle bundle) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("blockuser_id", bundle.getString("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.blockUser(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.showDialog(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void acceptFriendRequest(Bundle bundle) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("friend_id", bundle.getString("id"));
                input.put("is_accept", "1");

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.acceptRejectRequest(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.showDialog(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }

    public void isUserBlocked(String blockUserId ) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("blockuser_id", blockUserId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.checkBlockUser(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onUserBlocked(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.userNotBlocked();

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
