package com.tekzee.cardate.ui.image_viewer;


import android.content.Context;

import com.tekzee.cardate.ui.base.BasePresenter;

public class DocumentViewerPresenter extends BasePresenter<DocumentView> {

    public DocumentViewerPresenter(DocumentView view, Context mContext) {

        attachView(view,mContext);
    }
}
