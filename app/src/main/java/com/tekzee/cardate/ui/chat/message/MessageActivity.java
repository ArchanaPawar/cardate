package com.tekzee.cardate.ui.chat.message;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpMapActivity;
import com.tekzee.cardate.ui.browsedetails.BrowseDetailsActivity;
import com.tekzee.cardate.ui.chat.adapter.MessageAdapter;
import com.tekzee.cardate.ui.chat.model.Chat;
import com.tekzee.cardate.ui.chat.model.User;
//import com.tekzee.cardate.ui.chat.notifications.APIService;
import com.tekzee.cardate.ui.chat.notifications.Client;
import com.tekzee.cardate.ui.chat.notifications.Data;
import com.tekzee.cardate.ui.chat.notifications.MyResponse;
import com.tekzee.cardate.ui.chat.notifications.Sender;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.UploadType;
import com.tekzee.cardate.utils.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageActivity extends MvpMapActivity<MessagePresenter> implements MessageView {
    private static final String TAG = MessageActivity.class.getSimpleName();
    private static final String AUDIO_RECORDER_FOLDER = "/CarDate/Audio/Sent/";
    private CircleImageView ivProfileImage;
    private TextViewRegular toolbar_title;
    private RecyclerView recyclerViewData;
    private String userId;
    private MessageAdapter messageAdapter;
    private List<Chat> mchat;
    private EmojiconEditText etChat;
    private ImageView ivMood;
    private View rootView;
    private EmojIconActions emojIcon;
    private FirebaseUser firebaseUser;
    private StorageReference mStorageRef;
    private DatabaseReference reference;
    private ValueEventListener seenListener;
//    private APIService mApiService;
    private boolean notify = false;
    private MediaRecorder recorder = null;
    private MediaPlayer player = null;
    private String fileStoreLocation;
    private String profileImageUrl;
    private Location mLocation;
    private User userModel = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        userId = getIntent().getExtras().getString("userid");
        init();
        initUserModel();

        findViewById(R.id.toolbar_icon_navigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        ivProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (profileImageUrl.equalsIgnoreCase("default")) {
                    onToast(getResources().getString(R.string.image_details_not_found));
                } else {
                    Intent intent = new Intent(getAppContext(), DocumentViewerActivity.class);
                    intent.putExtra("title", toolbar_title.getText().toString());
                    intent.putExtra("imageUrl", profileImageUrl);
                    startActivity(intent);
                }

                ;

            }
        });

        toolbar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Location mLocation = getLocation();
                if (mLocation != null) {
                    Intent intent = new Intent(getAppContext(), BrowseDetailsActivity.class);
                    intent.putExtra("lat", mLocation.getLatitude());
                    intent.putExtra("long", mLocation.getLongitude());
                    intent.putExtra("id", userModel.getUserId());
                    startActivity(intent);

                } else {
                    onToast(getResources().getString(R.string.location_not_found));
                }

            }
        });

        findViewById(R.id.ivSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notify = true;
                String msg = etChat.getText().toString();
                if (!msg.equals("")) {
                    sendMessage(firebaseUser.getUid(), userId, msg, "", UploadType.TEXT);
                } else {
                    onToast(getResources().getString(R.string.alert_empty_message));
                }
                etChat.setText("");
            }
        });

        findViewById(R.id.ivRecorder).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        onToast(getResources().getString(R.string.alert_start_recording));
                        showInSnackBar(getResources().getString(R.string.alert_hold));
                        onStartRecording(getFilename());
                        return true;

                    case MotionEvent.ACTION_UP:
                        onToast(getResources().getString(R.string.alert_stop_recording));
                        Log.view(TAG, "stop Recording : " + fileStoreLocation);
                        onStopRecording();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                openDialogForAudioConformation();

                            }

                        }, 1000);


                        break;
                }
                return false;
            }
        });


//        mApiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.view(TAG, "DataSnapshot : " + dataSnapshot);
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {

                    toolbar_title.setText(user.getName());
                    profileImageUrl = user.getImageUrl();

                    if (profileImageUrl.equalsIgnoreCase("default")) {
                        ivProfileImage.setImageResource(R.mipmap.ic_launcher);
                    } else {
                        //and this
                        Glide.with(getApplicationContext()).load(profileImageUrl).into(ivProfileImage);
                    }

                    readMessages(firebaseUser.getUid(), userId, user.getImageUrl());
                } else {
                    onToast(getResources().getString(R.string.user_details_noot_found));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        emojIcon = new EmojIconActions(this, rootView, etChat, ivMood);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
//                onToast("Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
//                onToast("Keyboard closed");
            }
        });

        seenMessage(userId);

        getRequiredPermission();

    }

    private void openDialogForAudioConformation() {
        final Dialog dialog = new Dialog(getAppContext());
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.popup_audio_conformation);
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);


            dialog.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    try {
                        String filename = fileStoreLocation.substring(fileStoreLocation.lastIndexOf("/") + 1);
                        InputStream stream = new FileInputStream(fileStoreLocation);
                        uploadData(stream, filename);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            dialog.findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (deleteRecorderFile(new File(fileStoreLocation))) {
                        Log.view(TAG, "Deleted");
                    } else {
                        Log.view(TAG, "Not Deleted");
                    }
                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getAppContext());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getAppContext().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void seenMessage(final String userid) {
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        seenListener = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(userid)) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isseen", true);
                        snapshot.getRef().updateChildren(hashMap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected MessagePresenter createPresenter() {
        return new MessagePresenter(this, getAppContext());
    }

    private void init() {
        etChat = findViewById(R.id.etChat);
        rootView = findViewById(R.id.message_activity);
        ivMood = findViewById(R.id.ivMood);
        ivMood.setImageResource(R.drawable.ic_mood);

        recyclerViewData = findViewById(R.id.recyclerViewData);
        recyclerViewData.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerViewData.setLayoutManager(linearLayoutManager);

        ivProfileImage = findViewById(R.id.ivProfileImage);
        toolbar_title = findViewById(R.id.toolbar_title);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(userId);

    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void showInSnackBar(String message) {

        snackBarTop(R.id.message_activity, message);

    }

    @Override
    public void sendMessage(String sender, final String receiver, String message, String url, String type) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        hashMap.put("url", url);
        hashMap.put("viewType", type);
        hashMap.put("isseen", false);
        hashMap.put("timestamp",String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
        reference.child("Chats").push().setValue(hashMap);


        // add user to chat fragment
        final DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_CHAT_LIST)
                .child(firebaseUser.getUid())
                .child(userId);

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    chatRef.child("id").setValue(userId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference chatRefReceiver = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_CHAT_LIST)
                .child(userId)
                .child(firebaseUser.getUid());
        chatRefReceiver.child("id").setValue(firebaseUser.getUid());

        final String msg = message;

        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (notify) {

                    sendNotification(receiver, user.getName(), msg);
                }
                notify = false;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public String getUserMessage() {
        return "";
    }

    @Override
    public void onStartRecording(final String filePath) {
        this.fileStoreLocation = filePath;
        Log.view(TAG, "Start Recording : " + fileStoreLocation);
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(filePath);
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStopRecording() {
        if (null != recorder) {
            try {
                recorder.stop();
                recorder.reset();
                recorder.release();
                recorder = null;
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }


    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.view(TAG, "Error: " + what + ", " + extra);
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.view(TAG, "Warning: " + what + ", " + extra);
        }
    };


    @Override
    public void onStartPlaying(String fileName) {
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStopPlaying() {
        player.release();
        player = null;

    }

    private void getRequiredPermission() {
        Dexter.withActivity(getAppContext())
                .withPermissions(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {


                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    private void sendNotification(String receiver, final String username, final String message) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userModel = snapshot.getValue(User.class);
                    Log.view(TAG, userModel.getUserId() + " " + userModel.getName() + " mUser.getToken() : " + userModel.getToken());

//                    Data data = new Data(firebaseUser.getUid(), R.mipmap.ic_launcher, "7007", username + ": " + message, "New Message", firebaseUser.getUid());
//                    Sender sender = new Sender(data, userModel.getToken());

                    mvpPresenter.sendNotification(userModel.getToken(),message,userModel.getUserId());

//                    mApiService.sendNotification(sender)
//                            .enqueue(new Callback<MyResponse>() {
//                                @Override
//                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
//                                    if (response.code() == 200) {
//                                        if (response.body().success != 1) {
//                                            onToast(getResources().getString(R.string.alert_failed));
//                                        }
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(Call<MyResponse> call, Throwable t) {
//
//                                }
//                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initUserModel() {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS);
        Query query = tokens.orderByKey().equalTo(userId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userModel = snapshot.getValue(User.class);
                    Log.view(TAG, userModel.getUserId() + " " + userModel.getName() + " mUser.getToken() : " + userModel.getToken());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readMessages(final String myid, final String userid, final String imageurl) {
        mchat = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mchat.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(myid) && chat.getSender().equals(userid) || chat.getReceiver().equals(userid) && chat.getSender().equals(myid)) {
                        mchat.add(chat);
                    }

                    messageAdapter = new MessageAdapter(MessageActivity.this, mchat, imageurl);
                    recyclerViewData.setAdapter(messageAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        this.mLocation = location;
        progressDialog.dismiss();

    }

    public Location getLocation() {
        if (mLocation != null) {
            return mLocation;
        }

        return null;
    }

    private void currentUser(String userid) {
        Utility.setSharedPreference(getAppContext(), Constant.FIREBASE_USER_ID, userid);
    }

    private void status(String status) {
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        status("online");
        currentUser(userId);
    }

    @Override
    public void onPause() {
        super.onPause();
        reference.removeEventListener(seenListener);
//        status("offline");
        currentUser("none");
    }


    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }
    }


    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/AUD_" + System.currentTimeMillis() + ".mp3");
    }

    private void uploadData(InputStream stream, final String fileName) {
        if (stream != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getAppContext());
            showProgressDialog(false);
            StorageReference riversRef = mStorageRef.child("voice/" + fileName);
            riversRef.putStream(stream)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            dismissProgressDialog();
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful()) ;
                            Uri downloadUrl = urlTask.getResult();
                            sendMessage(firebaseUser.getUid(), userId, fileName, downloadUrl.toString(), UploadType.AUDIO);
                            Log.view(TAG, "downloadUrl : " + downloadUrl);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            dismissProgressDialog();
                            Log.view(TAG, exception.getMessage());
//                            exception.printStackTrace();
                        }
                    });
        } else {
            onToast(getResources().getString(R.string.alert_upload));
        }

    }


    private boolean deleteRecorderFile(final File file) {
        final String where = MediaStore.MediaColumns.DATA + "=?";
        final String[] selectionArgs = new String[]{file.getAbsolutePath()
        };
        final ContentResolver contentResolver = getContentResolver();
        final Uri filesUri = MediaStore.Files.getContentUri("external");

        contentResolver.delete(filesUri, where, selectionArgs);

        if (file.exists()) {

            contentResolver.delete(filesUri, where, selectionArgs);
        }
        return !file.exists();
    }


//    private String getFileName(Uri uri) {
//        String result = null;
//        if (uri.getScheme().equals("content")) {
//            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//            try {
//                if (cursor != null && cursor.moveToFirst()) {
//                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                }
//            } finally {
//                cursor.close();
//            }
//        }
//        if (result == null) {
//            result = uri.getPath();
//            int cut = result.lastIndexOf('/');
//            if (cut != -1) {
//                result = result.substring(cut + 1);
//            }
//        }
//
//        Log.view(TAG,"File Name : "+result);
//        return result;
//    }

}
