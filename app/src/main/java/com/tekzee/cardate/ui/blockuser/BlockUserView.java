package com.tekzee.cardate.ui.blockuser;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface BlockUserView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onUserListSuccess(ArrayList<BlockedUserModel> arrayList);

    void onUnBlockSuccess();

}
