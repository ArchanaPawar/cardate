package com.tekzee.cardate.ui.profile;

public class PhotoModel {
    private String id;
    private String userId;
//    private String title;
//    private String content;
    private String image;
    private String status;

    public PhotoModel(String id, String userId, String image, String status) {
        this.id = id;
        this.userId = userId;
//        this.title = title;
//        this.content = content;
        this.image = image;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

//    public String getTitle() {
//        return title;
//    }
//
//    public String getContent() {
//        return content;
//    }

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }
}
