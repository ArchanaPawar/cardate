package com.tekzee.cardate.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import android.text.Html;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.carnumber.CarNumberActivity;
import com.tekzee.cardate.ui.forgotpassword.ForgotPasswordActivity;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.ui.signup.SignUpActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

import cn.refactor.lib.colordialog.PromptDialog;

public class LoginActivity extends MvpActivity<LoginPresenter> implements LoginView {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private TextViewRegular txt_forgot_password;
    private TextViewRegular txt_sign_up;
    private FirebaseAuth mFirebaseAuth;
    private boolean isOtherPasswordCheck = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        findViewById(R.id.toolbar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartIntroductionActivity();
            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartForgotPasswordActivity();
            }
        });

        txt_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartSignUpActivity();
            }
        });

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getEmail().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_email));

                } else if (!Utility.emailValidator(getEmail())) {
                    showInSnackBar(getResources().getString(R.string.alert_invalid_email));

                } else if (getPassword().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_password));

                } else if (getPassword().length() < 6) {
                    showInSnackBar(getResources().getString(R.string.alert_password_length));

                } else {

                    showProgressDialog(false);
                    getFCMToken();

                }
            }
        });
    }

    private void init() {
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        txt_forgot_password.setText(Html.fromHtml(getResources().getString(R.string.txt_forgot_password)));

        txt_sign_up = findViewById(R.id.txt_sign_up);
        txt_sign_up.setText(Html.fromHtml(getResources().getString(R.string.hint_signup_1)));
        /**
         * init Firebase Auth
         */
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.activity_login, message);
    }

    @Override
    public void onStartCarNumberActivity() {
        try {
            String imageUrl = Utility.getSharedPreferences(getAppContext(), Constant.PROFILE_IMAGE);
            if (imageUrl == null && imageUrl.equalsIgnoreCase("")) {
                imageUrl = "default";
            }
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("imageUrl", imageUrl);
            hashMap.put("userId", Utility.getSharedPreferences(getAppContext(), Constant.USER_ID));
            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.getSharedPreferences(getAppContext(), Constant.CAR_NUMBER).equalsIgnoreCase("")) {
            startActivity(new Intent(getAppContext(), CarNumberActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        } else {
            Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN, true);
            startActivity(new Intent(getAppContext(), NavigationActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        }


    }

    @Override
    public void onStartIntroductionActivity() {
        startActivity(new Intent(getAppContext(), IntroductionActivity.class));
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        finish();
    }

    @Override
    public void onStartSignUpActivity() {
        startActivity(new Intent(getAppContext(), SignUpActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onStartForgotPasswordActivity() {
        startActivity(new Intent(getAppContext(), ForgotPasswordActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();

    }

    @Override
    public String getEmail() {
        return ((EditTextRegular) findViewById(R.id.et_email)).getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return ((EditTextRegular) findViewById(R.id.et_password)).getText().toString().trim();
    }


    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            onToast(getResources().getString(R.string.get_instance_id_failed) + task.getException());
                            Log.view(TAG, "getInstanceId failed : " + task.getException());
                            return;
                        } else {
                            task.addOnFailureListener(getAppContext(), new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    e.printStackTrace();
                                    Log.view(TAG, "Error : " + e.getLocalizedMessage());

                                }
                            });
                        }

                        // Get new Instance ID token
                        String fcmToken = task.getResult().getToken();
                        Log.view(TAG, "InstanceID Token: " + fcmToken);

                        loginInFireBase(fcmToken, getPassword());


                    }
                });

    }


    private void loginInFireBase(final String fcmToken, String password) {

        Log.view(TAG, "Email: " + getEmail() + " password: " + password);

        mFirebaseAuth.signInWithEmailAndPassword(getEmail(), password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            continueWithLogin(fcmToken);

                        } else {

                            if (isOtherPasswordCheck) {
                                loginInFireBase(fcmToken, Constant.FIREBASE_PASSWORD);
                                isOtherPasswordCheck = false;

                            } else {

                                task.addOnFailureListener(getAppContext(), new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        e.printStackTrace();

//                                        showInPopup(getResources().getString(R.string.error_firebase_login));
                                        showInPopup(e.getLocalizedMessage());
                                        dismissProgressDialog();

                                        Log.view(TAG, "Error : " + e.getLocalizedMessage());

                                    }
                                });

                            }


                            /**
                             * update device token to firebase
                             */
//                            updateToken(fcmToken);

                            /**
                             * get user firebase id and send to your local server
                             */
//                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//                            mvpPresenter.loginUser(fcmToken, firebaseUser.getUid());
//
//
//                            showInPopup(getResources().getString(R.string.error_firebase_login));
//                            dismissProgressDialog();


                        }
                    }
                });

    }


    private void continueWithLogin(final String fcmToken) {
        /**
         * get user firebase id and send to your local server
         */
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            mvpPresenter.loginUser(fcmToken, firebaseUser.getUid());

        } else {
            showInPopup(getResources().getString(R.string.error_firebase_login));
            dismissProgressDialog();


        }


        /**
         * update device token to firbase
         */
//        updateToken(fcmToken);

    }

    private void updateToken(String refreshToken) {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("token", refreshToken);
            hashMap.put("userId", Utility.getSharedPreferences(getAppContext(), Constant.USER_ID));
            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onStartIntroductionActivity();
    }
}
