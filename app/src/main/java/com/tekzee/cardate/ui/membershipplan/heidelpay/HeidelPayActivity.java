package com.tekzee.cardate.ui.membershipplan.heidelpay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.heidelpay.android.Heidelpay;
import com.heidelpay.android.HeidelpayCreatePaymentTypeExtensionsKt;
import com.heidelpay.android.HeidelpayError;
import com.heidelpay.android.types.PaymentMethod;
import com.heidelpay.android.types.PaymentType;
import com.heidelpay.android.types.PublicKey;
import com.heidelpay.android.ui.model.BICInput;
import com.heidelpay.android.ui.model.CardExpiryInput;
import com.heidelpay.android.ui.model.CreditCardInput;
import com.heidelpay.android.ui.model.CvvInput;
import com.heidelpay.android.ui.model.IBANInput;
import com.heidelpay.android.ui.model.Input;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewBold;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.membershipplan.PlanModel;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.refactor.lib.colordialog.PromptDialog;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class HeidelPayActivity extends MvpActivity<HeidelPayPresenter> implements HeidelPayView {
    private static final String TAG = HeidelPayActivity.class.getSimpleName();
    /**
     * some place to hold the reference
     */
    private Heidelpay mHeidelpay;

    /**
     * create a PublicKey object
     */
    private PublicKey key = new PublicKey("s-pub-2a10lyNaOVJU3kuaH1yqgxcd3Qkfp1ip");

    private PlanModel model;
    private double totalPayable;
    private RecyclerView rvPaymentMethod;
    private String selectedPaymentMethod = "";
    private boolean isImprintChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heidel_pay);
        mActivity = this;

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.toolbar_navigation_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });


        findViewById(R.id.btnPay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.view(TAG, "selectedPaymentMethod : " + selectedPaymentMethod);
                if (selectedPaymentMethod.equalsIgnoreCase("")) {
                    onToast(getString(R.string.select_payment_method));

                } else {
                    mvpPresenter.getTnCPage();
                }

            }
        });

        /**
         * setup the HeidelPay instance
         */
        showProgressDialog(false);
        Heidelpay.Companion.setup(key, new Function2<Heidelpay, HeidelpayError, Unit>() {
            @Override
            public Unit invoke(Heidelpay chalPay, HeidelpayError heidelpayError) {
                dismissProgressDialog();
                if (heidelpayError != null) {
                    //handle error
                    if (heidelpayError instanceof HeidelpayError.NoInternetConnection) {
                        Log.view(TAG, heidelpayError.toString());
                        Log.view(TAG, "The server could not be reached because there seems to be no internet connection");

                    } else if (heidelpayError instanceof HeidelpayError.GeneralProcessingError) {
                        Log.view(TAG, heidelpayError.toString());
                        Log.view(TAG, "The request failed because of a technical issue. try again at a later time or contact technical support if the error persists");

                    } else if (heidelpayError instanceof HeidelpayError.NotAuthorized) {
                        Log.view(TAG, heidelpayError.toString());
                        Log.view(TAG, "The provided key is not authorized to use the HeidelPay service");

                    } else if (heidelpayError instanceof HeidelpayError.ServerError) {
                        /**
                         * the server reported an error
                         */
                        HeidelpayError.ServerError error = (HeidelpayError.ServerError) heidelpayError;
                        String customerMessage = error.getDetails().getCustomerMessage();
                        Log.view(TAG, "customerMessage : " + customerMessage);

                        String serverErrorCode = error.getDetails().getCode();
                        Log.view(TAG, "serverErrorCode : " + serverErrorCode);

                        /**
                         * internal message for logging or further error handling
                         */
                        String internalMessage = error.getDetails().getMerchantMessage();
                        Log.view(TAG, "internalMessage : " + internalMessage);
                    }

                } else {
                    mHeidelpay = chalPay;
                    Log.view(TAG, "Setup succeeded. store reference");

                    /**
                     * get available payment method
                     */
                    initPaymentMethod();


                }
                return null;
            }
        });

    }

    private void init() {
        model = (PlanModel) getIntent().getSerializableExtra("model");

        ((TextViewRegular) findViewById(R.id.tvMonth)).setText(model.getDuration() + getResources().getString(R.string.month));
        ((TextViewRegular) findViewById(R.id.tvPerMonthCharge)).setText(model.getCurrency() + model.getAmount());

        totalPayable = (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount()));
        ((TextViewRegular) findViewById(R.id.tvTotal)).setText(model.getCurrency() + totalPayable);

        rvPaymentMethod = findViewById(R.id.rvPaymentMethod);
        rvPaymentMethod.setLayoutManager(new LinearLayoutManager(getAppContext()));

    }

    @Override
    protected HeidelPayPresenter createPresenter() {
        return new HeidelPayPresenter(this, this);
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.rootView, message);

    }

    @Override
    public void onInsertPaymentSuccess(JSONObject data, String paymentId, String orderId) {
        try {
            /**
             * RedirectUrl & ReturnUrl received from backend
             */
            String redirectUrl = data.getString("redirectUrl");
            Log.view(TAG, "redirectUrl : " + redirectUrl);

            String returnUrl = data.getString("returnUrl");
            if (!returnUrl.contains("https")) {
                redirectUrl = redirectUrl.replace("http", "https");
            }

            Log.view(TAG, "returnUrl : " + returnUrl);

            mvpPresenter.openPaymentPopUp(redirectUrl, returnUrl, paymentId, orderId);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void makeCardPayment(CreditCardInput cardInput, CardExpiryInput expiryInput, CvvInput cvcInput) {
        try {
            HeidelpayCreatePaymentTypeExtensionsKt.createPaymentCard(
                    mHeidelpay,
                    cardInput.getCreditCardNumber(),
                    cvcInput.getCvv(),
                    expiryInput.getExpiryDate(),
                    true,
                    new Function2<PaymentType, Object, Unit>() {
                        @Override
                        public Unit invoke(PaymentType paymentType, Object error) {
                            if (error != null) {
                                // handle the error
                            } else if (paymentType != null) {
                                String paymentId = paymentType.getPaymentId();

                                // 1. transfer the payment id to your server
                                // 2. create a charge with the payment id on your server
                                ((TextViewRegular) findViewById(R.id.tvPaymentId)).setText("Payment ID : " + paymentId);
                                mvpPresenter.insertPayment(model, paymentId);
                            }

                            return null;
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initPaymentMethod() {
        ArrayList<String> paymentMethodArrayList = new ArrayList<>();

        /**
         * Alipay
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Alipay))
            paymentMethodArrayList.add("Alipay");

        /**
         * Credit Card Payment
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Card))
            paymentMethodArrayList.add("Credit Card Payment");

        /**
         * Ideal
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Ideal))
            paymentMethodArrayList.add("Ideal");

        /**
         * Invoice Factoring
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.InvoiceFactoring))
            paymentMethodArrayList.add("Invoice Factoring");

        /**
         *Invoice
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Invoice))
            paymentMethodArrayList.add("Invoice");

        /**
         *PIS
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.PIS))
            paymentMethodArrayList.add("PIS");

        /**
         * Sepa Direct Debit
         */
        //Close Sepa Direct Debit option requested by client ove skypee on 18-06-2020 at 07:22 PM
//        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.SepaDirectDebit))
//            paymentMethodArrayList.add("Sepa Direct Debit");

        /**
         * WeChat
         */

        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Wechatpay))
            paymentMethodArrayList.add("WeChat");

        /**
         *  Sofort Ueberweisung
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Sofort))
            paymentMethodArrayList.add(" Sofort Ueberweisung");

        /**
         * Sepa Direct Debit Guaranteed
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.SepaDirectDebitGuaranteed))
            paymentMethodArrayList.add("Sepa Direct Debit Guaranteed");

        /**
         *  Invoice Guaranteed
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.InvoiceGuaranteed))
            paymentMethodArrayList.add(" Invoice Guaranteed");

        /**
         * Giropay
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Giropay))
            paymentMethodArrayList.add("Giropay");

        /**
         * Prepayment
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Prepayment))
            paymentMethodArrayList.add("Prepayment");

        /**
         * Przelewy24
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Przelewy24))
            paymentMethodArrayList.add("Przelewy24");

        /**
         * PayPal
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Paypal))
            paymentMethodArrayList.add("PayPal");

        /**
         * Ideal
         */
        if (mHeidelpay.getPaymentMethods().contains(PaymentMethod.Ideal))
            paymentMethodArrayList.add("Ideal");

        /**
         * populate payment view
         */
        dismissProgressDialog();
        rvPaymentMethod.setAdapter(new PaymentMethodAdapter(paymentMethodArrayList, new PaymentMethodAdapter.Click() {
            @Override
            public void onCLick(String paymentMethod) {
                selectedPaymentMethod = paymentMethod;
            }
        }));


    }

    @Override
    public void onOurLocalServerSuccess(String message) {
        onToast(message);
        finish();

    }

    @Override
    public void makeSepaDirectDebitPayment(IBANInput ibanInput, BICInput bicInput, String holder) {
        try {
            HeidelpayCreatePaymentTypeExtensionsKt.createPaymentSepaDirect(
                    mHeidelpay,
                    ibanInput.getIban(),
                    bicInput.getBic(),
                    holder,
                    true,
                    new Function2<PaymentType, Object, Unit>() {
                        @Override
                        public Unit invoke(PaymentType paymentType, Object error) {
                            if (error != null) {
                                // handle the error
                                Log.view(TAG, "error : " + error.toString());

                            } else if (paymentType != null) {
                                String paymentId = paymentType.getPaymentId();

                                // 1. transfer the payment id to your server
                                // 2. create a charge with the payment id on your server
                                Log.view(TAG, "Payment ID : " + paymentId);

                                ((TextViewRegular) findViewById(R.id.tvPaymentId)).setText("Payment ID : " + paymentId);
                                mvpPresenter.insertPayment(model, paymentId);
                            } else {
                                Log.view(TAG, "Payment Type null!!!");
                            }

                            return null;
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTnCPageLoad(String generalUrl,String impressumUrl) {
        final Dialog dialog = new Dialog(getAppContext());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.popup_payment_tnc);
        dialog.getWindow().setLayout(-1, -2);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        ((TextViewBold) dialog.findViewById(R.id.title)).setText(getAppContext().getResources().getString(R.string.txt_payment_t_n_c));
        ImageView ivTermsOfUse =  dialog.findViewById(R.id.ivTermsOfUse);
        ivTermsOfUse.setVisibility(View.VISIBLE);
        TextView tvTermsOfUse =  dialog.findViewById(R.id.tvTermsOfUse);

        ImageView ivImprint =  dialog.findViewById(R.id.ivImprint);
        ivImprint.setImageResource(R.drawable.round_ractangle);
        isImprintChecked = false;
        TextView tvImprint =  dialog.findViewById(R.id.tvImprint);

        WebView wv = dialog.findViewById(R.id.webview);
        wv.loadUrl(generalUrl);

        tvTermsOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.loadUrl(generalUrl);
            }
        });

        tvImprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivImprint.setImageResource(R.drawable.ic_baseline_check_24);
                isImprintChecked = true;
                wv.loadUrl(impressumUrl);
            }
        });

        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        dialog.findViewById(R.id.btnAgreed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isImprintChecked){

                    dialog.dismiss();
                    switch (selectedPaymentMethod) {
                        case "Credit Card Payment":
                            mvpPresenter.openCardDetailsSelector();
                            break;

                        case "Sepa Direct Debit":
                            mvpPresenter.openSepaDirectDebitSelector();
                            break;

                        case "PayPal":
                            /**
                             * paypal-customer@heidelpay.de
                             * heidelpay
                             */
                            makePaypalPayment();
                            break;

                        default:
                            onToast(selectedPaymentMethod);

                    }

                }else{
                   onToast(getResources().getString(R.string.txt_alert_imprint));
                }
            }
        });

        dialog.findViewById(R.id.txtDecline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
//    public void makePaypalPayment(Input userName, Input password) {
    public void makePaypalPayment() {
        try {
            HeidelpayCreatePaymentTypeExtensionsKt.createPaymentPayPal(mHeidelpay,
                    new Function2<PaymentType, Object, Unit>() {
                        @Override
                        public Unit invoke(PaymentType paymentType, Object error) {
                            if (error != null) {
                                // handle the error
                                Log.view(TAG, "error : " + error.toString());

                            } else if (paymentType != null) {
                                String paymentId = paymentType.getPaymentId();

                                // 1. transfer the payment id to your server
                                // 2. create a charge with the payment id on your server
                                Log.view(TAG, "Payment ID : " + paymentId);

                                ((TextViewRegular) findViewById(R.id.tvPaymentId)).setText("Payment ID : " + paymentId);
                                mvpPresenter.insertPayment(model, paymentId);
                            } else {
                                Log.view(TAG, "Payment Type null!!!");
                            }

                            return null;
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void heidelPayChargeFragmentDidFinish(boolean paymentStatus, String paymentId, String orderId) {
        /**
         * close the finish charge view and
         * retrieve the status of the payment from your backend
         */
        if (paymentStatus) {
            mvpPresenter.submitPaymentSuccess(model, paymentId, orderId);
        } else {
            onToast("Failed");
        }
    }
}
