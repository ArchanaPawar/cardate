package com.tekzee.cardate.ui.profile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tekzee.cardate.BuildConfig;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.social_auth.instagramSignIn.InstagramHelper;
import com.tekzee.cardate.social_auth.instagramSignIn.InstagramResponse;
import com.tekzee.cardate.social_auth.instagramSignIn.InstagramUser;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.ui.image_viewer.DocumentViewerActivity;
import com.tekzee.cardate.ui.imageselector.ImageSelectorActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.refactor.lib.colordialog.ColorDialog;

public class ProfileFragment extends MvpFragment<ProfilePresenter> implements ProfileView, InstagramResponse {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    private View mView;
    private CircleImageView ivProfileImage;
    private RecyclerView rvPhotos;
    private TextViewRegular tvProfileName;
    private TextViewRegular tvAboutMe;
    private TextViewRegular tvOccupation;
    private TextViewRegular tvCarNumber;
    private TextViewRegular btnSubscribe;
    private ImageView ivChangeImage;
    private InputStream imageInputStream;
    public InstagramHelper mInstagramHelper;
    private String countryName = "";
    private String cityName = "";
    private ArrayList<String> interestIdList;
    private String interestName = "";
    private String UPLOAD_TYPE = "";

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_profile, container, false);
        init();

        tvProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mvpPresenter.openNameAgeEditDialog();

            }
        });


        tvCarNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mvpPresenter.updateCarNumber(tvCarNumber);

            }
        });

        mView.findViewById(R.id.tvEditAbout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mvpPresenter.openAboutMeEditorDialog(getActivity().getResources().getString(R.string.about_me), tvAboutMe, getAboutMe());

            }
        });

        mView.findViewById(R.id.tvHeadOccupation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mvpPresenter.openAboutMeEditorDialog(getActivity().getResources().getString(R.string.occupation), tvOccupation, getOccupation());

            }
        });

        mView.findViewById(R.id.tvEditInterestIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mvpPresenter.getAllInterestIn(interestIdList);

            }
        });


        ivChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dexter.withActivity(getAppContext())
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    UPLOAD_TYPE = "";
                                    showImagePickerOptions("profile");
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();


//                uploadImage();

            }
        });

//        mView.findViewById(R.id.tvLinkInstagram).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String mAccessToken = Utility.getSharedPreferences(getAppContext(), Constant.INSTA_ACCESS_TOKEN);
//                if (mAccessToken != null && !mAccessToken.equalsIgnoreCase("")) {
//                    mvpPresenter.getInstargramImages(mAccessToken);
//
//                } else {
//                    mInstagramHelper.performSignIn();
//                }
//
//            }
//        });


        /**
         * Add photo
         */
        mView.findViewById(R.id.ivAddPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(getAppContext())
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    UPLOAD_TYPE = "";
                                    showImagePickerOptions("post");
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();

            }
        });

        btnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnSubscribe.getText().toString().equalsIgnoreCase(getActivity().getResources().getString(R.string.menu_un_subscribe_plan))) {
                    /**
                     * cancel membership plan
                     */
                    ColorDialog dialog = new ColorDialog(getActivity());
                    dialog.setColor("#b51800");
                    dialog.setCancelable(false);
                    dialog.setAnimationEnable(true);
                    dialog.setTitle(getActivity().getResources().getString(R.string.app_name));
                    dialog.setContentText(getActivity().getResources().getString(R.string.message_unsubscribe));
                    dialog.setPositiveListener(getActivity().getResources().getString(R.string.yes), new ColorDialog.OnPositiveListener() {
                        @Override
                        public void onClick(ColorDialog dialog) {
                            dialog.dismiss();
                            mvpPresenter.unSubscribePlan();
                        }
                    })
                            .setNegativeListener(getActivity().getResources().getString(R.string.no), new ColorDialog.OnNegativeListener() {
                                @Override
                                public void onClick(ColorDialog colorDialog) {
                                    colorDialog.dismiss();
                                }
                            }).show();
                } else {
                    /**
                     * go to membership plan
                     */
                    ((NavigationActivity) getActivity()).gotoMembershipPlanFragment(6);
                }
            }
        });

        mView.findViewById(R.id.llMoreDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnSubscribe.getText().toString().equalsIgnoreCase(getActivity().getResources().getString(R.string.menu_subscribe_plan))) {
                    ColorDialog dialog = new ColorDialog(getActivity());
                    dialog.setColor("#b51800");
                    dialog.setCancelable(false);
                    dialog.setAnimationEnable(true);
                    dialog.setTitle(getActivity().getResources().getString(R.string.app_name));
                    dialog.setContentText(getActivity().getResources().getString(R.string.message_no_subscribe));
                    dialog.setPositiveListener(getActivity().getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
                        @Override
                        public void onClick(ColorDialog dialog) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    /**
                     * go to membership plan
                     */
                    ((NavigationActivity) getActivity()).gotoMembershipPlanFragment(6);
                }

            }
        });

        mvpPresenter.getProfileDetails();

        return mView;

    }


    private void init() {

        ivChangeImage = mView.findViewById(R.id.ivChangeImage);
        tvProfileName = mView.findViewById(R.id.tvProfileName);
        tvAboutMe = mView.findViewById(R.id.tvAboutMe);
        tvOccupation = mView.findViewById(R.id.tvOccupation);
        tvCarNumber = mView.findViewById(R.id.tvCarNumber);
        btnSubscribe = mView.findViewById(R.id.btnSubscribe);
        ivProfileImage = mView.findViewById(R.id.ivProfileImage);
        rvPhotos = mView.findViewById(R.id.rvPhotos);
        rvPhotos.setLayoutManager(new LinearLayoutManager(getAppContext(), LinearLayoutManager.HORIZONTAL, false));


        /**
         * Clearing older images from cache directory
         * don't call this line if you want to choose multiple images in the same activity
         *  call this once the bitmap(s) usage is over
         */
        ImagePickerActivity.clearCache(getAppContext());


        /**
         *Instagram initializer
         */
        mInstagramHelper = new InstagramHelper(BuildConfig.INSTAGRAM_CLIENT_ID, BuildConfig.INSTAGRAM_SECRET_ID, BuildConfig.CALL_BACK_URL, getAppContext(), this);

    }

    @Override
    protected ProfilePresenter createPresenter() {
        return new ProfilePresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void onUnsubscribePlan(String message) {
        ColorDialog dialog = new ColorDialog(getActivity());
        dialog.setColor("#b51800");
        dialog.setCancelable(false);
        dialog.setAnimationEnable(true);
        dialog.setTitle(getActivity().getResources().getString(R.string.app_name));
        dialog.setContentText(message);
        dialog.setPositiveListener(getActivity().getResources().getString(R.string.ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();

                mvpPresenter.getProfileDetails();
            }
        }).show();
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onProfileDataSuccess(JSONObject data) {
        imageInputStream = null;
        try {

            /**
             * set name
             */

            tvProfileName.setText(data.getString("name"));

            /**
             * DOB
             */
            String dob = data.getString("date_of_birth");
            Utility.setSharedPreference(getAppContext(), Constant.UPDATE_DOB, dob);


            /**
             * interest
             */
            JSONArray interestInJsonArray = data.getJSONArray("interestIn");
            interestIdList = new ArrayList<>();
            interestName = "";
            for (int i = 0; i < interestInJsonArray.length(); i++) {
                JSONObject object = interestInJsonArray.getJSONObject(i);
                interestIdList.add(object.getString("id"));
                interestName = interestName + object.getString("interest") + ", ";
            }
            if (!interestName.equalsIgnoreCase("")) {
                interestName = interestName.substring(0, (interestName.length() - 1));
            }

            ((TextViewRegular) mView.findViewById(R.id.tvInterestIn)).setText(interestName);
            String car_number = data.getString("car_number");
            if (car_number.equalsIgnoreCase("")) {
                tvCarNumber.setText(getResources().getString(R.string.register_car_no));
            } else {
                tvCarNumber.setText(data.getString("car_number"));
            }


            /**
             * set and, city, country
             */
            try
            {
                cityName = data.getJSONObject("city").getString("name");
                countryName = data.getJSONObject("country").getString("name");
                Utility.setSharedPreference(getAppContext(), Constant.UPDATE_CITY_ID, data.getJSONObject("city").getString("id"));
                Utility.setSharedPreference(getAppContext(), Constant.UPDATE_COUNTRY_ID, data.getJSONObject("country").getString("id"));

            }catch (Throwable e)
            {
            }

            StringBuilder ageStringBuilder = new StringBuilder();
            ageStringBuilder.append(data.getString("age"));
            ageStringBuilder.append(", ");
            ageStringBuilder.append(cityName);
            ageStringBuilder.append(", ");
            ageStringBuilder.append(countryName);
            ((TextViewRegular) mView.findViewById(R.id.tvAgeCity)).setText(ageStringBuilder.toString());

            /**
             * About me
             */
            tvAboutMe.setText(data.getString("about_me"));
            tvOccupation.setText(data.getString("occupation"));

            /**
             * plan subscribe
             */
            if (data.getString("is_plan_subscribed").equalsIgnoreCase("yes")) {
                btnSubscribe.setText(getActivity().getResources().getString(R.string.menu_un_subscribe_plan));
                btnSubscribe.setTextColor(getActivity().getResources().getColor(R.color.unSubscribePlanColor));
            } else {
                btnSubscribe.setText(getActivity().getResources().getString(R.string.menu_subscribe_plan));
                btnSubscribe.setTextColor(getActivity().getResources().getColor(R.color.subscribePlanColor));
            }

//            /**
//             * interest in
//             */
//            StringBuilder mStringBuilder = new StringBuilder();
//            JSONArray interestInJsonArray = data.getJSONArray("interestIn");
//            for (int i = 0; i < interestInJsonArray.length(); i++) {
//                JSONObject object = interestInJsonArray.getJSONObject(i);
//                mStringBuilder.append(object.getString("interest"));
//                mStringBuilder.append(", ");
//
//            }

            /**
             * profile image
             */
            final String imageUrl = data.getString("image");
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(getAppContext()).load(imageUrl).into(ivProfileImage);
                Utility.setSharedPreference(getAppContext(), Constant.PROFILE_IMAGE, imageUrl);
                ((NavigationActivity) getActivity()).updateProfileInfo();
                try
                {
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("imageUrl", imageUrl);
                    reference.updateChildren(hashMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bigImageView(getProfileName(), imageUrl);
                }
            });

            /**
             * get all photos
             */
            ArrayList<PhotoModel> arrayList = new ArrayList<>();
            JSONArray postJsonArray = data.getJSONArray("post");
            for (int i = 0; i < postJsonArray.length(); i++) {
                JSONObject photoObject = postJsonArray.getJSONObject(i);
                arrayList.add(new PhotoModel(
                        photoObject.getString("id"),
                        photoObject.getString("user_id"),
//                        photoObject.getString("title"),
//                        photoObject.getString("content"),
                        photoObject.getString("post_image"),
                        photoObject.getString("status")
                ));
            }
            PhotoAdapter adapter = new PhotoAdapter(arrayList);
            rvPhotos.smoothScrollToPosition(adapter.getItemCount());
            rvPhotos.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getAboutMe() {
        return tvAboutMe.getText().toString().trim();
    }

    @Override
    public String getProfileName() {
        return tvProfileName.getText().toString().trim();
    }

    @Override
    public void setProfileName(String name) {
        Utility.setSharedPreference(getAppContext(), Constant.NAME, name);
        tvProfileName.setText(name);
    }

    @Override
    public String getOccupation() {
        return tvOccupation.getText().toString().trim();
    }

    @Override
    public ArrayList<String> getInterestIs() {
        return interestIdList;
    }

    @Override
    public String getCompanyName() {
        return "Not clear till now";
    }

    @Override
    public String getCarNumber() {
        return tvCarNumber.getText().toString();
    }

    @Override
    public String getCountryName() {
        return countryName;
    }

    @Override
    public String getCityName() {
        return cityName;
    }

    @Override
    public void updateInterested(ArrayList<InterestModel> arrayList) {
        interestIdList = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            InterestModel model = arrayList.get(i);
            if (model.isSelected()) {
                interestIdList.add(model.getId());
            }
        }

    }

    @Override
    public void onInstagramImageLoaded(ArrayList<ImageUrl> arrayList) {
        Intent intent = new Intent(getAppContext(), ImageSelectorActivity.class);
        intent.putExtra("ImageUrl", arrayList);
        startActivityForResult(intent, Constant.INSTA_IMAGE_REQUEST);


    }

    public void bigImageView(String title, String imageUrl) {
        Intent intent = new Intent(getAppContext(), DocumentViewerActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("imageUrl", imageUrl);
        startActivity(intent);

    }

    private void showImagePickerOptions(final String type) {
        ImagePickerActivity.showImagePickerOptions(getAppContext(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent(type);
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent(type);
            }
        });
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getAppContext());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getAppContext().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void launchCameraIntent(String type) {
        this.UPLOAD_TYPE = type;
        Intent intent = new Intent(getAppContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);
        startActivityForResult(intent, Constant.IMAGE_REQUEST);
    }

    private void launchGalleryIntent(String type) {
        this.UPLOAD_TYPE = type;
        Intent intent = new Intent(getAppContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, Constant.IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.view("Activitycall",data+"  "+resultCode+"   "+requestCode);
        switch (requestCode)
        {
            case Constant.IMAGE_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getParcelableExtra("path");
                    try {
                        imageInputStream = getAppContext().getContentResolver().openInputStream(uri);
                        byte[] imageBytes = Utility.getBytes(imageInputStream);
                        mvpPresenter.uploadImage(UPLOAD_TYPE, imageBytes);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getAppContext(), getResources().getString(R.string.request_cancel), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constant.INSTA_IMAGE_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    final String image_url = data.getStringExtra("image_url");
                    new AsyncTask<Void, Void, byte[]>() {

                        @Override
                        protected byte[] doInBackground(Void... voids) {
                            byte[] imageBytes = new byte[0];
                            try {
                                URL url = new URL(image_url);
                                URLConnection connection = url.openConnection();
                                connection.connect();
                                imageInputStream = connection.getInputStream();
                                imageBytes = Utility.getBytes(imageInputStream);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return imageBytes;
                        }

                        @Override
                        protected void onPostExecute(byte[] aVoid) {
                            super.onPostExecute(aVoid);


                            mvpPresenter.uploadImage("post", aVoid);
                        }
                    }.execute();

                } else {
                    Toast.makeText(getAppContext(), getResources().getString(R.string.request_cancel), Toast.LENGTH_SHORT).show();
                }

                break;


            default: {
                break;
            }
        }

    }

    @Override
    public void onInstagramSignInSuccess(InstagramUser user) {
        mvpPresenter.getInstargramImages(user.getAccesstoken());
        Log.view(TAG, "Instagram user data: full name name=" + user.getFull_name() + " user name=" + user.getUsername());
    }

    @Override
    public void onInstagramSignInFail(String error) {
        Log.view(TAG, getActivity().getResources().getString(R.string.instagram_signin_failed));
        onToast(getActivity().getResources().getString(R.string.instagram_signin_failed));
    }

}