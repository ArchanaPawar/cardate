package com.tekzee.cardate.ui.carnumber;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface CarNumberView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onStartNavigationActivity();
    void onStartLoginActivity();

    String getCarNumber();

}
