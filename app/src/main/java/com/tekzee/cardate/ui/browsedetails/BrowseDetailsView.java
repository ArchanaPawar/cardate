package com.tekzee.cardate.ui.browsedetails;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import org.json.JSONObject;

public interface BrowseDetailsView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onProfileDataSuccess(JSONObject jsonObject);

    void showDialog(String message);

    void onUserBlocked(String message);

    void userNotBlocked();

//    String getUserMessage();
}
