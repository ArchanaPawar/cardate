package com.tekzee.cardate.ui.searchbycar;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchByCarPresenter extends BasePresenter<SearchByCarView> {
    private static final String TAG = SearchByCarPresenter.class.getSimpleName();

    public SearchByCarPresenter(SearchByCarView view, Context mContext) {

        attachView(view,mContext);
    }

    public void searchCar(final String search_key) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("car_no", search_key);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.searchByCar(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<SearchByCarModel> arrayList = new ArrayList<>();
                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject object = mJsonArray.getJSONObject(i);

                                arrayList.add(new SearchByCarModel(
                                        object.getString("id"),
                                        object.getString("name"),
                                        object.getString("email"),
                                        object.getString("date_of_birth"),
                                        object.getString("occupation"),
                                        object.getString("about_me"),
                                        object.getString("gender"),
                                        object.getString("image"),
                                        object.getString("is_online"),
                                        object.getString("interest"),
                                        object.getString("car_number"),
                                        object.getString("age")

                                ));

                            }

                            mvpView.onSearchResult(search_key,arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.onSearchFail(search_key+" "+jsonObject.getString("message"));
//                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }
                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }


    }
}
