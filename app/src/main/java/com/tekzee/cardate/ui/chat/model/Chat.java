package com.tekzee.cardate.ui.chat.model;

public class Chat {
    private String sender;
    private String receiver;
    private String message;
    private String url;
    private String viewType;
    private boolean isseen;

    public Chat(String sender, String receiver, String message, String url, String viewType, boolean isseen) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.url = url;
        this.viewType = viewType;
        this.isseen = isseen;
    }

    public Chat() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsseen() {
        return isseen;
    }

    public void setIsseen(boolean isseen) {
        this.isseen = isseen;
    }
}
