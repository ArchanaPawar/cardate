package com.tekzee.cardate.ui.settings;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import org.json.JSONObject;

public interface SettingsView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onLogoutSuccess(String message);

    void onCancelAccountSuccess(String message);

    void onStartContactUsActivity();

    void onStartBlockUserActivity();

    void onStartChangePasswordActivity();

    void onEmailStatusChange();

    void onWebPageSuccess(String webUrl,String pageType);
}
