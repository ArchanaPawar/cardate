package com.tekzee.cardate.ui.carnumber;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.login.LoginActivity;
import com.tekzee.cardate.ui.navigation.NavigationActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

import cn.refactor.lib.colordialog.PromptDialog;

public class CarNumberActivity extends MvpActivity<CarNumberPresenter> implements CarNumberView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_number);
        hideSoftKeyboard();

        findViewById(R.id.toolbar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartLoginActivity();
            }
        });

        findViewById(R.id.btn_skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN,true);
                updateToken();
                onStartNavigationActivity();

            }
        });

        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCarNumber().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_car_number));
                } else {
                    updateToken();
                    mvpPresenter.updateCarNumber();


                }
            }
        });
    }

    @Override
    protected CarNumberPresenter createPresenter() {
        return new CarNumberPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message, PromptDialog.DIALOG_TYPE_WARNING);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.activity_car_number, message);
    }

    @Override
    public void onStartNavigationActivity() {
        startActivity(new Intent(getAppContext(), NavigationActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onStartLoginActivity() {
        startActivity(new Intent(getAppContext(), LoginActivity.class));
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        finish();
    }

    @Override
    public String getCarNumber() {
        return ((EditTextRegular) findViewById(R.id.et_car_no)).getText().toString().trim();
    }


    private void updateToken() {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("userId", Utility.getSharedPreferences(getAppContext(),Constant.USER_ID));
            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
