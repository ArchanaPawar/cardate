package com.tekzee.cardate.ui.contactus;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

import cn.refactor.lib.colordialog.PromptDialog;

public class ContactUsActivity extends MvpActivity<ContactUsPresenter> implements ContactUsView {
    private EditTextRegular etName;
    private EditTextRegular etEmail;
    private EditTextRegular etDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                String description = etDescription.getText().toString().trim();

                if (name.equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_name));

                } else if (email.equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_email));

                } else if (!Utility.emailValidator(email)) {
                    showInSnackBar(getResources().getString(R.string.alert_invalid_email));

                } else if (description.equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_description));

                } else if (description.length() < 20) {
                    showInSnackBar(getResources().getString(R.string.alert_description_length));

                } else {


                    mvpPresenter.submitContactUsForm(name, email, description);


                }
            }
        });

    }

    private void init() {
        etName = findViewById(R.id.etName);
        etName.setText(Utility.getSharedPreferences(getAppContext(), Constant.NAME));


        etEmail = findViewById(R.id.etEmail);
        etEmail.setText(Utility.getSharedPreferences(getAppContext(), Constant.EMAIL));

        etDescription = findViewById(R.id.etDescription);
    }

    @Override
    protected ContactUsPresenter createPresenter() {
        return new ContactUsPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                }).show();
    }

    @Override
    public void showInSnackBar(String message) {

        snackBarTop(R.id.contact_us_activity, message);

    }
}
