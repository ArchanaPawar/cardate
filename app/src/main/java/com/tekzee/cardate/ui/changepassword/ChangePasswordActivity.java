package com.tekzee.cardate.ui.changepassword;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.login.LoginActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

import cn.refactor.lib.colordialog.PromptDialog;

public class ChangePasswordActivity extends MvpActivity<ChangePasswordPresenter> implements ChangePasswordView {
    private EditTextRegular etCurrentPassword;
    private EditTextRegular etNewPassword;
    private EditTextRegular etConformPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.ivReset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCurrentPassword.setText("");
                etNewPassword.setText("");
                etConformPassword.setText("");
            }
        });

        findViewById(R.id.btnChangePassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cPassword = etCurrentPassword.getText().toString().trim();
                String newPassword = etNewPassword.getText().toString().trim();
                String cNewPassword = etConformPassword.getText().toString().trim();

                if (cPassword.equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_current_password));

                } else if (cPassword.length() < 6) {
                    showInSnackBar(getResources().getString(R.string.alert_password_length));

                } else if (newPassword.equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_new_password));

                } else if (newPassword.length() < 6) {
                    showInSnackBar(getResources().getString(R.string.alert_password_length));

                } else if (!cNewPassword.equalsIgnoreCase(newPassword)) {
                    showInSnackBar(getResources().getString(R.string.alert_password_same));

                } else {


                    mvpPresenter.changePassword(cPassword, newPassword);

                }

            }
        });
    }

    private void init() {
        etCurrentPassword = findViewById(R.id.etCurrentPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConformPassword = findViewById(R.id.etConformPassword);

    }

    @Override
    protected ChangePasswordPresenter createPresenter() {
        return new ChangePasswordPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                }).show();
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.change_password_activity, message);

    }

    @Override
    public void onchangePasswordSuccess(String message) {
        deleteFirebaseToken();
        final PromptDialog dialog = new PromptDialog(getAppContext());
        dialog.setCancelable(false);
        dialog.setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPositiveListener(getResources().getString(R.string.ok), new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog1) {
                        dialog.dismiss();
                        String languageCode = Utility.getSharedPreferences(getAppContext(),Constant.LANGUAGE_CODE);
                        Utility.clearSharedPreference(getAppContext());
                        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN, false);
                        Utility.setSharedPreference(getAppContext(), Constant.LANGUAGE_CODE, languageCode);
                        Intent intent = new Intent(mActivity, LoginActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getAppContext().overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
                        getAppContext().finishAffinity();
                    }
                }).show();


    }


    private void deleteFirebaseToken() {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("status", "offline");
            hashMap.put("token", "logout");

            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
