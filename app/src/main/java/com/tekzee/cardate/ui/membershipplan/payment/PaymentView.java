//package com.tekzee.cardate.ui.membershipplan.payment;
//
//import android.app.Activity;
//
//import com.tekzee.cardate.ui.base.BaseView;
//
//import org.json.JSONObject;
//
//public interface PaymentView extends BaseView {
//
//    Activity getAppContext();
//
//    void showInPopup(String message);
//
//    void showInSnackBar(String message);
//
//    void onPaymentSuccess(JSONObject mJsonObject);
//
//    void onOurLocalServerSuccess(String message);
//
//    void onPaymentFail(String reason);
//
//    void onPaymentReceived(JSONObject data);
//}