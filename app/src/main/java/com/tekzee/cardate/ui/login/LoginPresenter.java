package com.tekzee.cardate.ui.login;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class LoginPresenter extends BasePresenter<LoginView> {
    private static final String TAG = LoginPresenter.class.getSimpleName();

    public LoginPresenter(LoginView view, Context mContext) {

        attachView(view,mContext);
    }

    public void loginUser(String fcmToken,String firebaseId) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("email", mvpView.getEmail());
                input.put("password", mvpView.getPassword());
                input.put("device_type", "ANDROID");
                input.put("device_token", fcmToken.equalsIgnoreCase("") ? Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FCM_TOKEN) : fcmToken);
                input.put("firebase_id", firebaseId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.signIn(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            /**
                             * get user info
                             */
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.USER_ID, dataObject.getString("id"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.NAME, dataObject.getString("name"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.EMAIL, dataObject.getString("email"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.DOB, dataObject.getString("date_of_birth"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.GENDER, dataObject.getString("gender"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.OCCUPATION, dataObject.getString("occupation"));
                            String carNumber = dataObject.getString("car_number");
                            if(carNumber == null || carNumber.equalsIgnoreCase("null")){
                                carNumber = "";
                            }
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.CAR_NUMBER,carNumber );
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.PROFILE_IMAGE, dataObject.getString("image"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.ABOUT_ME, dataObject.getString("about_me"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.AUTH_TOKEN, dataObject.getString("auth_token"));
                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.AGE, dataObject.getString("age"));
                            Utility.setSharedPreferenceBoolean(mvpView.getAppContext(), Constant.IS_PREMIUM_USER, dataObject.getString("is_premium_user").equalsIgnoreCase("1"));

                            /**
                             * get country
                             */
//                            JSONObject countryObject = dataObject.getJSONObject("country");
//                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.COUNTRY_ID, countryObject.getString("id"));
//                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.COUNTRY_NAME, countryObject.getString("name"));

                            /**
                             * get country
                             */
//                            JSONObject cityObject = dataObject.getJSONObject("city");
//                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.CITY_ID, cityObject.getString("id"));
//                            Utility.setSharedPreference(mvpView.getAppContext(), Constant.CITY_NAME, cityObject.getString("name"));

                            mvpView.onStartCarNumberActivity();

                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
