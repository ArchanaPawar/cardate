package com.tekzee.cardate.ui.matchmatching.matches;

public class MatchModel {
    private String id;
    private String name;
    private String email;
    private String dateIOfBirth;
    private String age;
    private String city;
    private String cityId;
    private String country;
    private String countryId;
    private String image;

    public MatchModel(String id, String name, String email, String dateIOfBirth, String age, String city, String cityId, String country, String countryId, String image) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dateIOfBirth = dateIOfBirth;
        this.age = age;
        this.city = city;
        this.cityId = cityId;
        this.country = country;
        this.countryId = countryId;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getDateIOfBirth() {
        return dateIOfBirth;
    }

    public String getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getImage() {
        return image;
    }
}