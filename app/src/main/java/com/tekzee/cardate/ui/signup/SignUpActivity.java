package com.tekzee.cardate.ui.signup;
import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.EditTextRegular;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpActivity;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.login.LoginActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

public class SignUpActivity extends MvpActivity<SignUpPresenter> implements SignUpView {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private EditText tv_dob;
//    private TextViewRegular tv_country;
//    private TextViewRegular tv_city;
    private EditTextRegular et_name;
    private EditTextRegular et_email;
    private TextViewRegular txt_login;
    private RadioGroup rg_gender;
//    private String countryId = "";
//    private String cityId = "";
    private String imageUrl = "";
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabaseReference;
    private int characterCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        hideSoftKeyboard();
        init();

        findViewById(R.id.toolbar_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        findViewById(R.id.btn_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFullName().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_name));

                } else if (getEmail().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_email));

                } else if (!Utility.emailValidator(getEmail())) {
                    showInSnackBar(getResources().getString(R.string.alert_invalid_email));

                } else if (getGender().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.select_gender));

                } else if (getDOB().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.select_dob));

                } else if (!Utility.dateValidator(getDOB())) {
                    showInSnackBar(getResources().getString(R.string.invalid_dob));

//                } else if (getCountry().equalsIgnoreCase("")) {
//                    showInSnackBar(getResources().getString(R.string.select_country));
//
//                } else if (getCity().equalsIgnoreCase("")) {
//                    showInSnackBar(getResources().getString(R.string.alert_select_city));

                } else if (getPassword().equalsIgnoreCase("")) {
                    showInSnackBar(getResources().getString(R.string.alert_password));

                } else if (getPassword().length() < 6) {
                    showInSnackBar(getResources().getString(R.string.alert_password_length));

                } else if (!getPassword().equalsIgnoreCase(getConformPassword())) {
                    showInSnackBar(getResources().getString(R.string.alert_password_same));

                } else {
                    showProgressDialog(false);
                    getFCMToken();

                }
            }
        });


//        tv_dob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Get Current Date
//                final Calendar c = Calendar.getInstance();
//                int mYear = c.get(Calendar.YEAR);
//                int mMonth = c.get(Calendar.MONTH);
//                int mDay = c.get(Calendar.DAY_OF_MONTH);
//
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(getAppContext(),
//                        new DatePickerDialog.OnDateSetListener() {
//
//                            @Override
//                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
//                                tv_dob.setText(Utility.getDoubleDigit(dayOfMonth) + "-" + Utility.getDoubleDigit((monthOfYear + 1)) + "-" + year);
//
//                            }
//                        }, mYear, mMonth, mDay);
//
//                /**
//                 * set min day in calendar
//                 */
////                Calendar minDate = Calendar.getInstance();
////                minDate.set(Calendar.DAY_OF_MONTH, mDay);
////                minDate.set(Calendar.MONTH, mMonth);
////                minDate.set(Calendar.YEAR, mYear);
////                datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
//
//                /**
//                 * set max day in calendar
//                 */
//                Calendar maxDate = Calendar.getInstance();
//                maxDate.set(Calendar.DAY_OF_MONTH, mDay);
//                maxDate.set(Calendar.MONTH, (mMonth));
//                maxDate.set(Calendar.YEAR, mYear);
//                datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
//
//                datePickerDialog.show();
//            }
//        });


        tv_dob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable view) {
                if (characterCount <= tv_dob.getText().toString().length()
                        && (tv_dob.getText().toString().length() == 2
                        || tv_dob.getText().toString().length() == 5)) {
                    tv_dob.setText(tv_dob.getText().toString() + "-");
                    int pos = tv_dob.getText().length();
                    tv_dob.setSelection(pos);
                } else if (characterCount >= tv_dob.getText().toString().length()
                        && (tv_dob.getText().toString().length() == 2
                        || tv_dob.getText().toString().length() == 5)) {
                    tv_dob.setText(tv_dob.getText().toString().substring(0, tv_dob.getText().toString().length() - 1));
                    int pos = tv_dob.getText().length();
                    tv_dob.setSelection(pos);
                }
                characterCount = tv_dob.getText().toString().length();
            }

        });


//        tv_country.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mvpPresenter.getAllCountry();
//            }
//        });
//
//        tv_city.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (countryId.equalsIgnoreCase("")) {
//
//                    showInSnackBar(getAppContext().getResources().getString(R.string.alert_country_select));
//
//                } else {
//                    mvpPresenter.getAllCity(countryId);
//                }
//
//
//            }
//        });


        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onStartLoginActivity();

            }
        });


    }

    private void init() {
        txt_login = findViewById(R.id.txt_login);
        txt_login.setText(Html.fromHtml(getResources().getString(R.string.hint_already_account)));

        tv_dob = findViewById(R.id.tv_dob);
//        tv_country = findViewById(R.id.tv_country);
//        tv_city = findViewById(R.id.tv_city);
        rg_gender = findViewById(R.id.rg_gender);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);

        if (Utility.getSharedPreferencesBoolean(getAppContext(), Constant.IS_SOCIAL)) {
            et_name.setText(getIntent().getExtras().getString("name"));
            et_email.setText(getIntent().getExtras().getString("email"));
            setImageUrl(getIntent().getExtras().getString("imageUrl"));

            /**
             * set gender
             */
            String gender = getIntent().getExtras().getString("gender");
            if (gender != null && !gender.equalsIgnoreCase("")) {
                if (gender.equalsIgnoreCase("Male")) {
                    rg_gender.check(R.id.rb_male);
                } else {
                    rg_gender.check(R.id.rb_female);

                }


            }

        } else {
            setImageUrl("");
        }


        /**
         * init Firebase Auth
         */
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected SignUpPresenter createPresenter() {
        return new SignUpPresenter(this, getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);
    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.activity_sign_up, message);
    }

    @Override
    public void onStartLoginActivity() {
        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_SOCIAL, false);
        startActivity(new Intent(getAppContext(), LoginActivity.class));
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
        finish();

    }

    @Override
    public String getFullName() {
        return et_name.getText().toString().trim();
    }

    @Override
    public String getEmail() {
        return et_email.getText().toString().trim();
    }


    @Override
    public String getGender() {
        String genderId = "";
        try {
            int selectedId = rg_gender.getCheckedRadioButtonId();
            RadioButton radioButton = findViewById(selectedId);
            String gender = radioButton.getText().toString();
            if (gender.equalsIgnoreCase(getResources().getString(R.string.male))) {

                genderId = "1";

            } else {

                genderId = "2";

            }
        } catch (Exception e) {

        }


        return genderId;
    }

    @Override
    public String getDOB() {
        return tv_dob.getText().toString();
    }

//    @Override
//    public String getCity() {
//        return cityId;
//    }
//
//    @Override
//    public String getCountry() {
//        return countryId;
//    }

    @Override
    public String getPassword() {
        return ((EditTextRegular) findViewById(R.id.et_password)).getText().toString().trim();
    }

    @Override
    public String getConformPassword() {
        return ((EditTextRegular) findViewById(R.id.et_con_password)).getText().toString().trim();
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;

    }

//    @Override
//    public void onCountrySelected(CountryModel model, String type) {
//        if (type.equalsIgnoreCase(getResources().getString(R.string.country))) {
//            tv_country.setText(model.getName());
//            countryId = model.getId();
//
//            /**
//             * reset city
//             */
//            tv_city.setText("");
//            cityId = "";
//
//        } else {
//            tv_city.setText(model.getName());
//            cityId = model.getId();
//        }
//
//
//    }

    @Override
    public void onGetPasswordSuccess(String password, String fcmToken) {
        /**
         * login in firebase and get user-id
         */
        mFirebaseAuth.signInWithEmailAndPassword(getEmail(), password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            continueWithRegistration(fcmToken);
                        }else{

                            task.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.view("signInWithEmailAndPassword occured",e.getMessage());
                                }
                            });

                        }
                    }



                });


    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            onToast(task.getException().getLocalizedMessage());
                            Log.view(TAG, "getInstanceId failed : " + task.getException());
                            return;
                        }

                        /**
                         *Get new Instance ID token
                         */
                        String token = task.getResult().getToken();
                        registerUserInFireBase(token);


                    }
                });

    }

    private void registerUserInFireBase(final String token) {
        mFirebaseAuth.createUserWithEmailAndPassword(getEmail(), Constant.FIREBASE_PASSWORD)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            continueWithRegistration(token);

                        } else {

                            dismissProgressDialog();
                            task.addOnFailureListener(getAppContext(), new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.view("registerUserInFireBase occured",e.getMessage());
                                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                        /**
                                         * User with this email already exist.
                                         */
                                        try {

                                            mvpPresenter.getPassword(token);

                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }


                                    } else {
                                        e.printStackTrace();
                                        Log.view(TAG, "Error : " + e.getLocalizedMessage());
                                        showInPopup(e.getLocalizedMessage());
                                    }

                                }
                            });
                        }

                    }
                });


    }


    private void continueWithRegistration(final String token) {
        FirebaseUser firebaseUser = mFirebaseAuth.getCurrentUser();
        String userId = firebaseUser.getUid();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(userId);
        HashMap<String, String> mHashMap = new HashMap<>();
        mHashMap.put("id", userId);
        mHashMap.put("name", getFullName());
        mHashMap.put("email", getEmail());
        mHashMap.put("imageUrl", (getImageUrl().equalsIgnoreCase("") ? "default" : getImageUrl()));
        mHashMap.put("status", "offline");
        mHashMap.put("token", token);
        mHashMap.put("userId", "userId");
        mHashMap.put("search", getFullName().toLowerCase());

        mDatabaseReference.setValue(mHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {

                    mvpPresenter.signUpNewUser(token, "APP");

                } else {

                    dismissProgressDialog();
                    showInPopup(getResources().getString(R.string.error_firebase_db_refrences));
                    task.addOnFailureListener(getAppContext(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                            Log.view(TAG, "Error : " + e.getLocalizedMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (Utility.getSharedPreferencesBoolean(getAppContext(), Constant.IS_SOCIAL)) {
            startActivity(new Intent(getAppContext(), IntroductionActivity.class));
            overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
            finish();
        } else {
            onStartLoginActivity();
        }

        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_SOCIAL, false);
    }
}
