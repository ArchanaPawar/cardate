package com.tekzee.cardate.ui.login;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;


public interface LoginView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onStartCarNumberActivity();

    void onStartIntroductionActivity();

    void onStartSignUpActivity();

    void onStartForgotPasswordActivity();

    String getEmail();

    String getPassword();

}
