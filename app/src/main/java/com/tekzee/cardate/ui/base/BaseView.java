package com.tekzee.cardate.ui.base;

import android.app.ProgressDialog;
import android.widget.EditText;

import com.tekzee.cardate.utils.Constant;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public interface BaseView extends Constant {
    ProgressDialog showProgressDialog(boolean cancelable);

    ProgressDialog showProgressDialog(CharSequence message, boolean cancelable);

    void dismissProgressDialog();

    void snackBarBottom(int view_id, String message);

    void snackBarTop(int view_id, String message);

    void hideSoftKeyboard();

    boolean isNetworkConnected();

    void showSoftKeyboard(EditText view);

    void onToast(String message);

    void restartApp();

//    String getFileStorageLocation(int fileType,boolean isSent);
}
