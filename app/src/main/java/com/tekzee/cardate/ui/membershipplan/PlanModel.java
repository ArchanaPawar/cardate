package com.tekzee.cardate.ui.membershipplan;

import java.io.Serializable;
import java.util.ArrayList;

public class PlanModel implements Serializable {
    private String id;
    private String name;
    private String duration;
    private String amount;
    private String currency;
    private String feature;
    private ArrayList<ContentModel> contentModelArrayList;

    public PlanModel(String id, String name, String duration, String amount, String currency, String feature, ArrayList<ContentModel> contentModelArrayList) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.amount = amount;
        this.currency = currency;
        this.feature = feature;
        this.contentModelArrayList = contentModelArrayList;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDuration() {
        return duration;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFeature() {
        return feature;
    }

    public ArrayList<ContentModel> getContentModelArrayList() {
        return contentModelArrayList;
    }
}
