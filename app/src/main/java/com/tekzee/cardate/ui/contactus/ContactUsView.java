package com.tekzee.cardate.ui.contactus;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface ContactUsView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);
}
