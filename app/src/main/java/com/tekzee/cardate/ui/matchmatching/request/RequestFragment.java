package com.tekzee.cardate.ui.matchmatching.request;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daprlabs.aaron.swipedeck.SwipeDeck;
import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;

import java.util.ArrayList;

public class RequestFragment extends MvpFragment<RequestPresenter> implements RequestView {
    private static final String TAG = RequestFragment.class.getSimpleName();
    private SwipeDeck mSwipeDeck;
    private SwipeDeckAdapter mSwipeDeckAdapter;
    private ArrayList<RequestModel> requestModelArrayList;
    private boolean isSuperLike;

    public static RequestFragment newInstance() {
        return new RequestFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_request, container, false);
        init(mView);

        mSwipeDeck.setCallback(new SwipeDeck.SwipeDeckCallback() {
            @Override
            public void cardSwipedLeft(long stableId) {
                isSuperLike = false;
                Log.view(TAG, "Card was swiped left, position in adapter: " + stableId);
            }

            @Override
            public void cardSwipedRight(long stableId) {
                /**
                 * marked as like
                 */
                RequestModel model = requestModelArrayList.get((int) stableId);
                if (isSuperLike) {

                    mvpPresenter.doLike(model.getId(), "0", "1");
                    isSuperLike = false;

                } else {
                    mvpPresenter.doLike(model.getId(), "1", "0");
                }

            }
        });

        mvpPresenter.getAllUser("request");

        return mView;

    }

    private void init(View mView) {

        mSwipeDeck = mView.findViewById(R.id.swipe_deck);

    }

    @Override
    protected RequestPresenter createPresenter() {
        return new RequestPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onUserFound(ArrayList<RequestModel> arrayList) {
        requestModelArrayList = new ArrayList<>();
        requestModelArrayList.addAll(arrayList);

        mSwipeDeckAdapter = new SwipeDeckAdapter(requestModelArrayList, new SwipeDeckAdapter.ButtonClick() {
            @Override
            public void onCancel(RequestModel model) {
                isSuperLike = false;
                mSwipeDeck.swipeTopCardLeft(500);
            }

            @Override
            public void onLike(RequestModel model) {
                isSuperLike = false;
                mSwipeDeck.swipeTopCardRight(500);
            }

            @Override
            public void onSuperLike(RequestModel model) {
                isSuperLike = true;
                mSwipeDeck.swipeTopCardRight(500);
            }
        });

        if (mSwipeDeck != null) {
            mSwipeDeck.setAdapter(mSwipeDeckAdapter);
        }

        mSwipeDeck.setLeftImage(R.id.left_image);
        mSwipeDeck.setRightImage(R.id.right_image);

    }
}
