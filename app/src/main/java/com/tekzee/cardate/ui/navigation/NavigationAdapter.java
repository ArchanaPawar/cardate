package com.tekzee.cardate.ui.navigation;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.CircleImageView;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.custom.TextViewSemiBold;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hasnain on 1-May-19.
 */

public class NavigationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<NavigationData> navigationDatas;
    private INavigation listener;
    private Context mContext;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private int last_position = 0;

    interface INavigation {
        void onItemClick(int position);

        void onProfileClick();

        void onSettingsClick();
    }

    public NavigationAdapter(INavigation listener) {
        navigationDatas = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.row_navigation, parent, false);
            return new ViewHolderItem(itemView);

        } else if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.navigation_header, parent, false);
            return new ViewHolderHeader(itemView);

        } else {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderItem) {
            position = position - 1;
            ((ViewHolderItem) holder).bind(navigationDatas.get(position), position);

        } else if (holder instanceof ViewHolderHeader) {
            ((ViewHolderHeader) holder).bind();
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return (navigationDatas.size()) + 1;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        private TextViewRegular tvNavigationName;
        private TextViewSemiBold tvCount;
        private ImageView ivNavigation;
        private RelativeLayout rl_row;

        public ViewHolderItem(View itemView) {
            super(itemView);
            tvNavigationName = itemView.findViewById(R.id.tvNavigationName);
            tvCount = itemView.findViewById(R.id.tvCount);
            ivNavigation = itemView.findViewById(R.id.ivNavigation);
            rl_row = itemView.findViewById(R.id.rl_row);
        }

        private void bind(NavigationData navigationData, final int position) {
            tvNavigationName.setText(navigationData.getName());
            int count = Integer.parseInt((navigationData.getCount().equalsIgnoreCase("") ? "0" : navigationData.getCount()));
            if (count > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText("" + count + "");
            }else{
                tvCount.setText("");
                tvCount.setVisibility(View.GONE);
            }
            ivNavigation.setImageResource(navigationData.getDrawableId());
            ivNavigation.setTag(position);

            rl_row.setBackgroundResource(navigationData.isSelected() ? R.drawable.round_right_top_bottom : android.R.color.transparent);
            tvNavigationName.setTextColor(navigationData.isSelected() ? mContext.getResources().getColor(R.color.navigation_text_select_color) : mContext.getResources().getColor(R.color.navigation_text_default_color));
            ivNavigation.setColorFilter(navigationData.isSelected() ? mContext.getResources().getColor(R.color.navigation_text_select_color) : mContext.getResources().getColor(R.color.navigation_text_default_color));

//            ivNavigation.setColorFilter(mContext.getResources().getColor(android.R.color.black), PorterDuff.Mode.SRC_IN);

            tvNavigationName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(position);
                }
            });

            ivNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(position);
                }
            });

        }
    }


    public void refreshAdapter(ArrayList<NavigationData> data) {
        navigationDatas.clear();
        navigationDatas.addAll(data);
        notifyDataSetChanged();
    }


    public void setSelected(int position) {
        for (int i = 0; i < navigationDatas.size(); i++) {
            if (i != 7) {
                if (i == position) {
                    navigationDatas.get(i).setSelected(true);
                    last_position = position;
                } else {
                    navigationDatas.get(i).setSelected(false);
                }

            } else {
                navigationDatas.get(last_position).setSelected(true);
            }
        }

        notifyDataSetChanged();
    }

    public void usSelectedAll() {
        for (int i = 0; i < navigationDatas.size(); i++) {
            navigationDatas.get(i).setSelected(false);
        }

        notifyDataSetChanged();
    }


    class ViewHolderHeader extends RecyclerView.ViewHolder {
        private CircleImageView ivProfileImage;
        private ImageView ivSettings;
        private TextViewRegular tvProfileName;
        private TextViewRegular tvEmailAddress;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            ivProfileImage = itemView.findViewById(R.id.ivProfileImage);
            ivSettings = itemView.findViewById(R.id.ivSettings);
            tvProfileName = itemView.findViewById(R.id.tvProfileName);
            tvEmailAddress = itemView.findViewById(R.id.tvEmailAddress);
        }

        public void bind() {
            tvProfileName.setText(Utility.getSharedPreferences(mContext, Constant.NAME));
            tvEmailAddress.setText(Utility.getSharedPreferences(mContext, Constant.EMAIL));
            if (!Utility.getSharedPreferences(mContext, Constant.PROFILE_IMAGE).equalsIgnoreCase("")) {
                Glide.with(mContext).load(Utility.getSharedPreferences(mContext, Constant.PROFILE_IMAGE)).into(ivProfileImage);
            }

            ivProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onProfileClick();

                }
            });

            ivSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onSettingsClick();

                }
            });
        }
    }

}
