package com.tekzee.cardate.ui.profile;

import java.io.Serializable;

public class ImageUrl implements Serializable {
    private String imageUrl;

    public ImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
