package com.tekzee.cardate.ui.introduction;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class IntroductionPresenter extends BasePresenter<IntroductionView> {
    private static final String TAG = IntroductionPresenter.class.getSimpleName();

    public IntroductionPresenter(IntroductionView view, Context mContext) {

        attachView(view,mContext);
    }

    public void getSliderImage() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            addSubscription(apiStores.getSplashScreen(), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);
                        if (jsonObject.getBoolean("status")) {

                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            ArrayList<String> arrayList = new ArrayList<>();
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject imageObject = mJsonArray.getJSONObject(i);
                                arrayList.add(imageObject.getString("image"));
                            }

                            mvpView.onSliderFound(arrayList);

                        } else if (!jsonObject.getBoolean("status")) {


                            mvpView.showInPopup(jsonObject.getString("message"));


                        } else {
                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
