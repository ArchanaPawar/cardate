package com.tekzee.cardate.ui.matchmatching.matches;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MatchesPresenter extends BasePresenter<MatchesView> {
    private static final String TAG = MatchesPresenter.class.getSimpleName();

    public MatchesPresenter(MatchesView view, Context mContext) {

        attachView(view,mContext);
    }

    public void getMyLikes(String filter) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("filter", filter);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getAllMatching(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            ArrayList<MatchModel> arrayList = new ArrayList<>();
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject object = mJsonArray.getJSONObject(i);
                                arrayList.add(new MatchModel(
                                        object.getString("id"),
                                        object.getString("name"),
                                        object.getString("email"),
                                        object.getString("date_of_birth"),
                                        object.getString("age"),
                                        object.getString("city"),
                                        object.getString("city_id"),
                                        object.getString("country"),
                                        object.getString("country_id"),
                                        object.getString("image")
                                ));

                            }

                            mvpView.onMatchSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }
    }


    public void isUserBlocked(String blockUserId ) {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("blockuser_id", blockUserId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.checkBlockUser(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onUserBlocked(jsonObject.getString("message"));


                        } else if (!jsonObject.getBoolean("status")) {

                            mvpView.userNotBlocked();

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }

    public void getFriendStatus(MatchModel matchModel) {
            if (Utility.isConnectingToInternet()) {
                mvpView.hideSoftKeyboard();
                mvpView.showProgressDialog(false);
                JSONObject input = new JSONObject();
                try {
                    input.put("loginuser_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                    input.put("user_id",matchModel.getId() );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.view(TAG, "input : " + input);

                addSubscription(apiStores.getFriendStatus(input), new ApiCallback() {
                    @Override
                    public void onSuccess(Object model) {
                        try {
                            JSONObject jsonObject = (JSONObject) model;
                            Log.view(TAG, "Response : " + jsonObject);

                            if (jsonObject.getBoolean("status")) {

                                mvpView.onFriendStatusSuccess(matchModel,jsonObject.getJSONObject("data"));


                            } else if (!jsonObject.getBoolean("status")) {
                                mvpView.showInPopup(jsonObject.getString("message"));

                            } else {

                                mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                            }

                        } catch (Exception e) {
                            mvpView.dismissProgressDialog();
                            mvpView.showInSnackBar(e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(String msg) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(msg);
                    }

                    @Override
                    public void onTokenExpired(String message) {
                        mvpView.showProgressDialog(message,false);
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                mvpView.dismissProgressDialog();
                                mvpView.restartApp();
                            }

                        }, Constant.LOGOUT_DELAY);

                    }


                    @Override
                    public void onFinish() {
                        mvpView.dismissProgressDialog();
                    }
                });


            } else {
                mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
            }

    }
}
