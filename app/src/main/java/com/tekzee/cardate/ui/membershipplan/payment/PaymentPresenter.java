//package com.tekzee.cardate.ui.membershipplan.payment;
//
//import android.content.Context;
//import android.os.Handler;
//
//import com.tekzee.cardate.R;
//import com.tekzee.cardate.data.network.ApiCallback;
//import com.tekzee.cardate.ui.base.BasePresenter;
//import com.tekzee.cardate.ui.membershipplan.PlanModel;
//import com.tekzee.cardate.utils.Constant;
//import com.tekzee.cardate.utils.Log;
//import com.tekzee.cardate.utils.Utility;
//
//import org.json.JSONObject;
//
//public class PaymentPresenter extends BasePresenter<PaymentView> {
//    private static final String TAG = PaymentPresenter.class.getSimpleName();
//
//    public PaymentPresenter(PaymentView view, Context mContext) {
//
//        attachView(view, mContext);
//    }
//
//    public void insertPayment(PlanModel model) {
//        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
//            JSONObject input = new JSONObject();
//            try {
//                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
//                input.put("plan_id", model.getId());
//                input.put("amount", (Integer.parseInt(model.getDuration()) * Double.parseDouble(model.getAmount())));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Log.view(TAG, "input : " + input);
//
//            addSubscription(apiStores.insertPayment(input), new ApiCallback() {
//                @Override
//                public void onSuccess(Object model) {
//                    try {
//                        JSONObject jsonObject = (JSONObject) model;
//                        Log.view(TAG, "Response : " + jsonObject);
//
//                        if (jsonObject.getBoolean("status")) {
//
//                            mvpView.onPaymentReceived(jsonObject.getJSONObject("data"));
//
//
//                        } else if (!jsonObject.getBoolean("status")) {
//                            mvpView.showInPopup(jsonObject.getString("message"));
//
//                        } else {
//
//                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
//
//                        }
//
//                    } catch (Exception e) {
//                        mvpView.dismissProgressDialog();
//                        mvpView.showInSnackBar(e.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(String msg) {
//                    mvpView.dismissProgressDialog();
//                    mvpView.showInSnackBar(msg);
//                }
//
//                @Override
//                public void onTokenExpired(String message) {
//                    mvpView.showProgressDialog(message, false);
//                    new Handler().postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            mvpView.dismissProgressDialog();
//                            mvpView.restartApp();
//                        }
//
//                    }, Constant.LOGOUT_DELAY);
//
//                }
//
//                @Override
//                public void onFinish() {
//                    mvpView.dismissProgressDialog();
//                }
//            });
//
//
//        } else {
//            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
//        }
//    }
//
//    public void submitPaymentSuccess(String transaction_id, String paymentId, String id) {
//        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
//            JSONObject input = new JSONObject();
//            try {
//                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
//                input.put("transaction_id", transaction_id);
//                input.put("payment_id", paymentId);
//                input.put("plan_id", id);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Log.view(TAG, "input : " + input);
//
//            addSubscription(apiStores.validateTransaction(input), new ApiCallback() {
//                @Override
//                public void onSuccess(Object model) {
//                    try {
//                        JSONObject jsonObject = (JSONObject) model;
//                        Log.view(TAG, "Response : " + jsonObject);
//
//                        if (jsonObject.getBoolean("status")) {
//
//                            mvpView.onOurLocalServerSuccess(jsonObject.getString("message"));
//
//
//                        } else if (!jsonObject.getBoolean("status")) {
//                            mvpView.showInPopup(jsonObject.getString("message"));
//
//                        } else {
//
//                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
//
//                        }
//
//                    } catch (Exception e) {
//                        mvpView.dismissProgressDialog();
//                        mvpView.showInSnackBar(e.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(String msg) {
//                    mvpView.dismissProgressDialog();
//                    mvpView.showInSnackBar(msg);
//                }
//
//                @Override
//                public void onTokenExpired(String message) {
//                    mvpView.showProgressDialog(message, false);
//                    new Handler().postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            mvpView.dismissProgressDialog();
//                            mvpView.restartApp();
//                        }
//
//                    }, Constant.LOGOUT_DELAY);
//
//                }
//
//                @Override
//                public void onFinish() {
//                    mvpView.dismissProgressDialog();
//                }
//            });
//
//
//        } else {
//            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
//        }
//
//    }
//}
