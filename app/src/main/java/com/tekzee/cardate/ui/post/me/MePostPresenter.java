package com.tekzee.cardate.ui.post.me;

import android.content.Context;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MePostPresenter extends BasePresenter<MePostView> {
    private static final String TAG = MePostPresenter.class.getSimpleName();

    public MePostPresenter(MePostView view, Context mContext) {

        attachView(view,mContext);
    }


    public void getMePost(String filter) {
        if (Utility.isConnectingToInternet()) {
//            mvpView.hideSoftKeyboard();
//            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
                input.put("filter", filter);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.getPosts(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            ArrayList<MePostModel> arrayList = new ArrayList<>();
                            JSONArray mJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                JSONObject object = mJsonArray.getJSONObject(i);
                                arrayList.add(new MePostModel(
                                        object.getString("id"),
                                        object.getString("post_image"),
                                        object.getString("status"),
                                        object.getString("created_at"),
                                        object.getString("updated_at"),
                                        object.getJSONObject("user").getString("id"),
                                        object.getJSONObject("user").getString("name"),
                                        object.getJSONObject("user").getString("image")

                                ));
                            }

                            mvpView.onPostSuccess(arrayList);


                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.onPostFail(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {
                        mvpView.dismissProgressDialog();
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.dismissProgressDialog();
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message,false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mvpView.dismissProgressDialog();
                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {
                    mvpView.dismissProgressDialog();
                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
