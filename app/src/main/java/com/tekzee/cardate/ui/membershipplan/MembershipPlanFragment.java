package com.tekzee.cardate.ui.membershipplan;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.base.MvpFragment;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;

public class MembershipPlanFragment extends MvpFragment<MembershipPlanPresenter> implements MembershipPlanView {
    private  ViewPager viewPager;
    private TextViewRegular tvValidity;
    private TextViewRegular tvPlanName;
    private LinearLayout llCurrentPlan;

    public static MembershipPlanFragment newInstance() {
        return new MembershipPlanFragment();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_membership_plan, container, false);
        init(mView);



        return mView;

    }

    private void init(View mView){
        viewPager = mView.findViewById(R.id.view_pager);
        tvPlanName = mView.findViewById(R.id.tvPlanName);
        tvValidity = mView.findViewById(R.id.tvValidity);
        llCurrentPlan = mView.findViewById(R.id.llCurrentPlan);
        llCurrentPlan.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        mvpPresenter.getMembershipPlan();
    }

    @Override
    protected MembershipPlanPresenter createPresenter() {
        return new MembershipPlanPresenter(this,getAppContext());
    }

    @Override
    public Activity getAppContext() {
        return getActivity();
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {
        snackBarTop(R.id.drawerLayout, message);
    }

    @Override
    public void onPlanFoundSuccess(ArrayList<PlanModel> arrayList) {

        viewPager.setAdapter(new MyViewPagerAdapter(getChildFragmentManager(),arrayList));

    }

    @Override
    public void updateCurrentPlan(String planId, String planName, String startDate, String endDate, String duration) {
        if(planId.equalsIgnoreCase("")){
            llCurrentPlan.setVisibility(View.GONE);

        }else {
            llCurrentPlan.setVisibility(View.VISIBLE);
            tvPlanName.setText(planName);
            tvValidity.setText(startDate+" "+(getAppContext().getResources().getString(R.string.to))+" "+endDate);
            Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_PREMIUM_USER, true);

        }

    }
}
