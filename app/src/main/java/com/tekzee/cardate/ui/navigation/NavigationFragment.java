package com.tekzee.cardate.ui.navigation;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewRegular;
import com.tekzee.cardate.ui.profile.ProfileFragment;
import com.tekzee.cardate.ui.settings.SettingsFragment;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import java.util.ArrayList;

/**
 * Created by Hasnain on 3-Apr-18.
 */

public class NavigationFragment extends Fragment {
    private static final String TAG = NavigationFragment.class.getSimpleName();
    public NavigationAdapter adapter;
    private RecyclerView rvNavigation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        rvNavigation = view.findViewById(R.id.rv_navigation);
        rvNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        String isSelectorChange = getArguments().getString("isSelectorChange");
        fillData();
        setAdapter();

        adapter.setSelected(Integer.parseInt(isSelectorChange));


        /**
         * set app version in text
         * tv_version
         */
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            ((TextViewRegular) view.findViewById(R.id.tv_version)).setText(getString(R.string.txt_version) + " " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        view.findViewById(R.id.tv_version).setOnClickListener(null);

        return view;
    }

    public void updateProfileData() {
        if (adapter != null) {
            adapter.refreshAdapter(fillData());

        }
    }


    private ArrayList<NavigationData> fillData() {
        ArrayList<NavigationData> navigationDataArrayList = new ArrayList<>();
        String title_array_navigation[] = getResources().getStringArray(R.array.title_array_navigation);
        TypedArray drawable_array_navigation = getResources().obtainTypedArray(R.array.drawable_array_navigation);

        for (int i = 0; i < title_array_navigation.length; i++) {
            NavigationData navigationData = new NavigationData();
            String title = title_array_navigation[i];
            /**
             * set match count
             */
            if (title.equalsIgnoreCase(getResources().getString(R.string.menu_match_making))) {
                String MATCH_COUNT = Utility.getSharedPreferences(getActivity(), Constant.MATCH_COUNT);
                Log.view(TAG,"MATCH_COUNT : "+MATCH_COUNT);
                navigationData.setCount(MATCH_COUNT);

            } else if (title.equalsIgnoreCase(getResources().getString(R.string.menu_friends))) {
                String FRIEND_COUNT = Utility.getSharedPreferences(getActivity(), Constant.FRIEND_COUNT);
                Log.view(TAG,"FRIEND_COUNT : "+FRIEND_COUNT);
                navigationData.setCount(FRIEND_COUNT);

            } else if (title.equalsIgnoreCase(getResources().getString(R.string.menu_chat))) {
                String CHAT_COUNT = Utility.getSharedPreferences(getActivity(), Constant.CHAT_COUNT);

                Log.view(TAG,"CHAT_COUNT : "+CHAT_COUNT);
                navigationData.setCount(CHAT_COUNT);

            } else {
                navigationData.setCount("0");
            }


            navigationData.setName(title);
            navigationData.setDrawableId(drawable_array_navigation.getResourceId(i, -1));
            navigationDataArrayList.add(navigationData);
        }

        return navigationDataArrayList;
    }


    private void replaceFragment(int position) {
        ((NavigationActivity) getActivity()).replaceFragment(position);
        adapter.setSelected(position);
    }

    private void setAdapter() {
        rvNavigation.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NavigationAdapter(new NavigationAdapter.INavigation() {
            @Override
            public void onItemClick(int position) {
                Log.view(TAG, "View" + position);
                replaceFragment(position);
            }

            @Override
            public void onProfileClick() {
                ((NavigationActivity) getActivity()).resetAllFilter();
                ((NavigationActivity) getActivity()).replaceFragment(ProfileFragment.newInstance(), getActivity().getResources().getString(R.string.menu_profile));
                adapter.usSelectedAll();


            }

            @Override
            public void onSettingsClick() {
                ((NavigationActivity) getActivity()).resetAllFilter();
                ((NavigationActivity) getActivity()).replaceFragment(SettingsFragment.newInstance(), getActivity().getResources().getString(R.string.menu_settings));
                adapter.usSelectedAll();

            }
        });
        rvNavigation.setAdapter(adapter);
        adapter.refreshAdapter(fillData());
    }

    public void changeRowSelector(int position) {
        adapter.setSelected(position);

    }

}
