package com.tekzee.cardate.ui.changepassword;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

public interface ChangePasswordView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onchangePasswordSuccess(String message);

}
