package com.tekzee.cardate.ui.imageselector;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.profile.ImageUrl;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder>  {
    private ArrayList<ImageUrl> arrayList;
    private RowSelect listener;
    private Context mContext;

    public ImageAdapter(ArrayList<ImageUrl> arrayList, RowSelect listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    public interface RowSelect {
        void onSelect(ImageUrl model);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_selector, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(arrayList.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cvRoot = itemView.findViewById(R.id.cvRoot);
            ivImage = itemView.findViewById(R.id.ivImage);
        }


        public void bind(final ImageUrl model, final RowSelect listener) {
            /**
             * set image
             */
            String imageUrl = model.getImageUrl();
            if (!imageUrl.equalsIgnoreCase("")) {
                Glide.with(mContext).load(imageUrl).into(ivImage);
            }

            cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelect(model);
                }
            });


        }
    }
}
