package com.tekzee.cardate.ui.base;


import android.content.Context;

import com.tekzee.cardate.data.network.ApiClient;
import com.tekzee.cardate.data.network.ApiStores;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public class BasePresenter<V> {
    public V mvpView;
    protected ApiStores apiStores;
    protected ApiStores instargarmApiStores;
    private CompositeSubscription mCompositeSubscription;

    public void attachView(V mvpView, Context mContext) {
        this.mvpView = mvpView;
        apiStores = ApiClient.retrofit(mContext).create(ApiStores.class);
        instargarmApiStores = ApiClient.getClient().create(ApiStores.class);
    }


    public void detachView() {
        this.mvpView = null;
        onUnsubscribe();
    }


    //RXjava unregisters to avoid memory leaks
    public void onUnsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription.hasSubscriptions()) {
            mCompositeSubscription.unsubscribe();
        }
    }


    public void addSubscription(Observable observable, Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber));
    }



    public void addSubscriptionInterval(final Observable observable , Subscriber subscriber) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }


        mCompositeSubscription.add(Observable.interval(0, 5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Long, Observable<?>>() {
                    @Override
                    public Observable<?> call(Long aLong) {
                        return observable;
                    }
                })
                .repeat()
                .subscribe(subscriber));

    }


}
