package com.tekzee.cardate.ui.friends.all;

import android.app.Activity;

import com.tekzee.cardate.ui.base.BaseView;

import java.util.ArrayList;

public interface AllFriendsView extends BaseView {

    Activity getAppContext();

    void showInPopup(String message);

    void showInSnackBar(String message);

    void onPostSuccess(ArrayList<AllFriendModel> arrayList);

    void onPostFail(String message);
}
