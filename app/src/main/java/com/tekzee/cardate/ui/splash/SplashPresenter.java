package com.tekzee.cardate.ui.splash;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.CountDownTimer;
import android.os.Handler;

import com.tekzee.cardate.R;
import com.tekzee.cardate.data.network.ApiCallback;
import com.tekzee.cardate.ui.base.BasePresenter;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

public class SplashPresenter extends BasePresenter<SplashView> {
    private static final String TAG = SplashPresenter.class.getSimpleName();

    public SplashPresenter(SplashView view, Context mContext) {

        attachView(view, mContext);
    }

    public void validateAppVersion(String token) {
        if (Utility.isConnectingToInternet()) {
            JSONObject input = new JSONObject();
            try {
                PackageInfo pInfo = mvpView.getAppContext().getPackageManager().getPackageInfo(mvpView.getAppContext().getPackageName(), 0);
                input.put("app_version", pInfo.versionName);
                input.put("fcmToken", token.equalsIgnoreCase("") ? Utility.getSharedPreferences(mvpView.getAppContext(), Constant.FCM_TOKEN) : token);
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.validateAppVersion(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            continueExecution(jsonObject);


                        } else if (!jsonObject.getBoolean("status")) {

                            if (jsonObject.getJSONObject("data").getBoolean("isMandatory")) {

                                mvpView.showUpdatePopup(jsonObject.getString("message"));

                            } else {

                                continueExecution(jsonObject);

                            }


                        } else {
                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));
                        }

                    } catch (Exception e) {
                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {
                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onFinish() {

                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {

                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }
            });


        } else {
            mvpView.onNoInternetConnection();
        }

    }

    private void continueExecution(JSONObject jsonObject) {
        try {
            String languageCode = Utility.getSharedPreferences(mvpView.getAppContext(), Constant.LANGUAGE_CODE);
            if (!languageCode.equalsIgnoreCase("")) {
                if (Utility.getSharedPreferencesBoolean(mvpView.getAppContext(), Constant.IS_LOGIN)) {

                    /**
                     * Redirection check for user
                     */
                    JSONObject response = jsonObject.getJSONObject("data");

                    mvpView.onStartNavigationActivity();


                } else {

                    mvpView.onStartIntroductionActivity();

                }

            } else {

                mvpView.onStartLanguageSettingsActivity();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void logoutFromApp() {
        if (Utility.isConnectingToInternet()) {
            mvpView.hideSoftKeyboard();
            mvpView.showProgressDialog(false);
            JSONObject input = new JSONObject();
            try {
                input.put("user_id", Utility.getSharedPreferences(mvpView.getAppContext(), Constant.USER_ID));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.view(TAG, "input : " + input);

            addSubscription(apiStores.logout(input), new ApiCallback() {
                @Override
                public void onSuccess(Object model) {
                    try {
                        JSONObject jsonObject = (JSONObject) model;
                        Log.view(TAG, "Response : " + jsonObject);

                        if (jsonObject.getBoolean("status")) {

                            mvpView.onLogoutSuccess(jsonObject.getString("message"));

                        } else if (!jsonObject.getBoolean("status")) {
                            mvpView.showInPopup(jsonObject.getString("message"));

                        } else {

                            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.something_went_wrong));

                        }

                    } catch (Exception e) {

                        mvpView.showInSnackBar(e.getMessage());
                    }
                }

                @Override
                public void onFailure(String msg) {

                    mvpView.showInSnackBar(msg);
                }

                @Override
                public void onTokenExpired(String message) {
                    mvpView.showProgressDialog(message, false);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {

                            mvpView.restartApp();
                        }

                    }, Constant.LOGOUT_DELAY);

                }

                @Override
                public void onFinish() {

                }
            });


        } else {
            mvpView.showInPopup(mvpView.getAppContext().getResources().getString(R.string.internet_error));
        }

    }
}
