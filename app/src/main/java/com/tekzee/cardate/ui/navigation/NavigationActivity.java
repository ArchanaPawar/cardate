package com.tekzee.cardate.ui.navigation;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.custom.TextViewLight;
import com.tekzee.cardate.ui.base.MvpMapActivity;
import com.tekzee.cardate.ui.browse.BrowseFragment;
import com.tekzee.cardate.ui.chat.chat.ChatFragment;
import com.tekzee.cardate.ui.chat.model.Chat;
import com.tekzee.cardate.ui.filter.FilterActivity;
import com.tekzee.cardate.ui.friends.FriendsFragment;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.ui.matchmatching.MatchMatchingFragment;
import com.tekzee.cardate.ui.membershipplan.MembershipPlanFragment;
import com.tekzee.cardate.ui.post.PostFragment;
import com.tekzee.cardate.ui.profile.ProfileFragment;
import com.tekzee.cardate.ui.searchbycar.SearchByCarFragment;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.MyCustomDialog;
import com.tekzee.cardate.utils.Utility;

import java.util.HashMap;

import cn.refactor.lib.colordialog.ColorDialog;

public class NavigationActivity extends MvpMapActivity<NavigationPresenter> implements NavigationView {
    private static final String TAG = NavigationActivity.class.getSimpleName();
    private Toolbar toolbar;
    private DrawerLayout drawer;
    public FragmentManager fragmentManager;
    private NavigationFragment navigationFragment;
    private boolean backPressed = false;
    private int currentFragment = -1;
    private Location mLocation;
    private ImageView toolbar_filter;
    private ImageView toolbar_logo;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabaseReference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_naigation);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        init();
        findViewById(R.id.toolbar_icon_navigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCloseDrawer();
            }
        });

        toolbar_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.getSharedPreferencesBoolean(getAppContext(), Constant.IS_PREMIUM_USER)) {
                    startActivityForResult(new Intent(getAppContext(), FilterActivity.class), Constant.APPLY_FILTER_REQUEST_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                } else {

                    showInPopup(getResources().getString(R.string.warring_preimum_access));

                }

            }
        });


        findViewById(R.id.flContainerNavigationMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    /**
     * goto membership plan
     */
    public void gotoMembershipPlanFragment(int position) {
       replaceFragment(position);
        navigationFragment.changeRowSelector(position);
    }

    private void init() {
        toolbar_filter = findViewById(R.id.toolbar_filter);
        toolbar_filter.setVisibility(View.INVISIBLE);

        toolbar_logo = findViewById(R.id.toolbar_logo);
        toolbar_logo.setVisibility(View.INVISIBLE);

        fragmentManager = getSupportFragmentManager();
        toolbar = findViewById(R.id.toolbar);
        setToolbarTitle(getResources().getString(R.string.menu_browse));
        toolbar.setNavigationIcon(null);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        replaceFragment(0);

        /**
         * init firebase object
         */
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(mFirebaseUser.getUid());
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                MyFirebaseUser user = dataSnapshot.getValue(MyFirebaseUser.class);

                Log.view(TAG, "******************MyFirebaseUser******************");
                Log.view(TAG, "id : " + user.getId());
                Log.view(TAG, "name : " + user.getName());
                Log.view(TAG, "email : " + user.getEmail());
                Log.view(TAG, "image : " + user.getImageUrl());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void setToolbarTitle(String title) {
        ((TextViewLight) findViewById(R.id.toolbar_title)).setText(title);
    }

    public void closeNavigationDrawer() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer(Gravity.LEFT);
        }

    }


    public void populateNavigationView(String isSelectorChange) {
        Bundle bundle = new Bundle();
        bundle.putString("isSelectorChange", isSelectorChange);

        navigationFragment = new NavigationFragment();
        navigationFragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainerNavigationMenu, navigationFragment, "Navigation").commit();
    }

    public void replaceFragment(int position) {

        if (currentFragment == position && position != 0)
            return;

        Fragment fragment = null;
        String title = null;

        switch (position) {
            case 0:
                toolbar_filter.setVisibility(View.VISIBLE);
                toolbar_logo.setVisibility(View.VISIBLE);
                fragment = BrowseFragment.newInstance();
                title = getResources().getString(R.string.menu_browse);
                break;

            case 1:
                resetAllFilter();
                fragment = MatchMatchingFragment.newInstance();
                title = getResources().getString(R.string.menu_match_making);
                break;

            case 2:
                resetAllFilter();
                fragment = ChatFragment.newInstance();
                title = getResources().getString(R.string.menu_chat);
                break;

            case 3:
                resetAllFilter();
                fragment = PostFragment.newInstance();
                title = getResources().getString(R.string.manu_post);
                break;


            case 4:
                resetAllFilter();
                fragment = FriendsFragment.newInstance();
                title = getResources().getString(R.string.menu_friends);
                break;


            case 5:
                resetAllFilter();
                fragment = SearchByCarFragment.newInstance();
                title = getResources().getString(R.string.menu_search_by_car);
                break;

            case 6:
                resetAllFilter();
                fragment = MembershipPlanFragment.newInstance();
                title = getResources().getString(R.string.menu_membership_plan);
                break;

            case 7:
                /**
                 * Logout
                 */
                fragment = null;
                openCloseDrawer();

                ColorDialog dialog = new ColorDialog(getAppContext());
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setContentText(getResources().getString(R.string.message_logout));
                dialog.setPositiveListener(getResources().getString(R.string.yes), new ColorDialog.OnPositiveListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();
                        deleteFirebaseToken();
                        FirebaseAuth.getInstance().signOut();
                        mvpPresenter.logoutFromApp();

                    }
                }).setNegativeListener(getResources().getString(R.string.no), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {

                        dialog.dismiss();
                    }
                }).show();

                break;
        }

        if (fragment != null) {
            replaceFragment(fragment, title);

        } else {
            closeNavigationDrawer();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        this.mLocation = location;
        progressDialog.dismiss();

    }

    public Location getLocation() {
        if (mLocation != null) {
            return mLocation;
        }

        return null;
    }

    public void replaceFragment(Fragment fragment, String title) {
        setToolbarTitle(title);
        closeNavigationDrawer();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.flContainerFragment, fragment).commit();
    }


    private void openCloseDrawer() {
        if (drawer.isDrawerOpen(Gravity.LEFT))
            drawer.closeDrawer(Gravity.LEFT);

        else
            drawer.openDrawer(Gravity.LEFT);

    }


    @Override
    protected NavigationPresenter createPresenter() {
        return new NavigationPresenter(this, getAppContext());
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer(Gravity.LEFT);
        } else {


            if (backPressed) {
                finish();
                System.exit(0);

            } else {

                backPressed = true;
                snackBarBottom(R.id.drawerLayout, getResources().getString(R.string.exit));
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        backPressed = false;
                    }
                }, 2000);

            }
        }
    }


    public void updateProfileInfo() {
        if (navigationFragment != null) {
            navigationFragment.updateProfileData();
        }
    }


    @Override
    public Activity getAppContext() {
        return this;
    }

    @Override
    public void showInPopup(String message) {
        MyCustomDialog.show(getAppContext(), message);

    }

    @Override
    public void showInSnackBar(String message) {

        snackBarTop(R.id.drawerLayout, message);

    }

    @Override
    public void onLogoutSuccess(String message) {
        // FirebaseAuth.getInstance().signOut();
//        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
//        firebaseAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                if (firebaseAuth.getCurrentUser() == null){
                    try{
                        String languageCode = Utility.getSharedPreferences(getAppContext(), Constant.LANGUAGE_CODE);
                        Utility.clearSharedPreference(getAppContext());
                        Utility.setSharedPreferenceBoolean(getAppContext(), Constant.IS_LOGIN, false);
                        Utility.setSharedPreference(getAppContext(), Constant.LANGUAGE_CODE, languageCode);
                        Intent intent = new Intent(mActivity, IntroductionActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
                        finishAffinity();

                    }catch (Exception e){
                        e.printStackTrace();
                    }


//                }
//            }
//        });
//
//        firebaseAuth.signOut();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment mFragment = fragmentManager.findFragmentById(R.id.flContainerFragment);
        switch (requestCode) {
            case Constant.APPLY_FILTER_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    if (mFragment instanceof BrowseFragment) {
                        ((BrowseFragment) mFragment).applyFilter();
                    }
                }

                break;

            case Constant.IMAGE_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    if (mFragment instanceof ProfileFragment) {
                        ((ProfileFragment) mFragment).onActivityResult(requestCode, resultCode, data);
                    }
                }

                break;

            default: {
                break;
            }

        }

    }


    @Override
    public void resetAllFilter() {
        toolbar_filter.setVisibility(View.INVISIBLE);
        toolbar_logo.setVisibility(View.INVISIBLE);
        /**
         * gender
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_GENDER, "");

        /**
         * age
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MIN, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_AGE_MAX, "");

        /**
         * location
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_LOCATION, "");

        /**
         * Country
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_COUNTRY, "");

        /**
         * City
         */
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY_ID, "");
        Utility.setSharedPreference(getAppContext(), Constant.FILTER_CITY, "");

    }


    @Override
    public void onResume() {
        super.onResume();
        status("online");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        status("offline");
    }

    public void populateSideMenu() {
        getChatCount();
        populateNavigationView("0");
    }

    private void status(String status) {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("status", status);

            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void deleteFirebaseToken() {
        try {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("status", "offline");
            hashMap.put("token", "logout");

            reference.updateChildren(hashMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getChatCount() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_CHATS);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try{


                    int unread = 0;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Chat chat = snapshot.getValue(Chat.class);
                        assert chat != null;
                        if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsseen()) {
                            unread++;
                        }
                    }

//                    if (unread == 0) {
//                        Utility.setSharedPreference(getAppContext(),Constant.CHAT_COUNT,"0");
//                    } else {
                        Utility.setSharedPreference(getAppContext(),Constant.CHAT_COUNT,""+unread+"");
//                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//        adapter.addFragment(ChatFragment.newInstance(), tabTitle[0]);
//        adapter.addFragment(UserFragment.newInstance(), tabTitle[1]);
//        viewPager.setAdapter(adapter);
    }
}
