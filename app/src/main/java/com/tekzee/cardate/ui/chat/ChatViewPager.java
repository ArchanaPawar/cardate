package com.tekzee.cardate.ui.chat;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.chat.chat.ChatFragment;
import com.tekzee.cardate.ui.chat.model.Chat;
import com.tekzee.cardate.ui.chat.model.User;
import com.tekzee.cardate.ui.chat.user.UserFragment;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;

public class ChatViewPager extends Fragment {
    private static final String TAG = ChatViewPager.class.getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    //    String[] tabTitle = {"CHAT", "USERS"};
//    private ChatViewPagerAdapter adapter;

    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    public static ChatViewPager newInstance() {
        return new ChatViewPager();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chat_view_pager, container, false);
        init(mView);


        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
//                username.setText(user.getUsername());
//                if (user.getImageURL().equals("default")){
//                    profile_image.setImageResource(R.mipmap.ic_launcher);
//                } else {
//
//                    //change this
//                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(profile_image);
//                }

                Log.view(TAG,"id : "+user.getId());
                Log.view(TAG,"name : "+user.getName());
                Log.view(TAG,"email : "+user.getEmail());
                Log.view(TAG,"image : "+user.getImageUrl());
                Log.view(TAG,"status : "+user.getStatus());
                Log.view(TAG,"search : "+user.getSearch());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return mView;

    }

    private void init(View mView) {
        //Initializing viewPager
        viewPager = mView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        //Initializing the tablayout
        tabLayout = mView.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

//        try {
//            setupTabIcons();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private void setupViewPager(final ViewPager viewPager) {
        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_CHATS);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try{

                    ChatViewPagerAdapter adapter = new ChatViewPagerAdapter(getChildFragmentManager());
                    int unread = 0;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Chat chat = snapshot.getValue(Chat.class);
                        assert chat != null;
                        if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsseen()) {
                            unread++;
                        }
                    }

                    if (unread == 0) {
                        adapter.addFragment(ChatFragment.newInstance(), "Chats");
                    } else {
                        adapter.addFragment(ChatFragment.newInstance(), "(" + unread + ") Chats");
                    }

                    adapter.addFragment(UserFragment.newInstance(), "Users");

                    viewPager.setAdapter(adapter);

                    tabLayout.setupWithViewPager(viewPager);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//        adapter.addFragment(ChatFragment.newInstance(), tabTitle[0]);
//        adapter.addFragment(UserFragment.newInstance(), tabTitle[1]);
//        viewPager.setAdapter(adapter);
    }


//    private void setupTabIcons() {
//        for (int i = 0; i < tabTitle.length; i++) {
//            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
//        }
//
//
//    }

//    private View prepareTabView(int pos) {
//        View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
//        TextViewRegular tv_title = view.findViewById(R.id.tv_title);
//        TextViewRegular tv_count = view.findViewById(R.id.tv_count);
//        tv_title.setText(tabTitle[pos]);
//        if (unreadCount[pos] > 0) {
//            tv_count.setVisibility(View.VISIBLE);
//            tv_count.setText("" + unreadCount[pos]);
//        } else
//            tv_count.setVisibility(View.GONE);
//
//
//        return view;
//    }


//    private void status(String status) {
//        reference = FirebaseDatabase.getInstance().getReference(Constant.FIREBASE_USERS).child(firebaseUser.getUid());
//
//        HashMap<String, Object> hashMap = new HashMap<>();
//        hashMap.put("status", status);
//
//        reference.updateChildren(hashMap);
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        status("online");
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        status("offline");
//    }
}
