package com.tekzee.cardate.ChatVoicePlayer;

import android.media.AudioManager;
import android.media.MediaPlayer;

public class MyPlayer {
    private static MediaPlayer mediaPlayer = null;

    private MyPlayer() {

    }

    public static MediaPlayer getInstance(String AudioPath) {
        if (mediaPlayer == null) {
            try{
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(AudioPath);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.prepare();
                mediaPlayer.setVolume(10, 10);
            }catch (Exception e){
                e.printStackTrace();
            }


        }

        return mediaPlayer;
    }
}
