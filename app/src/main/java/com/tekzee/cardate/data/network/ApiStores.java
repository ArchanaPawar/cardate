package com.tekzee.cardate.data.network;


import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;


/**
 * Created by Hasnain on 24-Apr-19.
 */

public interface ApiStores {

    @Headers("Content-Type: application/json")
    @POST("auth/validateAppVersion")
    Observable<JSONObject> validateAppVersion(@Body JSONObject jsonObject);


    @Headers("Content-Type: application/json")
    @GET("auth/getSplashScreen")
    Observable<JSONObject> getSplashScreen();

    //    @Headers("Content-Type: application/json")
//    @GET("auth/getTnC_unsubscribe")
//    Observable<JSONObject> getTnC_unsubscribe();

    @Headers("Content-Type: application/json")
    @POST("auth/getTnC_unsubscribe")
    Observable<JSONObject> getTnC_unsubscribe(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/sendNotification")
    Observable<JSONObject> sendNotification(@Body JSONObject jsonObject);


    @Headers("Content-Type: application/json")
    @GET("auth/getAllCountry")
    Observable<JSONObject> getAllCountry();

    @Headers("Content-Type: application/json")
    @POST("auth/getAllCity")
    Observable<JSONObject> getAllCity(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/signup")
    Observable<JSONObject> signUp(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/getPassword")
    Observable<JSONObject> getPassword(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/signin")
    Observable<JSONObject> signIn(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/forgotPassword")
    Observable<JSONObject> forgotPassword(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("auth/logout")
    Observable<JSONObject> logout(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("addCarNo")
    Observable<JSONObject> addCarNo(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("nearbyMeV1")
    Observable<JSONObject> nearbyMe(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getUserDetail")
    Observable<JSONObject> getUserDetail(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getFriendStatus")
    Observable<JSONObject> getFriendStatus(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("blockUser")
    Observable<JSONObject> blockUser(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("sendFriendRequest")
    Observable<JSONObject> sendFriendRequest(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getAllMatching")
    Observable<JSONObject> getAllMatching(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("searchbyCar")
    Observable<JSONObject> searchByCar(@Body JSONObject jsonObject);


    @Headers("Content-Type: application/json")
    @POST("getPosts")
    Observable<JSONObject> getPosts(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getPlan")
    Observable<JSONObject> getPlan(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("insertPayment")
    Observable<JSONObject> insertPayment(@Body JSONObject jsonObject);

//    @Headers("Content-Type: application/json")
//    @POST("validateTransaction")
//    Observable<JSONObject> validateTransaction(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("validateTransactionV1")
    Observable<JSONObject> validateTransactionV1(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getAllFriends")
    Observable<JSONObject> getAllFriends(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("acceptrejectRequest")
    Observable<JSONObject> acceptRejectRequest(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("checkBlockUser")
    Observable<JSONObject> checkBlockUser(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("doLike")
    Observable<JSONObject> doLike(@Body JSONObject jsonObject);


    @Headers("Content-Type: application/json")
    @POST("contactUs")
    Observable<JSONObject> contactUs(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("blockUserList")
    Observable<JSONObject> blockUserList(@Body JSONObject jsonObject);


    @Headers("Content-Type: application/json")
    @POST("unBlockUser")
    Observable<JSONObject> unBlockUser(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("changePassword")
    Observable<JSONObject> changePassword(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("cancelAccount")
    Observable<JSONObject> cancelAccount(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getSetting")
    Observable<JSONObject> getSetting(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("setSetting")
    Observable<JSONObject> setSetting(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("getUserProfile")
    Observable<JSONObject> getUserProfile(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("unsubscribePlan")
    Observable<JSONObject> unsubscribePlan(@Body JSONObject jsonObject);

    @Headers("Content-Type: application/json")
    @GET("auth/getAllInterest")
    Observable<JSONObject> getAllInterest();


    @Headers("Content-Type: application/json")
    @GET("v1/users/self/media/recent")
    Observable<JSONObject> getInstargramImages(@Query("access_token") String access_token);

    @Headers("Content-Type: application/json")
    @POST("updateprofile")
    Observable<JSONObject> updateProfile(@Body JSONObject jsonObject);

    @Multipart
    @POST("createPost")
    Observable<JSONObject> uploadImage(@Part MultipartBody.Part formData,
                                       @Part("user_id") RequestBody userId,
                                       @Part("type") RequestBody name);
}
