package com.tekzee.cardate.data.network;

import android.content.Context;
import android.util.Log;

import com.tekzee.cardate.BuildConfig;
import com.tekzee.cardate.data.json.JSONConverterFactory;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public class ApiClient {
    public static Retrofit mRetrofit;
    public static Retrofit instargramRetrofit;

    public static Retrofit retrofit(final Context mContext) {
        if (mRetrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
                builder.addInterceptor(loggingInterceptor);
            }

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();
                    Log.e("lang",Utility.getSharedPreferences(mContext, Constant.LANGUAGE_CODE));
                    Log.e("loggedInUserId",Utility.getSharedPreferences(mContext, Constant.USER_ID));
                    Request request = original.newBuilder()
                            .header("Authorization", "Bearer "+ Utility.getSharedPreferences(mContext, Constant.AUTH_TOKEN))
                            .header("loggedInUserId", Utility.getSharedPreferences(mContext, Constant.USER_ID))
                            .header("lang", Utility.getSharedPreferences(mContext, Constant.LANGUAGE_CODE))
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            OkHttpClient okHttpClient = builder
                    .readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .build();

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(JSONConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofit;
    }


    public static Retrofit getClient() {
        if (instargramRetrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
                builder.addInterceptor(loggingInterceptor);
            }

            OkHttpClient okHttpClient = builder
                    .readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .build();

            instargramRetrofit = new Retrofit.Builder()
                    .baseUrl("https://api.instagram.com/")
                    .addConverterFactory(JSONConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }

        return instargramRetrofit;


    }


}
