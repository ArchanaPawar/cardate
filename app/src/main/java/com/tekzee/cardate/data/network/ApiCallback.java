package com.tekzee.cardate.data.network;


import android.content.Intent;

import com.tekzee.cardate.R;
import com.tekzee.cardate.ui.introduction.IntroductionActivity;
import com.tekzee.cardate.utils.Constant;
import com.tekzee.cardate.utils.Log;
import com.tekzee.cardate.utils.Utility;

import org.json.JSONObject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * Created by Hasnain on 24-Apr-19.
 */

public abstract class ApiCallback<M> extends Subscriber<M> {

    public abstract void onSuccess(M model);

    public abstract void onFailure(String msg);

    public abstract void onFinish();

    public abstract void onTokenExpired(String message);


    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            //httpException.response().errorBody().string()
            int code = httpException.code();
            String msg = httpException.getMessage();
            Log.view("code=", "" + code);
            if (code == 504) {
                msg = "Network error with code 504";
            }
            if (code == 502 || code == 404) {
                msg = "The server is abnormal. Please try again later";
            }
            onFailure(msg);
        } else {
            onFailure(e.getMessage());
        }
        onFinish();
    }

    @Override
    public void onNext(M model) {
        try {
            JSONObject jsonObject = (JSONObject) model;
            if (jsonObject.has("status_code")) {
                if (jsonObject.getInt("status_code") == Constant.STATUS_CODE) {

                    onTokenExpired(jsonObject.getString("message"));

                } else {
                    onSuccess(model);
                }
            } else {

                onSuccess(model);

            }

            Log.view("response", "- " + model.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCompleted() {
        onFinish();
    }
}
