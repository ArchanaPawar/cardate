package com.tekzee.cardate;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.util.Base64;

import com.tekzee.cardate.utils.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ApplicationController extends Application {
    private static final String TAG = ApplicationController.class.getSimpleName();
    public static final String CHANNEL_FRIEND_REQUEST = "FRIEND_REQUEST";
    public static final String CHANNEL_FRIEND_RESPONSE = "CHANNEL_FRIEND_RESPONSE";

    @Override
    public void onCreate() {
        super.onCreate();

//        printHashKey();
        createNotificationChannel();
    }

//    public void printHashKey() {
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String hashKey = new String(Base64.encode(md.digest(), 0));
//                Log.view(TAG, "printHashKey() Hash Key: " + hashKey);
//            }
//        } catch (NoSuchAlgorithmException e) {
//            Log.view(TAG, "printHashKey()"+ e);
//        } catch (Exception e) {
//            Log.view(TAG, "printHashKey()"+ e);
//        }
//    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel1 = new NotificationChannel(CHANNEL_FRIEND_REQUEST,getResources().getString(R.string.friend_request), NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription(getResources().getString(R.string.friend_request_channel));

            NotificationChannel channel2 = new NotificationChannel(CHANNEL_FRIEND_RESPONSE,getResources().getString(R.string.friend_response), NotificationManager.IMPORTANCE_HIGH);
            channel2.setDescription(getResources().getString(R.string.friend_response_channel));

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);

        }
    }

}



