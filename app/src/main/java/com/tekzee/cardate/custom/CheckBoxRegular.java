package com.tekzee.cardate.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CheckBoxRegular extends androidx.appcompat.widget.AppCompatCheckBox {

    // Constructors
    public CheckBoxRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CheckBoxRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckBoxRegular(Context context) {
        super(context);
        init();
    }

    // This class requires myfont.ttf to be in the assets/fonts folder
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"JosefinSans-Regular.ttf");
        setTypeface(tf);
    }
}