package com.tekzee.cardate.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.tekzee.cardate.R;


/**
 * Created by Hasnain on 24-Apr-19.
 */

public class TextViewSemiBold extends AppCompatTextView {
private Typeface tf = null;
private String customFont;
public TextViewSemiBold(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    setCustomFontTextView(context, attrs);
}

public TextViewSemiBold(Context context, AttributeSet attrs) {
    super(context, attrs);
    setCustomFontTextView(context, attrs);
}

public TextViewSemiBold(Context context) {
    super(context);

}
public boolean setCustomFontTextView(Context ctx, String asset) {
    try {
        tf = Typeface.createFromAsset(ctx.getAssets(), "JosefinSans-SemiBold.ttf");
    } catch (Exception e) {
        return false;
    }
    setTypeface(tf);
    return true;
}

private void setCustomFontTextView(Context ctx, AttributeSet attrs) {
    final TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EditTextLight);
    customFont = a.getString(R.styleable.EditTextLight_edittextfont);
    setCustomFontTextView(ctx, customFont);
    a.recycle();
}

}