/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.tekzee.cardate;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.tekzee.cardate";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "1.7";
  // Fields from default config.
  public static final String BASE_URL = "https://admin.cardateapp.com/api/";
  public static final String CALL_BACK_URL = "http://admin.cardateapp.com";
  public static final String INSTAGRAM_CLIENT_ID = "098474f5811b487b9f58c0ece085fa60";
  public static final String INSTAGRAM_SECRET_ID = "db54112adc704d80b8697fee18dedbdc";
}
